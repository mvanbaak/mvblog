<?php
/**
 * MvBlog -- An open source no-nosense blogtool
 *
 * Copyright (C) 2005-2008, Michiel van Baak
 * Michiel van Baak <mvanbaak@users.sourceforge.net>
 *
 * See http://dev.mvblog.org for more information on MvBlog.
 * That page also provides Bugtrackers, Filereleases etc.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 * PHP version 5
 *
 * @category  PHP
 * @package   MvBlog
 * @author    Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright 2005-2008 Michiel van Baak
 * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @version   SVN: $Revision: 809 $
 * @link      http://www.mvblog.org
 */

/**
 * Class to parse an ini file
 *
 * @category  PHP
 * @package   MvBlog
 * @author    Ferry Boender <void@example.com>
 * @copyright 2005-2008 Ferry Boender
 * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @version   Release: %%VERSION%%
 * @link      http://www.mvblog.org
 */
class MvBlog_IniParser {
	const TYPE_STRING = 1;
	const TYPE_BOOL = 2;
	const TYPE_INT = 3;

	public static $typeNames = array(
		MvBlog_IniParser::TYPE_STRING => "String",
		MvBlog_IniParser::TYPE_BOOL   => "Boolean",
		MvBlog_IniParser::TYPE_INT    => "Integer",
		);
	protected $availSettings = array();
	protected $settings = array();

	/**
	 * Class constructor that sets a set of default settings
	 *
	 * @param array $availSettings The defaults
	 *
	 * @return void
	 */
	public function __construct($availSettings) {
		$this->availSettings = $availSettings;
		$this->setDefaults($availSettings);
	}

	/**
	 * Parse ini settings
	 *
	 * @param string $contents The settings contents
	 *
	 * @return array The settings in an assoc array
	 */
	protected function parse($contents) {
		$values = array();

		// Find and replace dos line endings
		$contents = str_replace("\r\n", "\n", $contents);
		// Find and replace mac line endings
		$contents = str_replace("\r", "\n", $contents);
		// Set section to nothing by default
		$section = "";

		$contents = explode("\n", $contents);
		for ($i = 0; $i < count($contents); $i++) {
			// Look for a section
			if (preg_match('/^\[(.*?)\]*$/', $contents[$i], $match)) {
				$section = $match[1];
			}

			if (!preg_match("/^\s*[#;].*$/", $contents[$i])
			    && preg_match('/^\s*(.*?)\s*=\s*(.*?)\s*$/', $contents[$i], $match)
			) {
				$key = $match[1];
				$value = $match[2];
				$values[$section][$key] = $value; // FIXME: Strip 
			}
		}

		return($values);
	}

	/**
	 * Set config defaults
	 *
	 * @param array $availSettings The available settings
	 *
	 * @return void
	 */
	protected function setDefaults($availSettings) {
		foreach ($availSettings as $section => $data) {
			foreach ($data as $key => $value) {
				if (array_key_exists("default", $value)) {
					$this->setSetting($section, $key, $value["default"]);
				}
			}
		}
	}

	/**
	 * Set a specific setting
	 *
	 * @param string $section The section to set the setting in
	 * @param string $key     The key to set
	 * @param string $value   The value to set the setting to
	 *
	 * @return void
	 */
	public function setSetting($section, $key, $value) {
		if (!array_key_exists($key, $this->availSettings[$section])) {
			throw new MvBlog_IniParserException(1, $key, $section);
		}

		$type = $this->availSettings[$section][$key]["type"];
		switch($type) {
		case MvBlog_IniParser::TYPE_STRING:
			$this->settings[$section][$key] = (string)$value;
			break;
		case MvBlog_IniParser::TYPE_BOOL:
			if (strcasecmp($value, "on") === 0
			    || strcasecmp($value, "true") === 0
			    || strcasecmp($value, "yes") === 0
			    || $value == '1'
			) {
				$this->settings[$section][$key] = true;
			} elseif (strcasecmp($value, "off") === 0
			    || strcasecmp($value, "false") === 0
			    || strcasecmp($value, "no") === 0
			    || $value == '0'
			) {
				$this->settings[$section][$key] = false;
			} else {
				throw new MvBlog_IniParserException(2, $value, $key, MvBlog_IniParser::$typeNames[$type], $section);
			}
			break;
		case MvBlog_IniParser::TYPE_INT:
			if (is_int($value) || ctype_digit($value)) {
				$this->settings[$section][$key] = (int)$value;
			} else {
				throw new MvBlog_IniParserException(2, $value, $key, MvBlog_IniParser::$typeNames[$type], $section);
			}
			break;
		default:
			throw new MvBlog_IniParserException(3, $this->availSettings[$section][$key]);
			break;
		}
	}

	/**
	 * Get one setting from inifile
	 *
	 * @param string $section The section to use for the setting
	 * @param string $key     The setting key to fetch
	 *
	 * @return string The value of the setting
	 */
	public function getSetting($section, $key) {
		if (array_key_exists($key, $this->settings[$section])) {
			return($this->settings[$section][$key]);
		} else {
			return(null);
		}
	}

	/**
	 * Get all settings from in-memory inifile config
	 *
	 * @param string $section The section to look in
	 *
	 * @return string the setting content
	 */
	public function getSettings($section = "") {
		//if section is empty return the complete settings array
		if (!$section) {
			return $this->settings;
		} else {
			if (array_key_exists($section, $this->settings)) {
				return $this->settings[$section];
			} else {
				return null;
			}
		}
	}
}
?>
