<?php
/**
 * MvBlog -- An open source no-nosense blogtool
 *
 * Copyright (C) 2005-2008, Michiel van Baak
 * Michiel van Baak <mvanbaak@users.sourceforge.net>
 *
 * See http://dev.mvblog.org for more information on MvBlog.
 * That page also provides Bugtrackers, Filereleases etc.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 * PHP version 5
 *
 * @category  PHP
 * @package   MvBlog
 * @author    Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright 2005-2008 Michiel van Baak
 * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @version   SVN: $Revision: 809 $
 * @link      http://www.mvblog.org
 */
/*
 * There are several possible rss feeds:
 * - global article feed. no url params needed
 * - comments. rss.php?mode=comments
 * - comments on a specific article. rss.php?mode=comments&articleid=<nr>
 * - posts for a category. rss.php&mode=category&cat_id=<nr>
 * - posts by an author. rss.php&mode=author&author_id=<nr>
 */
require "mvblog.php";
$mvblog = new MvBlog();

//generate url to the root of our website
if (array_key_exists("HTTPS", $_SERVER) && $_SERVER["HTTPS"] == "on") {
	$proto = "https";
} else {
	$proto = "http";
}
$url = $proto."://".$_SERVER["SERVER_NAME"].(substr($_SERVER["REQUEST_URI"], 0, strpos($_SERVER["REQUEST_URI"], "common/")));

/* most feeds have limit of 1 month */
$max_time = mktime(0, 0, 0, date("m"), date("d")+1, date("Y"));

if (!array_key_exists("mode", $_REQUEST)) {
	$_REQUEST["mode"] = "";
}

switch ($_REQUEST["mode"]) {
case "comments" :
	$sql  = "SELECT c.id AS commentid, c.comment AS body, c.date AS date, c.name AS name,";
	$sql .= " a.title AS title, a.id AS id, a.categories_ids AS categories_id";
	$sql .= " FROM comments c, articles a WHERE a.active=1 AND a.public=1 AND a.id=c.articles_id";
	if (array_key_exists("articleid", $_REQUEST)) {
		$sql .= sprintf(" AND a.id = %d", $_REQUEST["articleid"]);
	}
	$sql .= " ORDER BY c.date DESC";
	$comments = true;
	break;
case "category" :
	if (!array_key_exists("cat_id", $_REQUEST)) {
		die("no category given");
	}
	$sql = sprintf(
		"SELECT * FROM articles WHERE date <= %1\$d AND active=1 AND public=1 AND (
			categories_ids = '%2\$d' OR categories_ids like '%%,%2\$d' OR categories_ids like '%2\$d,%%' 
			OR categories_ids like '%%,%2\$d,%%'
		) ORDER BY date DESC", $max_time, $_REQUEST["cat_id"]
	);
	$comments = false;
	break;
case "author" :
	if (!array_key_exists("author_id", $_REQUEST)) {
		die("no author given");
	}
	$sql = sprintf(
		"SELECT * FROM articles WHERE date <= %d AND active=1 AND public=1 AND authors_id = %d ORDER BY date DESC",
		$max_time, $_REQUEST["author_id"]
	);
	$comments = false;
	break;
case "dossier" :
	if (!array_key_exists("dossier_id", $_REQUEST)) {
		/* list of dossiers */
		$sql = sprintf(
			"SELECT id, name AS title, 0 AS authors_id, %s AS body, '%d' AS date 
			FROM dossiers WHERE public = 1 AND active = 1 ORDER BY name",
			$mvblog->db_quote("desc"), mktime()
		);
	} else {
		/* list articles in given dossier */
		$sql = sprintf("SELECT * FROM articles WHERE active = 1 AND public = 1 AND dossier_id = %d", $_REQUEST["dossier_id"]);
	}
	$comments = false;
	break;
default :
	$sql = sprintf("SELECT * FROM articles WHERE date <= %d AND active=1 AND public=1 ORDER BY date DESC", $max_time);
	$comments = false;
	break;
}
$mvblog->db->setLimit(20);
$res =& $mvblog->db->query($sql);

if (PEAR::isError($res)) {
	die($res->getUserInfo());
}
header("Content-Type: text/xml");
echo "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n";
echo "<rss version=\"2.0\">\n";
echo "\t<channel>\n";
echo "\t\t<title>".$mvblog->settings["blogtitle"]."</title>\n";
echo "\t\t<link>".$url."</link>\n";
echo "\t\t<description>".htmlentities($mvblog->settings["blogdescription"])."</description>\n";
echo "\t\t<generator>MvBlog ".$mvblog->version."</generator>\n";
echo "\t\t<docs>http://blogs.law.harvard.edu/tech/rss</docs>\n";
echo "\t\t<ttl>1440</ttl>\n";
while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
	if ($comments) {
		$row["title"] = "Comment on ".$row["title"]." by: ".$row["name"];
		$link = "#comment".$row["commentid"];
	} else {
		$link = "";
	}
	echo "\t\t<item>\n";
	echo "\t\t\t<title>".htmlspecialchars($row["title"])."</title>\n";
	if (array_key_exists("aside", $row) && $row["aside"] == 1) {
		echo "\t\t\t<link>".$url."index.php</link>\n";
		echo "\t\t\t<category>asides</category>\n";
		echo "\t\t\t<guid isPermaLink=\"false\">".$url."index.php?aside=".$row["id"]."</guid>\n";
		echo "\t\t\t<author>".htmlspecialchars($mvblog->authors[$row["authors_id"]]["email"])." ";
		echo "(".htmlspecialchars($mvblog->authors[$row["authors_id"]]["fullname"]).")</author>\n";
	} else {
		if (array_key_exists("mode", $_REQUEST) && $_REQUEST["mode"] == "dossier" && !array_key_exists("dossier_id", $_REQUEST)) {
			echo "\t\t\t<link>".$url."index.php?action=viewdossier&amp;id=".$row["id"]."$link</link>\n";
		} else {
			echo "\t\t\t<link>".$url."index.php?action=view&amp;id=".$row["id"]."$link</link>\n";
		}
		if (array_key_exists("categories_ids", $row)) {
			$categories = explode(",", $row["categories_ids"]);
		} else {
			$categories = array();
		}
		echo "\t\t\t<category>";
		$category_names = array();
		foreach ($categories as $v) {
			if (array_key_exists($v, $mvblog->categories)) {
				$category_names[] = $mvblog->categories[$v]["name"];
			}
		}
		echo implode(", ", $category_names);
		echo "</category>\n";
		if (array_key_exists("mode", $_REQUEST) && $_REQUEST["mode"] == "dossier" && !array_key_exists("dossier_id", $_REQUEST)) {
			echo "\t\t\t<guid isPermaLink=\"true\">".$url."index.php?action=viewdossier&amp;id=".$row["id"]."$link</guid>\n";
		} else {
			echo "\t\t\t<guid isPermaLink=\"true\">".$url."index.php?action=view&amp;id=".$row["id"]."$link</guid>\n";
		}
		if (!$comments) {
			echo "\t\t\t<comments>".$url."index.php?action=view&amp;id=".$row["id"]."#comments</comments>\n";
			echo "\t\t\t<author>".htmlspecialchars($mvblog->authors[$row["authors_id"]]["email"])." ";
			echo "(".htmlspecialchars($mvblog->authors[$row["authors_id"]]["fullname"]).")</author>\n";
		}
	}
	echo "\t\t\t<description>";
	echo htmlentities($mvblog->strip_bbcode(strip_tags(str_replace("##BREAKPOINT##", "", stripslashes($row["body"])))));
	echo "</description>\n";
	echo "\t\t\t<pubDate>".date("r", $row["date"])."</pubDate>\n";

	echo "\t\t</item>\n";
}
echo "\t</channel>\n";
echo "</rss>";
?>
