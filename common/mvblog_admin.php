<?php
/**
     * MvBlog -- An open source no-nosense blogtool
     *
     * Copyright (C) 2005-2008, Michiel van Baak
     * Michiel van Baak <mvanbaak@users.sourceforge.net>
     *
     * See http://dev.mvblog.org for more information on MvBlog.
     * That page also provides Bugtrackers, Filereleases etc.
     *
     * This program is free software, distributed under the terms of
     * the GNU General Public License Version 2. See the LICENSE file
     * at the top of the source tree.
     *
     * PHP version 5
     *
     * @category  PHP
     * @package   MvBlog
     * @author    Michiel van Baak <mvanbaak@users.sourceforge.net>
     * @copyright 2005-2008 Michiel van Baak
     * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
     * @version   SVN: $Revision: 859 $
     * @link      http://www.mvblog.org
     */

/**
 * Class that holds methods to create admin site.
 *
 * @category  PHP
 * @package   MvBlog
 * @author    Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright 2005-2008 Michiel van Baak
 * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @version   Release: %%VERSION%%
 * @link      http://www.mvblog.org
 */
Class MvBlog_admin extends MvBlog_common {

    /* constants */

    /* variables */
    public $lang = "en_US";
    public $languages = array(
        "en_US" => "english",
        "nl_NL" => "dutch",
        "sv_SE" => "swedish",
    );
    private $_selected_menuitem;
    private $_selected_submenuitem;

    /* methods */

    /* __construct {{{ */
    /**
     * Class constructor. Check some defaults etc
     *
     * @param string $basedir Base directory where plugins are located
     *
     * @return void
     */
    public function __construct($basedir="") {
        /* first do the common construct tasks */
        parent::__construct($basedir."plugins/", 1);
        $this->webroot = $this->webroot."admin/";
        $this->log = new MvBlog_log($basedir, 1);

        if (array_key_exists("action", $_POST)) {
            $action = $_POST["action"];
        } else {
            $action = "";
        }

        /* check if we are logged in */
        if (!array_key_exists("author_id", $_SESSION) && $action != "check_login") {
            $this->show_login();
        }
        if (array_key_exists("action", $_REQUEST)) {
            switch($_REQUEST["action"]) {
            case "show_cats" :
            case "edit_cat" :
                $this->_selected_menuitem = "manage";
                $this->_selected_submenuitem = "categories";
                break;
            case "show_dossiers" :
            case "edit_dossier" :
                $this->_selected_menuitem = "manage";
                $this->_selected_submenuitem = "dossiers";
                break;
            case "show_userlog":
                $this->_selected_menuitem = "log";
                $this->_selected_submenuitem = "users";
                break;
            case "show_adminlog":
                $this->_selected_menuitem = "log";
                $this->_selected_submenuitem = "admin";
                break;
            case "show_authors" :
            case "edit_author" :
                $this->_selected_menuitem = "users";
                if (array_key_exists("id", $_REQUEST) && $_REQUEST["id"] == $_SESSION["author_id"]) {
                    $this->_selected_submenuitem = "my";
                } else {
                    $this->_selected_submenuitem = "authors";
                }
                break;
            case "show_users" :
            case "edit_user" :
                $this->_selected_menuitem = "users";
                $this->_selected_submenuitem = "users";
                break;
            case "show_posts" :
            case "edit_post" :
                $this->_selected_menuitem = "manage";
                $this->_selected_submenuitem = "posts";
                break;
            case "show_comments" :
            case "edit_comment" :
                $this->_selected_menuitem = "manage";
                $this->_selected_submenuitem = "comments";
                break;
            case "show_plugins" :
            case "config_plugin" :
            case "edit_plugin_setting" :
            case "save_plugin_setting" :
                $this->_selected_menuitem = "plugins";
                break;
            case "show_settings" :
            case "save_settings" :
                $this->_selected_menuitem = "settings";
                $this->_selected_submenuitem = "settings";
                break;
            case "show_menuitems" :
                $this->_selected_menuitem = "settings";
                $this->_selected_submenuitem = "menuitems";
                break;
            case "show_about" :
                $this->_selected_menuitem = "index";
                $this->_selected_submenuitem = "about";
                break;
            default :
                $this->_selected_menuitem = "index";
                $this->_selected_submenuitem = "index";
                break;
            }
        } else {
            $this->_selected_menuitem = "index";
            $this->_selected_submenuitem = "index";
        }
    }
    /* }}} */
    /* _strip_tags {{{ */
    /**
     * strip all html tags cept for some tags we like.
     * The tags we leave will be stripped from attributes we dont like.
     *
     * @param string $text The text to process
     *
     * @return string The text with only allowed tags.
     */
    private function _strip_tags($text) {
        $allowed_tags  = "<a><abbr><acronym><address><area><b><bdo><big><blockquote><br><caption><center><cite>";
        $allowed_tags .= "<code><col><dd><del><dir><div><dfn><dl><dt><fieldset><font><h1><h2><h3><h4><h5><h6>";
        $allowed_tags .= "<hr><i><img><ins><kbd><li><link><menu><ol><p><pre><q><s><samp><small><span><strike>";
        $allowed_tags .= "<strong><style><sub><sup><table><tbody><td><tfoot><th><thead><tr><tt><u><ul><var><em>";
        $text = strip_tags($text, $allowed_tags);
        return @preg_replace('/<(.*?)>/ie', "'<'.$this->_strip_attributes('\\1').'>'", $text);
    }
    /* }}} */
    /* _strip_attributes {{{ */
    /**
     * strip evil attributes from tags
     *
     * @param string $text The text to process
     *
     * @return string The processed text
     */
    private function _strip_attributes($text) {
        $attr_to_strip = array(
            "javascript:",
            "onclick",
            "ondblclick",
            "onmousedown",
            "onmouseup",
            "onmouseover"
        );
        foreach ($attr_to_strip as $attribute) {
            $text = stripslashes(preg_replace("/$attribute/i", "forbidden", $text));
        }
        return $text;
    }
    /* }}} */
    /* show_login {{{ */
    /**
     * Show admin login screen
     *
     * @return void
     */
    public function show_login() {
        $this->html_header("Admin login");
        ?>
		<form name="loginform" method="post" action="index.php">
		<input type="hidden" name="action" value="check_login" />
		<div id="if_container">
			<div id="if_title"></div>
			<div id="if_bar1"></div>
			<div id="if_page_header">
				<h1 class="page_title">login</h1>
			</div>
			<div id="if_page">
				<div class="log_post">
					<table border="0" cellspacing="3" cellpadding="0" align="center"><tr>
						<td align="right">username:</td><td><input type="text" id="loginname" name="login[name]" /></td>
					</tr><tr>
						<td align="right">password:</td><td><input type="password" name="login[password]" /></td>
					</tr><tr>
						<td colspan="2" align="center"><input type="submit" value="login" /></td>
					</tr></table>
				</div>
		</form>
		<script language="Javascript" type="text/javascript">
			document.loginform.loginname.focus();
		</script>
		<?php
        $this->html_footer();
        exit;
    }
    /* }}} */
    /* check_login {{{ */
    /**
     * Check user supplied data against admin database
     *
     * @param array $login name and password to check
     *
     * @return void
     */
    public function check_login($login) {
        $query = sprintf(
            "SELECT * FROM authors WHERE login = '%s' AND password = '%s' AND active = 1",
            $this->db_escape($login["name"]),
            $this->db_escape($login["password"])
        );
        $res =& $this->db->query($query);
        if (PEAR::isError($res)) {
            die($res->getUserInfo());
        }
        if ($res->numRows()) {
            $row = $res->fetchRow(MDB2_FETCHMODE_ASSOC);
            $_SESSION["author_id"]       = $row["id"];
            $_SESSION["author_name"]     = $row["login"];
            $_SESSION["author_fullname"] = $row["fullname"];
            $_SESSION["author_email"]    = $row["email"];
            $_SESSION["author_website"]  = $row["website"];
            $_SESSION["blog_user"]       = 1;
            $this->log->add_log(mktime(), $row["id"], 1, sprintf("Admin %s logged in", $row["login"]));
            header("Location: index.php");
        } else {
            $this->show_login();
        }
    }
    /* }}} */
    /* logout {{{ */
    /**
     * Logout user
     *
     * @return void
     */
    public function logout() {
        $this->log->add_log(mktime(), $_SESSION["author_id"], 1, sprintf("Admin %s logged off", $_SESSION["author_name"]));
        session_destroy();
        header("Location: index.php");
    }
    /* }}} */
    /* show_index {{{ */
    /**
     * Show nice welcome screen for admin
     *
     * @return void
     */
    public function show_index() {
        ?>
		<script language="Javascript1.2" type="text/javascript">
			var reloadinterval = setInterval("document.location.href='index.php';", 60000);
		</script>
		<p class="first"><?php echo gettext("Welcome to MvBlog"); ?> "<?php echo $_SESSION["author_fullname"]; ?>".</p>
		<p class="first">
			<h3><?php echo gettext("Use these links to get started"); ?>:</h3>
			<ul>
				<li><a href="index.php?action=edit_post&id=0"><?php echo gettext("write post"); ?></a></li>
				<li>
					<a href="index.php?action=edit_author&id=<?php echo $_SESSION["author_id"]; ?>">
						<?php echo gettext("update your account settings"); ?>
					</a>
				</li>
			</ul>
		</p>
		<p class="first">
			<h3><?php echo gettext("5 latest posts"); ?></h3>
			<ul>
				<?php
                $sql = "SELECT id, title, last_modified, modified_by, date, authors_id FROM articles ORDER BY date DESC LIMIT 5";
                $res =& $this->db->query($sql);
                while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                    if (!$row["last_modified"]) {
                        $row["last_modified"] = $row["date"];
                    }
                    if (!$row["modified_by"]) {
                        $row["modified_by"] = $row["authors_id"];
                    }
                    echo "<li>";
                    echo "<a href=\"index.php?action=edit_post&id=".$row["id"]."\">".$row["title"]."</a> ";
                    echo "(modified ".date("d-m-Y H:i", $row["last_modified"])." ";
                    echo "by ".$this->authors[$row["modified_by"]]["fullname"].")</li>\n";
                }
                ?>
			</ul>
		</p>
		<p class="first">
			<h3><?php echo gettext("5 latest comments"); ?></h3>
			<ul>
				<?php
                $sql = "SELECT id, title, date, name, articles_id FROM comments ORDER BY date DESC LIMIT 5";
                $res =& $this->db->query($sql);
                while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                    if (!$row["title"]) {
                        $row["title"] = "[".gettext("no title")."]";
                    }
                    $q = sprintf("SELECT title FROM articles WHERE id = %d", $row["articles_id"]);
                    $r =& $this->db->query($q);
                    $art = $r->fetchRow(MDB2_FETCHMODE_ASSOC);
                    echo "<li>";
                    echo "<a href=\"../index.php?action=view&id=".$row["articles_id"]."#comment".$row["id"]."\">".$row["title"]."</a>";
                    echo " - ".$art["title"]." (".date("d-m-Y H:i", $row["date"])." by ".$row["name"].")</li>\n";
                }
                ?>
			</ul>
		</p>
		<p class="first">
			<h3><?php echo gettext("latest mvblog news"); ?></h3>
			<?php
            $oldtimeout = ini_set("default_socket_timeout", 5);
            if (!($rss = file_get_contents("http://www.mvblog.org/blog/common/rdf.php"))) {
                echo "Your php setup does not allow opening remote files.<br />";
                echo "For the latest news and updates visit <a href=\"http://www.mvblog.org\">http://www.mvblog.org</a>";
            } else {
                echo "<ul>";
                include "rssparser.php";
                $rssparser = new RSSParser($rss);
                $rsscount = 0;
                foreach ($rssparser->rssItems as $rssItem) {
                    if (strstr($rssItem["dc:subject"], "updates")) {
                        $rsscount++;
                        if ($rsscount > 5) {
                            break;
                        }
                        echo "<li><a href=\"".$rssItem["link"]."\" target=\"_new\">".$rssItem["title"]."</a> ";
                        echo "(".date("d-m-Y H:i", $rssItem["dc:date"])." by ".$rssItem["dc:creator"].")</li>";
                    }
                }
                echo "</ul>";
            }
            ini_set("default_socket_timeout", $oldtimeout);
            ?>
		</p>
		<?php
    }
    /* }}} */
    /* show_about {{{ */
    /**
     * Show information about the current version etc.
     *
     * @return void
     */
    public function show_about() {
        /* gather some info we want to show */
        $mvblogversion = $this->version;
        if (array_key_exists("dbversion", $this->settings)) {
            $dbversion = $this->settings["dbversion"];
        } else {
            $dbversion = gettext("Unknown");
        }
        if (array_key_exists("webroot", $this->settings)) {
            $webroot = $this->settings["webroot"];
        } else {
            $webroot = gettext("Unknown");
        }
        if (is_array($this->active_plugins)) {
            $active_plugins = implode(", ", $this->active_plugins);
        } else {
            $active_plugins = gettext("None");
        }
        ?>
		<p class="first">
			<h2>Information about this MvBlog instance</h2>
			MvBlog version: <?php echo $mvblogversion; ?><br />
			Database version: <?php echo $dbversion; ?><br />
			Active plugins: <?php echo $active_plugins; ?> <br />
		</p>
		<p class="first">
			MvBlog is created by Michiel van Baak &lt;michiel@mvblog.org&gt;<br />
			With the help of Leonieke Aalders, Sofie van Tendeloo en Ferry Boender.<br />
			Besides those 3 contributers others have helped as well, but those 3 were there from the beginning<br />
			and without them this project would never be this far.<br /><br />
			For more information please visit <a href="http://www.mvblog.org">www.mvblog.org</a><br />
			For documentation please go to the <a href="http://dev.mvblog.org/wiki/EndUserDocumentation">wiki</a><br />
			If you found a bug or need a new feature please report it on <a href="http://dev.mvblog.org">the developement website</a>.
		</p>
		<?php
    }
    /* }}} */
    /* show_admin_menu {{{ */
    /**
     * If user is logged in, show menu
     *
     * @return void
     */
    public function show_admin_menu() {
        if (array_key_exists("author_id", $_SESSION)) {
            ?>
			<div id="if_menu">
				<a class="if_menu_item" href="../index.php">Site</a>&nbsp;
				<a class="if_menu_item<?php
                if ($this->_selected_menuitem == "index") {
                    echo "_act";
                }
                ?>" href="./index.php"><?php echo gettext("Main"); ?></a>&nbsp;
				<a class="if_menu_item<?php
                if ($this->_selected_menuitem == "manage") {
                    echo "_act";
                }
                ?>" href="./index.php?action=show_posts"><?php echo gettext("Manage"); ?></a>&nbsp;
				<a class="if_menu_item<?php
                if ($this->_selected_menuitem == "users") {
                    echo "_act";
                }
                ?>" href="./index.php?action=show_userlog"><?php echo gettext("Users"); ?></a>&nbsp;
				<a class="if_menu_item<?php
                if ($this->_selected_menuitem == "settings") {
                    echo "_act";
                }
                ?>" href="./index.php?action=show_settings"><?php echo gettext("Settings"); ?></a>&nbsp;
				<a class="if_menu_item<?php
                if ($this->_selected_menuitem == "plugins") {
                    echo "_act";
                }
                ?>" href="./index.php?action=show_plugins"><?php echo gettext("Plugins"); ?></a>&nbsp;
				<a class="if_menu_item<?php
                if ($this->_selected_menuitem == "import") {
                    echo "_act";
                }
                ?>" href="./index.php?action=show_import"><?php echo gettext("Import"); ?></a>&nbsp;
				<a class="if_menu_item<?php
                if ($this->_selected_menuitem == "log") {
                    echo "_act";
                }
                ?>" href="./index.php?action=show_adminlog"><?php echo gettext("Log"); ?></a>&nbsp;
				<a class="if_menu_item" href="./index.php?action=logout"><?php echo gettext("Logout"); ?></a>&nbsp;
			</div>
			<div id="if_submenu">
				<?php
                switch ($this->_selected_menuitem) {
                case "manage" :
                    ?>
					<a class="if_submenu_item<?php
                    if ($this->_selected_submenuitem == "posts") {
                        echo "_act";
                    }
                    ?>" href="./index.php?action=show_posts"><?php echo gettext("Posts"); ?></a>&nbsp;
					<a class="if_submenu_item<?php
                    if ($this->_selected_submenuitem == "dossiers") {
                        echo "_act";
                    }
                    ?>" href="./index.php?action=show_dossiers"><?php echo gettext("Dossiers"); ?></a>&nbsp;
					<a class="if_submenu_item<?php
                    if ($this->_selected_submenuitem == "categories") {
                        echo "_act";
                    }
                    ?>" href="./index.php?action=show_cats"><?php echo gettext("Categories"); ?></a>&nbsp;
					<a class="if_submenu_item<?php
                    if ($this->_selected_submenuitem == "comments") {
                        echo "_act";
                    }
                    ?>" href="./index.php?action=show_comments"><?php echo gettext("Comments"); ?></a>&nbsp;
					<?php
                    break;
                case "users" :
                    ?>
					<a class="if_submenu_item<?php
                    if ($this->_selected_submenuitem == "authors") {
                        echo "_act";
                    }
                    ?>" href="./index.php?action=show_authors"><?php echo gettext("Authors"); ?></a>&nbsp;
					<a class="if_submenu_item<?php
                    if ($this->_selected_submenuitem == "users") {
                        echo "_act";
                    }
                    ?>" href="./index.php?action=show_users"><?php echo gettext("Users"); ?></a>&nbsp;
					<a class="if_submenu_item<?php
                    if ($this->_selected_submenuitem == "my") {
                        echo "_act";
                    }
                    ?>" href="./index.php?action=edit_author&id=<?php echo $_SESSION["author_id"];?>">
					<?php echo gettext("Your profile"); ?></a>&nbsp;
					<?php
                    break;
                case "settings" :
                    ?>
					<a class="if_submenu_item<?php
                    if ($this->_selected_submenuitem == "settings") {
                        echo "_act";
                    }
                    ?>" href="./index.php?action=show_settings"><?php echo gettext("Settings"); ?></a>&nbsp;
					<a class="if_submenu_item<?php
                    if ($this->_selected_submenuitem == "menuitems") {
                        echo "_act";
                    }
                    ?>" href="./index.php?action=show_menuitems"><?php echo gettext("Menu items"); ?></a>&nbsp;
					<?php
                    break;
                case "index" :
                    ?>
					<a class="if_submenu_item<?php
                    if ($this->_selected_submenuitem == "index") {
                        echo "_act";
                    }
                    ?>" href="./index.php"><?php echo gettext("Main"); ?></a>&nbsp;
					<a class="if_submenu_item<?php
                    if ($this->_selected_submenuitem == "about") {
                        echo "_act";
                    }
                    ?>" href="./index.php?action=show_about"><?php echo gettext("About"); ?></a>&nbsp;
					<?php
                    break;
                case "log" :
                    ?>
					<a class="if_submenu_item<?php
                    if ($this->_selected_submenuitem == "admin") {
                        echo "_act";
                    }
                    ?>" href="./index.php?action=show_adminlog"><?php echo gettext("Backend"); ?></a>&nbsp;
					<a class="if_submenu_item<?php
                    if ($this->_selected_submenuitem == "users") {
                        echo "_act";
                    }
                    ?>" href="./index.php?action=show_userlog"><?php echo gettext("Frontend"); ?></a>&nbsp;
					<?php
                    break;
                default:
                    break;
                }
                ?>
			</div>
			<?php
        }
    }
    /* }}} */
    /* show_cats {{{ */
    /**
     * Show overview of available categories
     *
     * @return void
     */
    public function show_cats() {
        $count = 0;
        ?>
			<h1 class="log_post_new">
			<a href="./index.php?action=edit_cat&amp;id=0"><?php echo gettext("create new"); ?></a>
			</h1><br />
		<?php
        foreach ($this->categories as $id => $cat) {
            if ($id == 0) {
                continue;
            }
            ?>
			<div class="log_post">
				<div class="log_post_head">
					<h1 class="log_post_h1">
						<a href="?action=edit_cat&amp;id=<?php echo $id; ?>"><?php echo stripslashes($cat["name"]); ?></a>
					</h1>
					<?php
                    $r =& $this->db->query(
                        sprintf(
                            "SELECT COUNT(*) AS count FROM articles 
							WHERE (
							categories_ids LIKE '%%,%1\$d' 
							OR categories_ids LIKE '%1\$d,%%' 
							OR categories_ids = '%d' 
							OR categories_ids LIKE '%%,%1\$d,%%')",
                            $id
                        )
                    );
                    if (PEAR::isError($r)) {
                        die($r->getMessage());
                    }
                    $count = $r->fetchRow(MDB2_FETCHMODE_ASSOC);
                    ?>
					<h2 class="log_post_h2">
					<?php
                    echo sprintf(
                        ngettext("%d article in this category", "%d articles in this category", $count["count"]),
                        $count["count"]
                    );
                    ?></h2>
				</div>
				<div class="log_post_body">
					<div class="log_post_normal">
						<?php echo nl2br(stripslashes($cat["desc"])); ?>
					</div>
				</div>
				<div class="log_post_foot">
					<span class="log_post_date"><?php echo gettext("active"); ?>: <?php echo $cat["active"]; ?></span>
				</div>
			</div>
			<?php
        }
    }
    /* }}} */
    /* edit_cat {{{ */
    /**
     * Show form to edit a category.
     *
     * @param int $id The category id or 0 to create a new one
     *
     * @return void
     */
    function edit_cat($id) {
        $count = 0;
        /* check for valid category */
        if (!array_key_exists($id, $this->categories)) {
            die("no valid category id");
        }
        /* fetch icons for categories */
        $allowed_types = array("jpg", "png", "gif");
        $iconbasedir = "../images/categories/";
        $icons = array();
        if ($dh = opendir($iconbasedir)) {
            while (false !== ($v = readdir($dh))) {
                if (!is_dir($iconbasedir.$v) && in_array(substr($v, strlen($v)-3), $allowed_types)) {
                    $icons[] = $v;
                }
            }
        }
        asort($icons);
        /* get copy of current category */
        $cat = $this->categories[$id];
        ?>
		<form name="category" method="post" action="index.php">
		<input type="hidden" name="action" value="save_cat" />
		<input type="hidden" name="cat[id]" value="<?php echo $id; ?>" />
		<div class="log_post">
			<div class="log_post_head">
				<h1 class="log_post_h1"><input type="text" name="cat[name]" value="<?php echo stripslashes($cat["name"]); ?>" /></h1>
				<?php
                    /* count how many posts are holding this category */
                    $r =& $this->db->query(
                        sprintf(
                            "SELECT COUNT(*) AS count 
							FROM articles 
							WHERE (categories_ids LIKE '%%,%1\$d' 
							OR categories_ids LIKE '%1\$d,%%' 
							OR categories_ids = '%d' 
							OR categories_ids LIKE '%%,%1\$d,%%')",
                            $id
                        )
                    );
                    if (PEAR::isError($r)) {
                        die($r->getMessage());
                    }
                    $count = $r->fetchRow(MDB2_FETCHMODE_ASSOC);
                ?>
				<h2 class="log_post_h2">
					<?php
                    echo sprintf(
                        ngettext("%d article in this category", "%d articles in this category", $count["count"]),
                        $count["count"]
                    );
                    ?></h2>
				<br />
				<script language="Javascript1.2" type="text/javascript">
					function previewIcon() {
						var selectedIcon = document.getElementById('iconselect').options[document.getElementById('iconselect').selectedIndex].value;
						document.getElementById('iconPreview').innerHTML = '<img src="<?php echo $iconbasedir; ?>' + selectedIcon + '">';
					}
				</script>
				<?php
                /* show icons dropdown and preview of selected icon */
                if (count($icons)) {
                    echo "<select name=\"cat[icon]\" id=\"iconselect\" onchange=\"previewIcon()\">\n";
                    echo "<option value=\"0\">--</option>\n";
                    foreach ($icons as $icon) {
                        if (array_key_exists("icon", $cat) && $cat["icon"] == $icon) {
                            $selected = " SELECTED";
                        } else {
                            $selected = "";
                        }
                        echo "<option value=\"".$icon."\"".$selected.">".substr($icon, 0, strlen($icon)-4)."</option>\n";
                    }
                    echo "</select>\n";
                }
                echo gettext("choose category icon");
                ?><br />
				<span id="iconPreview">
					<?php
                    if (array_key_exists("icon", $cat)) {
                        echo "<img src=\"".$iconbasedir.$cat["icon"]."\" alt=\"icon\">\n";
                    }
                    ?>
				</span>
			</div>
			<div class="log_post_body">
				<textarea name="cat[description]" style="width: 200px; height: 100px;">
					<?php echo stripslashes($cat["desc"]); ?>
				</textarea><br />
				<input type="submit" value="<?php echo gettext("save"); ?>" />
				<input type="button" value="<?php echo gettext("cancel"); ?>" onClick="document.forms.category.action.value='show_cats';document.forms.category.submit();" />
				<?php
                if ($id) {
                    ?>
					<input type="button" value="<?php echo gettext("delete"); ?>" onClick="document.forms.category.action.value='delete_cat';document.forms.category.submit();" />
					<?php
                }
                ?>
			</div>
			<div class="log_post_foot">
				<span class="log_post_date"><?php echo gettext("active"); ?>:
				<?php
                if ($cat["active"]) {
                    $checked = "checked=\"checked\"";
                } else {
                    $checked = "";
                }
                ?>
				<input type="checkbox" value="1" name="cat[active]" <?php echo $checked; ?> />
				</span>
				<input type="hidden" name="cat[public]" value="1" />
			</div>
		</div>
		</form>
		<?php
    }
    /* }}} */
    /* save_cat {{{ */
    /**
     * Store altered/new category in the database
     *
     * @param array $cat The category info
     *
     * @return void
     */
    function save_cat($cat) {
        if ($cat["id"]) {
            $query  = sprintf(
                "UPDATE categories SET %s = '%s', %s = '%s', %s = %d, %s = %d, %s = '%s' WHERE id = %d",
                $this->db_quote("name"), $this->db_escape(strip_tags($cat["name"])),
                $this->db_quote("desc"), $this->db_escape(strip_tags($cat["description"])),
                $this->db_quote("active"), $cat["active"],
                $this->db_quote("public"), $cat["public"],
                $this->db_quote("icon"), $cat["icon"], $cat["id"]
            );
            $logmsg = sprintf("Category %d:%s updated", $cat["id"], $cat["name"]);
        } else {
            $query  = sprintf(
                "INSERT INTO categories (%s, %s, %s, %s, %s) VALUES ('%s', '%s', %d, %d, '%s')",
                $this->db_quote("name"), $this->db_quote("desc"), $this->db_quote("active"), $this->db_quote("public"), $this->db_quote("icon"),
                $this->db_escape(strip_tags($cat["name"])),
                $this->db_escape(strip_tags($cat["description"])),
                $cat["active"],
                $cat["public"],
                $cat["icon"]
            );
            $logmsg = sprintf("Category %s created", $cat["name"]);
        }
        $res =& $this->db->exec($query);
        if (PEAR::isError($res)) {
            die($res->getUserInfo());
        }
        $this->log->add_log(mktime(), $_SESSION["author_id"], 1, $logmsg);
        header("Location: index.php?action=show_cats");
    }
    /* }}} */
    /* delete_cat {{{ */
    /**
     * Delete category from database
     *
     * @param int $id The category id to delete
     *
     * @return void
     */
    function delete_cat($id) {
        $query = sprintf("DELETE FROM categories WHERE id=%d", $id);
        $res =& $this->db->exec($query);
        if (PEAR::isError($res)) {
            die($res->getUserInfo());
        }
        $this->log->add_log(mktime(), $_SESSION["author_id"], 1, sprintf("Category %d deleted", $id));
        header("Location: index.php?action=show_cats");
    }
    /* }}} */
    /* show_dossiers {{{ */
    /**
     * Show all dossiers in the database with some basic information attached
     *
     * @return void
     */
    public function show_dossiers() {
        $count = 0;
        ?>
		<h1 class="log_post_new">
		<a href="./index.php?action=edit_dossier&amp;id=0"><?php echo gettext("create new"); ?></a>
		</h1><br />
		<?php
        foreach ($this->dossiers as $id => $dossier) {
            if ($id == 0) {
                continue;
            }
            ?>
			<div class="log_post">
				<div class="log_post_head">
					<h1 class="log_post_h1">
					<a href="?action=edit_dossier&amp;id=<?php echo $id; ?>"><?php echo stripslashes($dossier["name"]); ?></a>
					</h1>
					<?php
                        $r =& $this->db->query(sprintf("SELECT COUNT(*) AS count FROM articles WHERE dossier_id = %d", $id));
                        if (PEAR::isError($r)) {
                            die($r->getMessage());
                        }
                        $count = $r->fetchRow(MDB2_FETCHMODE_ASSOC);
                    ?>
					<h2 class="log_post_h2">
					<?php
                    echo sprintf(
                        ngettext("%d article in this dossier", "%d articles in this dossier", $count["count"]),
                        $count["count"]
                    );
                    ?></h2>
				</div>
				<div class="log_post_body">
					<div class="log_post_normal">
						<?php echo nl2br(stripslashes($dossier["desc"])); ?>
					</div>
				</div>
				<div class="log_post_foot">
					<span class="log_post_date"><?php echo gettext("active"); ?>: <?php echo $dossier["active"]; ?></span>
					<span class="log_post_author"><?php echo gettext("public"); ?>: <?php echo $dossier["public"]; ?></span>
				</div>
			</div>
			<?php
        }
    }
    /* }}} */
    /* edit_dossier {{{ */
    /**
     * Show form to edit a dossier.
     *
     * @param int $id The dossier id or 0 to create a new one
     *
     * @return void
     */
    public function edit_dossier($id) {
        $count = 0;
        if (!array_key_exists($id, $this->dossiers)) {
            die("no valid dossier id");
        }
        $dossier = $this->dossiers[$id];
        ?>
		<form name="dossier" method="post" action="index.php">
		<input type="hidden" name="action" value="save_dossier" />
		<input type="hidden" name="dossier[id]" value="<?php echo $id; ?>" />
		<div class="log_post">
			<div class="log_post_head">
				<h1 class="log_post_h1">
				<input type="text" name="dossier[name]" value="<?php echo stripslashes($dossier["name"]); ?>" />
				</h1>
				<?php
                    $r =& $this->db->query(sprintf("SELECT COUNT(*) AS count FROM articles WHERE dossier_id = %d", $id));
                    if (PEAR::isError($r)) {
                        die($r->getMessage());
                    }
                    $count = $r->fetchRow(MDB2_FETCHMODE_ASSOC);
                ?>
				<h2 class="log_post_h2">
				<?php
                echo sprintf(
                    ngettext("%d article in this dossier", "%d articles in this dossier", $count["count"]),
                    $count["count"]
                );
                ?>
				</h2>
			</div>
			<div class="log_post_body">
				<textarea name="dossier[description]" style="width: 200px; height: 100px;">
					<?php echo stripslashes($dossier["desc"]); ?>
				</textarea><br />
				<input type="submit" value="<?php echo gettext("save"); ?>" />
				<input type="button" value="<?php echo gettext("cancel"); ?>" onClick="document.forms.dossier.action.value='show_dossiers';document.forms.dossier.submit();" />
				<?php
                if ($id) {
                    ?>
					<input type="button" value="<?php echo gettext("delete"); ?>" onClick="document.forms.dossier.action.value='delete_dossier';document.forms.dossier.submit();" />
					<?php
                }
                ?>
			</div>
			<div class="log_post_foot">
				<span class="log_post_date"><?php echo gettext("active"); ?>:
					<?php
                    if ($dossier["active"]) {
                        $checked = "checked=\"checked\"";
                    } else {
                        $checked = "";
                    }
                    ?>
					<input type="checkbox" value="1" name="dossier[active]" <?php echo $checked; ?> />
				</span>
				<span class="log_post_author"><?php echo gettext("public"); ?>:
					<?php
                    if ($dossier["public"]) {
                        $checked = "checked=\"checked\"";
                    } else {
                        $checked = "";
                    }
                    ?>
					<input type="checkbox" value="1" name="dossier[public]" <?php echo $checked; ?> />
				</span>
			</div>
		</div>
		</form>
		<?php
    }
    /* }}} */
    /* save_dossier {{{ */
    /**
     * Store altered/new dossier in the database
     *
     * @param array $dossier The dossier info
     *
     * @return void
     */
    public function save_dossier($dossier) {
        if ($dossier["id"]) {
            $query  = sprintf(
                "UPDATE dossiers SET %s = '%s', %s = '%s', %s = %d, %s = %d WHERE id = %d",
                $this->db_quote("name"), $this->db_escape(strip_tags($dossier["name"])),
                $this->db_quote("desc"), $this->db_escape(strip_tags($dossier["description"])),
                $this->db_quote("active"), $dossier["active"],
                $this->db_quote("public"), $dossier["public"],
                $dossier["id"]
            );
            $logmsg = sprintf("Dossier %d:%s updated", $dossier["id"], $dossier["name"]);
        } else {
            $query  = sprintf(
                "INSERT INTO dossiers (%s, %s, %s, %s) VALUES ('%s', '%s', %d, %d)",
                $this->db_quote("name"), $this->db_quote("desc"), $this->db_quote("active"), $this->db_quote("public"),
                $this->db_escape(strip_tags($dossier["name"])),
                $this->db_escape(strip_tags($dossier["description"])),
                $dossier["active"],
                $dossier["public"]
            );
            $logmsg = sprintf("Dossier %s created", $dossier["name"]);
        }
        $res =& $this->db->exec($query);
        if (PEAR::isError($res)) {
            die($res->getUserInfo());
        }
        $this->log->add_log(mktime(), $_SESSION["author_id"], 1, $logmsg);
        header("Location: index.php?action=show_dossiers");
    }
    /* }}} */
    /* delete_dossier {{{ */
    /**
     * Delete dossier from database
     *
     * @param int $id The dossier id to delete
     *
     * @return void
     */
    public function delete_dossier($id) {
        $query = sprintf("DELETE FROM dossiers WHERE id=%d", $id);
        $res =& $this->db->exec($query);
        if (PEAR::isError($res)) {
            die($res->getUserInfo());
        }
        $this->log->add_log(mktime(), $_SESSION["author_id"], 1, sprintf("Dossier %d deleted", $id));
        header("Location: index.php?action=show_dossiers");
    }
    /* }}} */
    /* show_authors {{{ */
    /**
     * show all authors
     *
     * @return void
     */
    public function show_authors() {
        ?>
		<h1 class="log_post_new">
			<a href="./index.php?action=edit_author&amp;id=0"><?php echo gettext("create new"); ?></a>
		</h1><br /><?php
        foreach ($this->authors as $id => $author) {
            if ($id == 0) {
                continue;
            }
            ?>
			<div class="log_post">
				<div class="log_post_head">
					<h1 class="log_post_h1">
					<a href="?action=edit_author&amp;id=<?php echo $id; ?>">
						<?php
                        echo stripslashes($author["fullname"]);
                        if ($author["active"]) {
                            echo "(".gettext("active").")";
                        } else {
                            echo "(".gettext("inactive").")";
                        }
                        ?>
					</a>
					</h1>
					<h2 class="log_post_h2"><?php echo gettext("loginname"); ?>: <?php echo stripslashes($author["login"]); ?></h2>
					<br />
				</div>
				<div class="log_post_foot">
				</div>
			</div>
			<?php
        }
    }
    /* }}} */
    /* edit_author {{{ */
    /**
     * show form to manipulate author
     *
     * @param int $authorid The author id to edit, or 0 to create a new one.
     *
     * @return void
     */
    public function edit_author($authorid) {
        /* check if we are editing a real author */
        if (!array_key_exists($authorid, $this->authors)) {
            die("invalid authorid");
        }
        $author = $this->authors[$authorid];
        if (array_key_exists("error", $_REQUEST)) {
            echo "<font color=\"red\">".stripslashes($_REQUEST["error"])."</font>";
        }
        ?>
		<form name="author" method="post" action="index.php">
		<input type="hidden" name="action" value="save_author" />
		<input type="hidden" name="author[id]" value="<?php echo $authorid; ?>" />
		<input type="hidden" id="active" name="author[active]" value="<?php echo $author["active"]; ?>" />
		<div class="log_post">
			<div class="log_post_head">
				<h1 class="log_post_h1">
					<?php
                    if ($authorid) {
                        echo stripslashes($author["login"]);
                    } else {
                        echo gettext("login").": <input type=\"text\" name=\"author[login]\" value=\"".stripslashes($author["login"])."\" />";
                    }
                    ?>
				</h1>
				<br />
			</div>
			<div class="log_post_body">
				<div class="log_post_normal">
					<table border="0" cellspacing="0" cellpadding="0"><tr>
						<td align="right"><?php echo gettext("password"); ?>:</td>
						<td><input type="password" name="author[password]" /></td>
					</tr><tr>
						<td align="right"><?php echo gettext("password") . "(" . gettext("repeat") . ")"; ?>:</td>
						<td><input type="password" name="author[password1]" /></td>
					</tr><tr>
						<td align="right"><?php echo gettext("email"); ?>:</td>
						<td><input type="text" name="author[email]" value="<?php echo stripslashes($author["email"]); ?>" /></td>
					</tr><tr>
						<td align="right"><?php echo gettext("website"); ?>:</td>
						<td><input type="text" name="author[website]" value="<?php echo stripslashes($author["website"]); ?>" /></td>
					</tr><tr>
						<td align="right"><?php echo gettext("full name"); ?>: </td>
						<td><input type="text" name="author[fullname]" value="<?php echo stripslashes($author["fullname"]); ?>" /></td>
					</tr></table>
				</div>
				<input type="submit" value="save" />
				<?php if ($authorid) { ?>
					<?php if ($author["active"]) { ?>
						<input type="button" value="<?php echo gettext("disable"); ?>" onClick="document.forms.author.active.value=0;document.forms.author.action.value='save_author';document.forms.author.submit();" />
					<?php } else { ?>
						<input type="button" value="<?php echo gettext("enable"); ?>" onClick="document.forms.author.active.value=1;document.forms.author.action.value='save_author';document.forms.author.submit();" />
					<?php } ?>
				<?php } ?>
			</div>
			<div class="log_post_foot">
			</div>
		</div>
		</form>
		<?php
    }
    /* }}} */
    /* save_author($author): {{{ */
    /**
     * store new/altered author in database
     *
     * @param array $author The author info
     *
     * @return void
     */
    public function save_author($author) {
        if ($author["password"] && $author["password1"]) {
            if ($author["password"] != $author["password1"]) {
                $error = "Passwords don't match. Please correct this error and try again.";
            }
        }
        //make sure we are not inserting double loginnames
        if (!$author["id"]) {
            $res =& $this->db->query("SELECT COUNT(*) FROM authors WHERE login='".$author["login"]."'");
            $count = $res->fetchRow();
            if ($count[0]) {
                $error = "Login already excists. Please correct this error and try again.";
            }
        }
        if (!isset($error)) {
            if ($author["id"]) {
                $query = sprintf(
                    "UPDATE authors SET %s = '%s', %s = '%s', %s = '%s', %s = %d",
                    $this->db_quote("fullname"), $this->db_escape(strip_tags($author["fullname"])),
                    $this->db_quote("email"), $this->db_escape(strip_tags($author["email"])),
                    $this->db_quote("website"), $this->db_escape(strip_tags($author["website"])),
                    $this->db_quote("active"), $author["active"]
                );
                if (trim($author["password"])) {
                    $query .= sprintf(", %s = '%s'", $this->db_quote("password"), trim($this->db_escape(strip_tags($author["password"]))));
                }
                $query .= sprintf(" WHERE id = %d", $author["id"]);
                $logmsg = sprintf("Author %d:%s updated", $author["id"], $author["login"]);
            } else {
                $query = sprintf(
                    "INSERT INTO authors (%s, %s, %s, %s, %s, %s) VALUES ('%s', '%s', '%s', '%s', '%s', %d)",
                    $this->db_quote("password"), $this->db_quote("fullname"), $this->db_quote("email"), $this->db_quote("website"),
                    $this->db_quote("login"), $this->db_quote("active"),
                    $this->db_escape(strip_tags(trim($author["password"]))),
                    $this->db_escape(strip_tags($author["fullname"])),
                    $this->db_escape(strip_tags($author["email"])),
                    $this->db_escape(strip_tags($author["website"])),
                    $this->db_escape(strip_tags($author["login"])),
                    $author["active"]
                );
                $logmsg = sprintf("Author %s created", $author["login"]);
            }
            $res =& $this->db->exec($query);
            if (PEAR::isError($res)) {
                die($res->getUserInfo());
            }
            $this->log->add_log(mktime(), $_SESSION["author_id"], 1, $logmsg);
            header("Location: index.php?action=show_authors");
        } else {
            header("Location: index.php?action=edit_author&id=".$author["id"]."&error=$error");
        }
    }
    /* }}} */
    /* show_users(); {{{ */
    /**
     * Show registered users
     *
     * @return void
     */
    public function show_users() {
        $res =& $this->db->query("SELECT * FROM blog_users ORDER BY active, username");
        if (PEAR::isError($res)) {
            die($res->getMessage());
        }
        $inactive_times = array(
            (24*60*60*7)   => gettext("1 week"),
            (24*60*60*7*2) => gettext("2 weeks"),
            (24*60*60*7*3) => gettext("3 weeks"),
            (24*60*60*7*4) => gettext("4 weeks")
        );
        ?>
		<div class="log_post">
			<form method="post" name="frm_delete_inactive" id="frm_delete_inactive" action="index.php">
			<input type="hidden" name="action" value="delete_inactive_users" />
			<?php echo gettext("delete inactive users "); ?>:
			<input type="hidden" name="select_inactive" value="1" />
			<input type="submit" value="go" />
			</form>
		</div>
		<?php
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            ?>
			<div class="log_post">
				<div class="log_post_head">
					<h1 class="log_post_h1">
						<a href="?action=edit_user&amp;id=<?php echo $row["id"]; ?>">
						<?php
                        echo stripslashes($row["realname"]);
                        if ($row["active"]) {
                            echo "(".gettext("active").")";
                        } else {
                            echo "(".gettext("inactive").")";
                        }
                        ?>
						</a>
					</h1>
					<h2 class="log_post_h2">
						<?php echo gettext("loginname").": ".stripslashes($row["username"]); ?>
					</h2>
				</div>
				<br />
				<div class="log_post_foot">
				</div>
			</div>
			<?php
        }
    }
    /* }}} */
    /* edit_user($userid) {{{ */
    /**
     * show form to manipulate user
     *
     * @param int $userid The userid to edit, or 0 to create a new user.
     *
     * @return void
     */
    public function edit_user($userid) {
        if ($userid==0) {
            $user["id"]       = 0;
            $user["username"]    = "login";
            $user["password"] = "";
            $user["email"]    = "";
            $user["realname"] = "";
            $user["active"]   = 1;
            $user["website"]  = "http://";
        } else {
            $res =& $this->db->query(sprintf("SELECT * FROM blog_users WHERE id = %d", $userid));
            if (PEAR::isError($res)) {
                die($res->getMessage());
            }
            $user = $res->fetchRow(MDB2_FETCHMODE_ASSOC);
        }
        if (array_key_exists("error", $_REQUEST)) {
            echo "<font color=\"red\">".stripslashes($this->_strip_tags($_REQUEST["error"]))."</font>";
        }
        ?>
		<form name="user" method="post" action="index.php">
		<input type="hidden" name="action" value="save_user" />
		<input type="hidden" name="user[id]" value="<?php echo $user["id"]; ?>" />
		<input type="hidden" id="active" name="user[active]" value="<?php echo $user["active"]; ?>" />
		<div class="log_post">
			<div class="log_post_head">
				<h1 class="log_post_h1">
					<?php
                    if ($userid) {
                        echo stripslashes($user["username"]);
                    } else {
                        echo gettext("login").": <input type=\"text\" name=\"user[username]\" value=\"".stripslashes($user["username"])."\" />";
                    }
                    ?>
				</h1>
			</div>
			<div class="log_post_body">
				<div class="log_post_normal">
					<table border="0" cellspacing="0" cellpadding="0"><tr>
						<td align="right"><?php echo gettext("password"); ?>:</td>
						<td><input type="password" name="user[password]" /></td>
					</tr><tr>
						<td align="right"><?php echo gettext("password") . "(" . gettext("repeat") . ")"; ?>:</td>
						<td><input type="password" name="user[password1]" /></td>
					</tr><tr>
						<td align="right"><?php echo gettext("email"); ?>:</td>
						<td><input type="text" name="user[email]" value="<?php echo stripslashes($user["email"]); ?>" /></td>
					</tr><tr>
						<td align="right"><?php echo gettext("website"); ?>:</td>
						<td><input type="text" name="user[website]" value="<?php echo stripslashes($user["website"]); ?>" /></td>
					</tr><tr>
						<td align="right"><?php echo gettext("full name"); ?>:</td>
						<td><input type="text" name="user[realname]" value="<?php echo stripslashes($user["realname"]); ?>" /></td>
					</tr></table>
				</div>
				<input type="submit" value="<?php echo gettext("save"); ?>" />
				<?php if ($userid) { ?>
					<?php if ($user["active"]) { ?>
						<input type="button" value="<?php echo gettext("disable"); ?>" onClick="document.forms.user.active.value=0;document.forms.user.action.value='save_user';document.forms.user.submit();" />
					<?php } else { ?>
						<input type="button" value="<?php echo gettext("enable"); ?>" onClick="document.forms.user.active.value=1;document.forms.user.action.value='save_user';document.forms.user.submit();" />
					<?php } ?>
					<input type="button" value="<?php echo gettext("delete"); ?>" onclick="del_user();" />
				<?php } ?>
			</div>
			<div class="log_post_foot">
				<script language="Javascript1.2" type="text/javascript">
					function del_user() {
						if (confirm('<?php echo gettext("Are you sure you want to delete this user"); ?>')) {
							document.forms.user.action.value='delete_user';
							document.forms.user.submit();
						}
					}
				</script>
			</div>
		</div>
		</form>
		<?php
    }
    /* }}} */
    /* save_user($user) {{{ */
    /**
     * store new/altered user in database
     *
     * @param array $user The user info
     *
     * @return void
     */
    public function save_user($user) {
        if ($user["password"] && $user["password1"]) {
            if ($user["password"] != $user["password1"]) {
                $error = gettext("Passwords don't match. Please correct this error and try again.");
            }
        }
        //make sure we are not inserting double loginnames
        if (!$user["id"]) {
            $res =& $this->db->query(sprintf("SELECT COUNT(*) FROM blog_users WHERE %s='%s'", $this->db_quote("username"), $user["username"]));
            $count = $res->fetchRow();
            if ($count[0]) {
                $error = gettext("Login already excists. Please correct this error and try again.");
            }
        }
        if (!isset($error)) {
            if ($user["id"]) {
                $query = sprintf(
                    "UPDATE blog_users SET %s = '%s', %s = '%s', %s = '%s', %s = %d",
                    $this->db_quote("realname"), $this->db_escape(strip_tags($user["realname"])),
                    $this->db_quote("email"), $this->db_escape(strip_tags($user["email"])),
                    $this->db_quote("website"), $this->db_escape(strip_tags($user["website"])),
                    $this->db_quote("active"), $user["active"]
                );
                if (trim($user["password"])) {
                    $query .= sprintf(", %s = '%s'", $this->db_quote("password"), trim($this->db_escape(strip_tags($user["password"]))));
                }
                $query .= sprintf(" WHERE id = %d", $user["id"]);
                $logmsg = sprintf("User %d:%s updated", $user["id"], $user["username"]);
            } else {
                $query = sprintf(
                    "INSERT INTO blog_users (%s, %s, %s, %s, %s, %s) VALUES ('%s', '%s', '%s', '%s', '%s', %d)",
                    $this->db_quote("password"), $this->db_quote("realname"), $this->db_quote("email"), $this->db_quote("website"),
                    $this->db_quote("username"), $this->db_quote("active"),
                    $this->db_escape(strip_tags(trim($user["password"]))),
                    $this->db_escape(strip_tags($user["realname"])),
                    $this->db_escape(strip_tags($user["email"])),
                    $this->db_escape(strip_tags($user["website"])),
                    $this->db_escape(strip_tags($user["username"])),
                    $user["active"]
                );
                $logmsg = sprintf("User %s created", $user["username"]);
            }
            $res =& $this->db->exec($query);
            if (PEAR::isError($res)) {
                die($res->getUserInfo());
            }
            $this->log->add_log(mktime(), $_SESSION["author_id"], 1, $logmsg);
            header("Location: index.php?action=show_users");
        } else {
            header("Location: index.php?action=edit_user&id=".$user["id"]."&error=$error");
        }
    }
    /* }}} */
    /* delete_inactive_users {{{ */
    /**
     * Delete users that are inactive for some time
     *
     * @param int $time The time a user should be inactive before deleting it
     *
     * @return void
     */
    public function delete_inactive_users($time=0) {
        $sql = "DELETE FROM blog_users WHERE active=0";
        $res =& $this->db->exec($sql);
        header("Location: index.php?action=show_users");
    }
    /* }}} */
    /* delete_user($userid) {{{ */
    /**
     * remove user from database
     *
     * @param int $userid The userid to delete
     *
     * @return void
     */
    public function delete_user($userid) {
        $userid = sprintf("%d", $userid);
        if ($userid > 0) {
            /* check if user is in database */
            $sql = sprintf("SELECT COUNT(*) FROM blog_users WHERE id = %d", $userid);
            $res =& $this->db->query($sql);
            $row = $res->fetchRow();
            if ($row[0] > 0) {
                $sql = sprintf("DELETE FROM blog_users WHERE id = %d", $userid);
                $res = $this->db->exec($sql);
            }
        }
        $this->log->add_log(mktime(), $_SESSION["author_id"], 1, sprintf("User %d deleted", $userid));
        header("Location: index.php?action=show_users");
    }
    /* }}} */
    /* show_posts {{{ */
    /**
     * show a list of all posts with some info
     *
     * @param array $options Optional search options
     *
     * @return void
     */
    public function show_posts($options = array()) {
        if (!is_array($options)) {
            $options = array();
        }
        if (!array_key_exists("top", $options)) {
            $options["top"]   = 0;
        } else {
            $options["top"]   = (int)$options["top"];
        }
        if (!array_key_exists("limit", $options)) {
            $options["limit"] = 15;
        } else {
            $options["limit"] = (int)$options["limit"];
        }
        //put all categories in $this->categories array
        $this->get_categories();
        $this->categories[-1] = array(
            "name"   => "asides",
            "desc"   => "Small posts without comments",
            "active" => 1,
            "public" => 1
        );
        /* we selected a month only */
        if ((array_key_exists("month", $options) && $options["month"] > 0) && (array_key_exists("year", $options) && $options["year"] > 0)) {
            $postoptions["archive"] = 1;
            $postoptions["timestamp_start"] = mktime(0, 0, 0, $options["month"], 1, $options["year"]);
            $postoptions["timestamp_end"]   = mktime(0, 0, 0, $options["month"]+1, 1, $options["year"]);
        }

        if (array_key_exists("category_id", $options) && $options["category_id"]) {
            $postoptions["archive"] = 2;
            $postoptions["category_id"] = $options["category_id"];
        }
        if (array_key_exists("dossier", $options) && $options["dossier"]) {
            $postoptions["dossier"] = $options["dossier"];
        }

        $postoptions["start"] = $options["top"]-1;
        $postoptions["top"]   = $options["top"];
        $postoptions["limit"] = $options["limit"];
        /* max time is 10 years in the future */
        $postoptions["max_time"] = mktime(0, 0, 0, date("m"), date("d"), date("Y")+10);
        /* make sure we get all the data */
        $postoptions["adminmode"] = true;

        $posts = $this->get_posts($postoptions);
        $counter = $posts["total_count"];

        ?>
		<h1 class="log_post_new"><a href="./index.php?action=edit_post&amp;id=0"><?php echo gettext("create new"); ?></a></h1><br />
		<form id="filter" method="post" action="index.php?action=show_posts">
		<div id="post_select">
			<?php echo gettext("View month"); ?> <?php echo gettext("or category"); ?>:
			<select name="options[month]">
				<option value="0">---</option>
				<?php
                for ($i=1;$i<=12;$i++) {
                    if (array_key_exists("month", $options) && $options["month"] == $i) {
                        $selected = " SELECTED";
                    } else {
                        $selected = "";
                    }
                    echo "<option value=\"".$i."\"".$selected.">".date("M", mktime(0, 0, 0, $i, 1, 0))."</option>\n";
                }
                ?>
			</select>
			<select name="options[year]">
				<option value="0">---</option>
				<?php
                /* find the first post we made so we know the start year */
                $sql1 = sprintf("SELECT %s FROM articles ORDER BY date ASC", $this->db_quote("date"));
                $this->db->setLimit(1, 0);
                $res1 =& $this->db->query($sql1);
                $row1 = $res1->fetchRow();
                for ($i=date("Y", $row1[0]);$i<=date("Y");$i++) {
                    if (array_key_exists("year", $options) && $options["year"] == $i) {
                        $selected1 = " SELECTED";
                    } else {
                        $selected1 = "";
                    }
                    echo "<option value=\"".$i."\"".$selected1.">".$i."</option>\n";
                }
                ?>
			</select>
			<select name="options[category_id]">
				<option value="0">--</option>
				<?php
                $tmpcategories = $this->categories;
                unset($tmpcategories[0]);
                foreach ($tmpcategories as $catid => $cat) {
                    if (array_key_exists("category_id", $options) && $options["category_id"] == $catid) {
                        $selected2 = " SELECTED";
                    } else {
                        $selected2 = "";
                    }
                    echo "<option value=\"".$catid."\"".$selected2.">".$cat["name"]."</option>\n";
                }
                ?>
			</select>
			<select name="options[dossier]">
				<option value="0">--</option>
				<?php
                $tmpdossiers = $this->dossiers;
                unset($tmpdossiers[0]);
                foreach ($tmpdossiers as $dossier_id => $dossier) {
                    if (array_key_exists("dossier", $options) && $options["dossier"] == $dossier_id) {
                        $selected = " SELECTED";
                    } else {
                        $selected = "";
                    }
                    echo "<option value=\"".$dossier_id."\"".$selected.">".$dossier["name"]."</option>\n";
                }
                ?>
			</select>
			<a href="javascript:document.getElementById('filter').submit();"><?php echo gettext("go"); ?></a>
			<br /><br />
		</div>
		</form>
		<?php
        if (!array_key_exists("posts", $posts)) {
            $posts["posts"] = array();
        }
        foreach ($posts["posts"] as $row) {
            if (!trim($row["title"])) {
                $row["title"] = "[".gettext("no title")."]";
            }
            if ($row["aside"] == 1) {
                $row["categories_id"] = -1;
            }
            ?>
			<div class="log_post">
				<div class="log_post_head">
					<h1 class="log_post_h1"><a href="?action=edit_post&amp;id=<?php echo $row["id"]; ?>"><?php echo stripslashes($row["title"]); ?></a></h1>
					<h2 class="log_post_h2">
						<?php echo gettext("category").":&nbsp;"; ?>
						<?php
                        if (array_key_exists("categories_ids", $row)) {
                            $categories = explode(",", $row["categories_ids"]);
                            foreach ($categories as $v) {
                                if (array_key_exists($v, $this->categories)) {
                                    echo $this->categories[$v]["name"]."&nbsp;";
                                }
                            }
                        }
                        ?>
					</h2>
				</div>
				<div class="log_post_body">
					<div class="log_post_normal">
						<?php echo $row["body_formatted"]; ?>
					</div>
				</div>
				<div class="log_post_foot">
					<span class="log_post_author"><?php echo gettext("active"); ?>: <?php echo (int)$row["active"]; ?></span>
					<span class="log_post_author"><?php echo gettext("public"); ?>: <?php echo (int)$row["public"]; ?></span>
					<span class="log_post_author"><?php echo gettext("aside"); ?>:  <?php echo (int)$row["aside"]; ?></span>
				</div>
			</div>
			<?php
        }
        if (!array_key_exists("month", $options)) {
            $options["month"] = 0;
        }
        if (!array_key_exists("year", $options)) {
            $options["year"] = 0;
        }

        if ($options["top"]) {
            echo "<a class=\"link_prev\" href=\"index.php?action=show_posts&amp;options[month]=".(int)$options["month"]."&amp;options[year]=".(int)$options["year"]."&amp;options[top]=".($options["top"]-$options["limit"])."\">".gettext("previous")."</a> ";
        }
        if (($options["top"] + $options["limit"]) > $counter) {
            $end = $counter;
        } else {
            $end = ($options["top"]+$options["limit"]);
        }
        if ($counter == 0) {
            echo "<span class=\"log_cat\">".gettext("no posts");
        } else {
            echo "<span class=\"log_cat\">".gettext("showing")." ".($options["top"]+1)." ".gettext("to")." ".$end." ".gettext("of")." ".$counter." ".gettext("total posts");
        }
        if (($options["top"]+$options["limit"]) < $counter) {
            echo " <a class=\"link_next\" href=\"index.php?action=show_posts&amp;options[month]=".(int)$options["month"]."&amp;options[year]=".(int)$options["year"]."&amp;options[top]=".($options["top"]+$options["limit"])."\">".gettext("next")."</a>";
        }
        echo "</span>";
    }
    /* }}} */
    /* edit_post($id) {{{ */
    /**
     * show user a form to edit the post
     *
     * @param int $id The postid to edit, or 0 to create a new post
     *
     * @return void
     */
    public function edit_post($id) {
        if ($id==0) {
            $post["id"]                = 0;
            $post["title"]             = gettext("post title");
            $post["body"]              = "<p>".gettext("post body")."</p>";
            $post["date"]              = mktime();
            $post["categories_id"]     = 0;
            $post["dossier_id"]        = 0;
            $post["active"]            = 1;
            $post["public"]            = 1;
            $post["mail_comments"]     = 1;
            $post["allowanoncomments"] = ($this->settings["allowanoncomments"])?1:0;
            $post["postformat"]        = $this->settings["defaultpostformat"];
        } else {
            $res =& $this->db->query(sprintf("SELECT * FROM articles WHERE id = %d", $id));
            if (PEAR::isError($res)) {
                die($res->getMessage());
            }
            $post = $res->fetchRow(MDB2_FETCHMODE_ASSOC);
        }
        if (!array_key_exists("tb_uri", $post)) {
            $post["tb_uri"] = "";
        }
        if (!array_key_exists("aside", $post)) {
            $post["aside"] = 0;
        }

        //put all known categories in $this->categories array
        $this->get_categories();
        //put all known dossiers in $this->dossiers array
        $this->get_dossiers();
        //only HTML and BBCODE can be handled by tinymce
        if (in_array($post["postformat"], array("HTML", "BBC")) && $this->settings["wysiwyg"]) {
            $autoload = 1;
        } else {
            $autoload = 0;
        }
        ?>
		<script language="javascript" type="text/javascript" src="../tiny_mce/tiny_mce_gzip.js"></script>
		<script language="javascript" type="text/javascript">
			tinyMCE_GZ.init({
				plugins    : "spellchecker,inlinepopups,table,advhr,advimage,advlink,flash,paste,noneditable,contextmenu<?php echo ($post["postformat"] == "BBC") ? ",bbcode" : ""; ?>",
				themes     : "advanced",
				languages  : "en",
				disk_cache : true,
				debug      : false
			});
		</script>
		<?php
        echo sprintf(
            "<script language=\"javascript\" type=\"text/javascript\" src=\"../common/tinymce_conf.js.php?mode=%s&autoload=%d\"></script>\n",
            $post["postformat"], $autoload
        );
        ?>
		<script language="javascript" type="text/javascript" src="../common/tinymce_filemanager.js"></script>
		<form name="blogpost" method="post" action="index.php">
		<input type="hidden" name="action" value="save_post" />
		<input type="hidden" name="post[id]" value="<?php echo $post["id"]; ?>" />
		<input type="hidden" name="post[postformat]" value="<?php echo $post["postformat"]; ?>" />
		<div class="log_post">
			<div class="log_post_head">
				<h1 class="log_post_h1"><input type="text" id="title" name="post[title]" value="<?php echo stripslashes($post["title"]); ?>" /></h1>
					<?php echo gettext("dossier"); ?>
					<select name="post[dossier_id]">
						<option value="">--</option>
						<?php
                        foreach ($this->dossiers as $did=>$dossier) {
                            if ($did == 0) {
                                continue;
                            }
                            if ($post["dossier_id"] == $did) {
                                $selected = " SELECTED";
                            } else {
                                $selected = "";
                            }
                            echo "<option value=\"".$did."\" ".$selected.">".$dossier["name"]."</option>\n";
                        }
                        ?>
					</select>
			</div>
			<div class="log_post_body">
				<textarea id="editor_area" name="post[body]" rows="30" cols="50" style="width: 100%"><?php echo stripslashes($post["body"]); ?></textarea>
				<?php
                echo "<a href=\"#\" onclick=\"tinyMCE.execCommand('mceToggleEditor', false, 'editor_area');\">".gettext("Toggle WYSIWYG mode")."</a>";
                echo "<br />";
                echo gettext("to limit this post on the frontpage, enter ##BREAKPOINT## in your post.");
                ?>
				<br /><br />
				<h2 class="log_post_h2"><?php echo gettext("category"); ?>:</h2><br />
				<?php
                $i = 0;
                if (!array_key_exists("categories_ids", $post)) {
                    $post["categories_ids"] = "";
                }
                $selected_categories = explode(",", $post["categories_ids"]);
                foreach ($this->categories as $k=>$v) {
                    $i++;
                    if (!$k) {
                        continue;
                    }
                    if (in_array($k, $selected_categories)) {
                        $checked = "checked";
                    } else {
                        $checked = "";
                    }
                    echo "<input type=\"checkbox\" name=\"post[categories_ids][]\" value=\"".$k."\" ".$checked.">&nbsp;".$v["name"]."&nbsp;\n";
                    if ($i == 6) {
                        $i = 0;
                        echo "<br />";
                    }
                }
                ?>
				<br /><br />
				<?php echo gettext("date"); ?>:
				<select name="post[day]">
					<?php
                    for ($i=1;$i<=31;$i++) {
                        echo "<option value=\"".$i."\" ".($i==date("d", $post["date"])?" SELECTED":"").">".$i."</option>\n";
                    }
                    ?>
				</select>
				<select name="post[month]">
					<?php
                    for ($i=1;$i<=12;$i++) {
                        echo "<option value=\"".$i."\" ".($i==date("m", $post["date"])?" SELECTED":"").">".$i."</option>\n";
                    }
                    ?>
				</select>
				<select name="post[year]">
					<?php
                    for ($i=date("Y")-5;$i<=date("Y")+1;$i++) {
                        echo "<option value=\"".$i."\" ".($i==date("Y", $post["date"])?" SELECTED":"").">".$i."</option>\n";
                    }
                    ?>
				</select>
				<br />
				<?php echo gettext("send trackback info to"); ?>:
				<input type="text" name="post[tb_uri]" value="<?php echo $post["tb_uri"]; ?>" size="50" /><br />
				<input type="checkbox" value="1" name="post[mail_comments]"<?php if ($post["mail_comments"]) { echo " checked=\"checked\""; } ?> />&nbsp;<?php echo gettext("send comments as email to me."); ?><br />
				<input type="submit" value="<?php echo gettext("save"); ?>" />
				<?php if ($post["id"]) { ?>
					<input type="button" value="<?php echo gettext("delete"); ?>" onClick="document.forms.blogpost.action.value='delete_post';document.forms.blogpost.submit();" />
				<?php } else { ?>
					<input type="button" value="<?php echo gettext("cancel"); ?>" onClick="document.forms.blogpost.action.value='show_posts';document.forms.blogpost.submit();" />
				<?php } ?>
			</div>
			<div class="log_post_foot">
				<span class="log_post_date"><?php echo gettext("active"); ?>: <input type="checkbox" value="1" name="post[active]" <?php if ($post["active"]) { echo "checked=\"checked\""; } ?> /></span>
				<span class="log_post_author"><?php echo gettext("public"); ?>: <input type="checkbox" value="1" name="post[public]" <?php if ($post["public"]) { echo "checked=\"checked\""; } ?> /></span>
				<span class="log_post_author"><?php echo gettext("aside"); ?>: <input type="checkbox" value="1" name="post[aside]" <?php if ($post["aside"]) { echo "checked=\"checked\""; } ?> /></span>
				<span class="log_post_author"><?php echo gettext("anoncomments"); ?>: <input type="checkbox" value="1" name="post[allowanoncomments]" <?php if ($post["allowanoncomments"]) { echo "checked=\"checked\""; } ?> /></span>
			</div>
		</div>
		</form>
		<script language="Javascript" type="text/javascript">
			document.blogpost.title.focus();
		</script>
		<?php
    }
    /* }}} */
    /* save_post($post) {{{ */
    /**
     * store post in database
     *
     * @param array $post the post data
     *
     * @return void
     */
    public function save_post($post) {
        if (!array_key_exists("categories_ids", $post)) {
            $post["categories_ids"] = array();
        }
        if ($post["id"]) {
            $sql = sprintf("SELECT date,active FROM articles WHERE id = %d", $post["id"]);
            $r =& $this->db->query($sql);
            $orig_post = $r->fetchRow(MDB2_FETCHMODE_ASSOC);
            $query  = sprintf(
                "UPDATE articles SET %s = '%s', %s = '%s', %s = '%s', %s = %d, %s = %d, %s = %d, %s = %d, %s = %d, %s = %d",
                $this->db_quote("title"), $this->db_escape(strip_tags($post["title"])),
                $this->db_quote("body"),  $this->db_escape($this->_strip_tags($post["body"])),
                $this->db_quote("categories_ids"), implode(",", $post["categories_ids"]),
                $this->db_quote("active"), $post["active"],
                $this->db_quote("public"), $post["public"],
                $this->db_quote("aside"), (array_key_exists("aside", $post))?$post["aside"]:0,
                $this->db_quote("mail_comments"), (array_key_exists("mail_comments", $post))?$post["mail_comments"]:0,
                $this->db_quote("allowanoncomments"), (array_key_exists("allowanoncomments", $post))?$post["allowanoncomments"]:0,
                $this->db_quote("dossier_id"), (array_key_exists("dossier_id", $post))?$post["dossier_id"]:0
            );
            //if post was inactive, and now it's active, we don't update the "modified" fields in the database.
            if ($post["active"]) {
                if ($orig_post["active"]) {
                    $query .= sprintf(", %s = %d", $this->db_quote("last_modified"), mktime());
                    $query .= sprintf(", %s = %d", $this->db_quote("modified_by"), $_SESSION["author_id"]);
                }
            }
            //only update the date if it is not the same day as the posts original date
            if (date("d", $orig_post["date"]) != $post["day"] || date("m", $orig_post["date"]) != $post["month"] || date("Y", $orig_post["date"]) != $post["year"]) {
                $query .= sprintf(", %s = %d", $this->db_quote("date"), mktime(date("H"), date("i"), date("s"), $post["month"], $post["day"], $post["year"]));
            }
            if (!isset($tb_uri)) {
                $tb_uri = "";
            }
            $query .= sprintf(", ping_sent = 1, tb_uri = '%s'", $this->db_escape($tb_uri));
            $query .= sprintf(" WHERE id = %d", $post["id"]);
            $logmsg = sprintf("Article %d:%s updated", $post["id"], $post["title"]);
        } else {
            $query  = sprintf(
                "INSERT INTO articles (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                $this->db_quote("title"), $this->db_quote("body"), $this->db_quote("authors_id"), $this->db_quote("categories_ids"), $this->db_quote("date"), $this->db_quote("active"),
                $this->db_quote("public"), $this->db_quote("aside"), $this->db_quote("mail_comments"), $this->db_quote("ping_sent"), $this->db_quote("tb_uri"), $this->db_quote("allowanoncomments"), $this->db_quote("dossier_id"), $this->db_quote("postformat")
            );
            $query .= sprintf(
                "VALUES ('%s', '%s', %d, '%s', %d, %d, %d, %d, %d, %d, '%s', %d, %d, '%s')",
                $this->db_escape(strip_tags($post["title"])),
                $this->db_escape($this->_strip_tags($post["body"])),
                $_SESSION["author_id"],
                implode(",", $post["categories_ids"]),
                mktime(date("H"), date("i"), date("s"), $post["month"], $post["day"], $post["year"]),
                $post["active"], $post["public"], (array_key_exists("aside", $post))?$post["aside"]:0, $post["mail_comments"], 0,
                $this->db_escape($post["tb_uri"]), (array_key_exists("allowanoncomments", $post))?$post["allowanoncomments"]:0,
                (array_key_exists("dossier_id", $post))?$post["dossier_id"]:0,
                $post["postformat"]
            );
            $logmsg = sprintf("Article %s created", $post["title"]);
        }
        $res =& $this->db->exec($query);
        if (PEAR::isError($res)) {
            die($res->getUserInfo());
        }
        $this->log->add_log(mktime(), $_SESSION["author_id"], 1, $logmsg);
        if (!$post["id"]) {
            /* this is a new post. fetch old data */
            $sql = sprintf(
                "SELECT id FROM articles WHERE %s = '%s' AND %s = '%s' AND authors_id = %d AND categories_ids = '%s'",
                $this->db_quote("title"), $this->db_escape(strip_tags($post["title"])),
                $this->db_quote("body"),  $this->db_escape($this->_strip_tags($post["body"])),
                $_SESSION["author_id"], implode(",", $post["categories_ids"])
            );
            $res =& $this->db->query($sql);
            $temp = $res->fetchRow(MDB2_FETCHMODE_ASSOC);
            $post["id"] = $temp["id"];
            $new_post = 1;
        }
        if ($post["tb_uri"] && $new_post) {
            $post_uri = "http://".$_SERVER["SERVER_NAME"].substr($_SERVER["REQUEST_URI"],0,strpos($_SERVER["REQUEST_URI"], "/admin/index.php"))."/index.php?action=view/".$post["id"];
            include "HTTP/Request.php";
            $http =& new HTTP_Request($post["tb_uri"]);
            $http->setMethod(HTTP_REQUEST_METHOD_POST);
            $http->addPostData("title", $post["title"]);
            $http->addPostData("url", $post_uri);
            $http->addPostData("blog_name", "http://".$_SERVER["SERVER_NAME"].substr($_SERVER["REQUEST_URI"],0,strpos($_SERVER["REQUEST_URI"], "/admin/index.php"))."/");
            $http->AddPostData("excerpt", substr($this->_strip_tags($post["body"]), 100));
            $http->addPostData("charset", "UTF-8");
            if (!PEAR::isError($http->sendRequest())) {
                $response1 = $http->getResponseBody();
            } else {
                die("error");
            }
            $this->log->add_log(mktime(), $_SESSION["author_id"], 1, sprintf("Trackback for article %s sent to %s", $post["title"], $post["tb_uri"]));
            $sql = sprintf("UPDATE articles SET ping_sent = 1 WHERE id = %d", $post["id"]);
            $res =& $db->exec($sql);
        }
        header("Location: index.php?action=show_posts");
    }
    /* }}} */
    /* delete_post($id) {{{ */
    /**
     * delete a post from the db
     *
     * @param int $id The postid to remove
     *
     * @return void
     */
    public function delete_post($id) {
        $query = sprintf("DELETE FROM articles WHERE id = %d", $id);
        $res =& $this->db->exec($query);
        if (PEAR::isError($res)) {
            die($res->getUserInfo());
        }
        //delete comments for this post
        $query = sprintf("DELETE FROM comments WHERE articles_id = %d", $id);
        $res =& $this->db->exec($query);
        if (PEAR::isError($res)) {
            die($res->getUserInfo());
        }
        $this->log->add_log(mktime(), $_SESSION["author_id"], 1, sprintf("Article %d deleted", $id));
        header("Location: index.php?action=show_posts");
    }
    /* }}} */
    /* show_comments {{{ */
    /**
     * show comment items
     *
     * @param array $options top => where to start for pages, limit => the pagesize
     *
     * @return void
     */
    public function show_comments($options) {
        if (!is_array($options)) {
            $options = array();
        }
        if (!array_key_exists("top", $options)) {
            $options["top"] = 0;
        }  else {
            $options["top"] = (int)$options["top"];
        }
        if (!array_key_exists("limit", $options)) {
            $options["limit"] = 15;
        } else {
            $options["limit"] = (int)$options["limit"];
        }
        /* create an array with the article titles */
        $sql = "SELECT id,title FROM articles";
        $res = $this->db->query($sql);
        $articles[0] = gettext("no article");
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $articles[$row["id"]] = stripslashes($row["title"]);
        }
        /* loop through the comments */
        $res_count =& $this->db->query("SELECT COUNT(*) as numcomments FROM comments");
        $counter_r = $res_count->fetchRow(MDB2_FETCHMODE_ASSOC);
        $counter = $counter_r["numcomments"];
        $this->db->setLimit($options["limit"], $options["top"]);
        $res =& $this->db->query("SELECT * FROM comments ORDER BY date DESC");
        if (PEAR::isError($res)) {
            die($res->getMessage());
        }
        echo "<div class=\"log_post\"></div>";
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            if (!trim($row["title"])) {
                $row["title"] = gettext("no title");
            }
            ?>
			<div class="log_post">
				<div class="log_post_head">
					<h1 class="log_post_h1">
						<?php echo htmlspecialchars(stripslashes($row["title"])); ?> (<?php echo $articles[$row["articles_id"]]; ?>)
					</h1><br />
					<h2 class="log_post_h2"><?php echo gettext("name") . ": " . stripslashes(htmlspecialchars($row["name"])) . "&nbsp;" . gettext("ip") . ": " . $row["ip"]; ?></h2>
					<h2 class="log_post_h2"><?php echo gettext("website") . ": " . stripslashes(htmlspecialchars($row["website"])) . "&nbsp;" . gettext("email") . ": " . stripslashes(htmlspecialchars($row["email"])); ?></h2>
				</div>
				<div class="log_post_body">
					<div class="log_post_normal"><?php echo stripslashes(htmlspecialchars($row["comment"])); ?></div>
				</div>
				<div class="log_post_foot">
					<?php if (!$row["deleted"]) { ?>
						<a href="index.php?action=delete_comment&id=<?php echo $row["id"]; ?>"><?php echo gettext("delete"); ?></a>
					<?php } else { ?>
						<a href="index.php?action=recover_comment&id=<?php echo $row["id"]; ?>"><?php echo gettext("recover"); ?></a>
					<?php } ?>
				</div>
			</div>
			<?php
        }
        if ($options["top"]) {
            echo "<a class=\"link_prev\" href=\"index.php?action=show_comments&amp;options[top]=".($options["top"]-$options["limit"])."\">".gettext("previous")."</a> ";
        }
        if (($options["top"] + $options["limit"]) > $counter) {
            $end = $counter;
        } else {
            $end = ($options["top"]+$options["limit"]);
        }
        if ($counter == 0) {
            echo "<span class=\"log_cat\">".gettext("no comments");
        } else {
            echo "<span class=\"log_cat\">".gettext("showing")." ".($options["top"]+1)." ".gettext("to")." ".$end." ".gettext("of")." ".$counter." ".gettext("total comments");
        }
        if (($options["top"]+$options["limit"]) < $counter) {
            echo " <a class=\"link_next\" href=\"index.php?action=show_comments&amp;options[top]=".($options["top"]+$options["limit"])."\">".gettext("next")."</a>";
        }
        echo "</span>";
    }
    /* }}} */
    /* delete_comment {{{ */
    /**
     * Delete a comment from the database
     *
     * @param int $id The comment id to remove
     *
     * @return void
     */
    public function delete_comment($id) {
        $sql = sprintf("DELETE FROM comments WHERE id = %d", $id);
        $res = $this->db->exec($sql);
        $this->log->add_log(mktime(), $_SESSION["author_id"], 1, sprintf("Comment %d deleted", $id));
        header("Location: index.php?action=show_comments");
    }
    /* }}} */
    /* show_plugins {{{ */
    /**
     * Show all loaded plugins
     *
     * @return void
     */
    public function show_plugins() {
        /* table to sortout layout */
        $output  = "<table cellspacing=\"1\" cellpadding=\"2\" bgcolor=\"black\"><tr>\n";
        $output .= "<td bgcolor=\"white\" valign=\"top\">".gettext("name")."</td>\n";
        $output .= "<td bgcolor=\"white\" valign=\"top\">".gettext("active")."</td>\n";
        $output .= "<td bgcolor=\"white\" valign=\"top\">".gettext("info")."</td>\n";
        $output .= "<td bgcolor=\"white\" valign=\"top\">&nbsp;</td>\n";
        $output .= "</tr>";
        foreach ($this->plugman->plugins as $name=>$pluginobj) {
            $output .= "<tr>\n";
            $output .= "<td bgcolor=\"white\" valign=\"top\">$name</td>\n";
            $output .= "<td bgcolor=\"white\" valign=\"top\">";
            if ($pluginobj->active) {
                $output .= "yes";
            } else {
                $output .= "no";
            }
            $output .= "</td>\n";
            $output .= "<td bgcolor=\"white\" valign=\"top\">";
            $output .= "
				author: ".$pluginobj->author."<br>
				website: ".$pluginobj->website."<br>
				license: ".$pluginobj->license."<br>
				info:<br> ".$pluginobj->description."
			";
            $output .= "</td>\n";
            $output .= "<td bgcolor=\"white\" valign=\"top\">";
            if ($pluginobj->active) {
                $output .= "<a href=\"index.php?action=deactivate_plugin&plugin=".$name."\">deactivate</a><br>";
            } else {
                $output .= "<a href=\"index.php?action=activate_plugin&plugin=".$name."\">activate</a><br>";
            }
            if (in_array("show_settings", get_class_methods($pluginobj))) {
                $output .= "<a href=\"index.php?action=config_plugin&plugin=".$name."\">settings</a><br>";
            }
            $output .= "</td>\n";
            $output .= "</tr>\n";
        }
        $output .= "</table>\n";
        echo $output;
    }
    /* }}} */
    /* configure_plugin {{{ */
    /**
     * Run specified plugins 'show_settings' method.
     * If that does not exist (or is not declared 'public') the pluginlist will be shown
     *
     * @param string $plugin The plugin to configure
     *
     * @return void
     */
    public function configure_plugin($plugin) {
        if (in_array("show_settings", get_class_methods($this->plugman->plugins[$plugin]))) {
            $this->plugman->plugins[$plugin]->show_settings();
        } else {
            $this->show_plugins();
        }
    }
    /* }}} */
    /* edit_plugin_setting {{{ */
    /**
     * run $plugin->edit_setting so a plugin can show edit screen
     *
     * @param string $plugin       The plugin to run the edit_setting on
     * @param mixed  $request_data All the $_REQUEST data
     *
     * @return void
     */
    public function edit_plugin_setting($plugin, $request_data) {
        if (in_array("edit_setting", get_class_methods($this->plugman->plugins[$plugin]))) {
            $this->plugman->plugins[$plugin]->edit_setting($request_data);
        } else {
            $this->show_plugins();
        }
    }
    /* }}} */
    /* save_plugin_setting {{{ */
    /**
     * run $plugin->save_setting so a plugin can save a setting
     *
     * @param string $plugin       The plugin to run the save_setting on
     * @param mixed  $request_data All the $_REQUEST data
     *
     * @return void
     */
    public function save_plugin_setting($plugin, $request_data) {
        if (in_array("save_setting", get_class_methods($this->plugman->plugins[$plugin]))) {
            $this->plugman->plugins[$plugin]->save_setting($request_data);
        } else {
            $this->show_plugins();
        }
    }
    /* }}} */
    /* _activate_plugin {{{ */
    /**
     * Activate plugin and save state to db
     *
     * @param string $plugin The plugin name to activate
     *
     * @return bool true
     */
    public function _activate_plugin($plugin) {
        if (in_array($plugin, $this->active_plugins)) {
            return true;
        }
        $this->active_plugins[] = $plugin;
        $this->plugman->activate_plugin($plugin);
        $active_plugins = serialize($this->active_plugins);
        $this->save_settings(array("active_plugins" => $active_plugins));
        $this->log->add_log(mktime(), $_SESSION["author_id"], 1, sprintf("Plugin %s activated", $plugin));
        return true;
    }
    /* }}} */
    /* _deactivate_plugin {{{ */
    /**
     * Deactivate plugin and save state to db
     *
     * @param string $plugin The plugin name to deactivate
     *
     * @return bool true
     */
    public function _deactivate_plugin($plugin) {
        if (!in_array($plugin, $this->active_plugins)) {
            return true;
        }
        unset($this->active_plugins[array_search($plugin, $this->active_plugins)]);
        $this->plugman->deactivate_plugin($plugin);
        $active_plugins = serialize($this->active_plugins);
        $this->save_settings(array("active_plugins" => $active_plugins));
        $this->log->add_log(mktime(), $_SESSION["author_id"], 1, sprintf("Plugin %s de-activated", $plugin));
        return true;
    }
    /* }}} */
    /* show_settings {{{ */
    /**
     * show the blogsettings with edit boxen
     *
     * @return void
     */
    public function show_settings() {
        /* get settings from db */
        $settings = $this->settings;
        ?>
		<div class="log_post">
			<div class="log_post_head">
				<h1 class="log_post_h1">
				<?php echo gettext("Blogsettings"); ?>
				</h1>
			</div>
			<div class="log_post_body">
				<div class="log_post_normal">
					<form name="settings" method="post" action="index.php">
					<input type="hidden" name="action" value="save_settings" />
					<table border="0" cellspacing="0" cellpadding="0"><tr>
						<td style="vertical-align: top;"><?php echo gettext("blog title"); ?></td>
						<td><input type="text" style="width: 200px;" name="settings[blogtitle]" value="<?php echo stripslashes($settings["blogtitle"]); ?>" /></td>
					</tr><tr>
						<td style="vertical-align: top;"><?php echo gettext("blog/meta description"); ?>:</td>
						<td><textarea name="settings[blogdescription]" style="width: 200px; height: 100px;"><?php echo stripslashes($settings["blogdescription"]); ?></textarea></td>
					</tr><tr>
						<td style="vertical-align: top;"><?php echo gettext("meta keywords"); ?></td>
						<td>
							<textarea name="settings[blogkeywords]" style="width: 200px; height: 100px;"><?php echo stripslashes($settings["blogkeywords"]); ?></textarea>
						</td>
					</tr><tr>
						<td style="vertical-align: top;"><?php echo gettext("posts per page"); ?></td>
						<td>
							<select name="settings[postsperpage]">
								<option value="10"<?php if ($settings["postsperpage"] == 10) { echo " SELECTED"; } ?>>10</option>
								<option value="20"<?php if ($settings["postsperpage"] == 20) { echo " SELECTED"; } ?>>20</option>
								<option value="50"<?php if ($settings["postsperpage"] == 50) { echo " SELECTED"; } ?>>50</option>
								<option value="75"<?php if ($settings["postsperpage"] == 75) { echo " SELECTED"; } ?>>75</option>
								<option value="100"<?php if ($settings["postsperpage"] == 100) { echo " SELECTED"; } ?>>100</option>
							</select>
						</td>
					</tr><tr>
						<td><?php echo gettext("enable anonymous comments"); ?></td>
						<td>
							<select name="settings[allowanoncomments]">
								<option value="0"><?php echo gettext("no"); ?></option>
								<option value="1"<?php if ($settings["allowanoncomments"] == 1) { echo " SELECTED"; } ?>><?php echo gettext("yes"); ?></option>
							</select>
						</td>
					</tr><tr>
						<td><?php echo gettext("enable clean urls"); ?></td>
						<td>
							<select name="settings[cleanurl]">
								<option value="0"><?php echo gettext("no"); ?></option>
								<option value="1"<?php if ($settings["cleanurl"] == 1) {
    echo " SELECTED";
}
?>><?php echo gettext("yes"); ?></option>
							</select>
						</td>
					</tr><tr>
						<td><?php echo gettext("blog webaddress"); ?></td>
						<td><input type="text" style="width: 200px;" name="settings[bloglocation]" value="<?php echo (array_key_exists("bloglocation", $settings)) ? stripslashes($settings["bloglocation"]) : ""; ?>" />
					</tr><tr>
						<td><?php echo gettext("blog language"); ?></td>
						<td>
							<select name="settings[language]">
								<?php
                                foreach ($this->languages as $k=>$v) {
                                    echo "<option value=\"$k\"";
                                    if ($this->lang == $k) {
                                        echo " SELECTED";
                                    }
                                    echo ">".gettext($v)."</option>";
                                }
                                ?>
							</select>
						</td>
					</tr><tr>
						<td><?php echo gettext("show category icons"); ?></td>
						<td>
							<select name="settings[show_cat_icons]">
								<option value="0"><?php echo gettext("no"); ?></option>
								<option value="1"<?php if ($settings["show_cat_icons"] == 1) {
    echo " SELECTED";
}
?>><?php echo gettext("yes"); ?></option>
							</select>
						</td>
					</tr><tr>
						<td><?php echo gettext("default markup format"); ?></td>
						<td>
							<select name="settings[defaultpostformat]">
								<option value="HTML"<?php if ($settings["defaultpostformat"] == "HTML") {
    echo " selected=\"selected\"";
}
?>>HTML</option>
								<option value="BBC"<?php if ($settings["defaultpostformat"] == "BBC") {
    echo " selected=\"selected\"";
}
?>>BBCode</option>
								<option value="MW"<?php if ($settings["defaultpostformat"] == "MW") {
    echo " selected=\"selected\"";
}
?>>MediaWiki</option>
							</select>
						</td>
					</tr><tr>
						<td><?php echo gettext("use WYSIWYG editor"); ?></td>
						<td>
							<select name="settings[wysiwyg]">
								<option value="0"><?php echo gettext("no"); ?></option>
								<option value="1"<?php if ($settings["wysiwyg"] == 1) {
    echo " selected=\"selected\"";
}
?>><?php echo gettext("yes"); ?></option>
							</select>
						</td>
					</tr><tr>
						<td colspan="2">&nbsp;</td>
					</tr><tr>
						<td>&nbsp;</td>
						<td><input type="submit" value="<?php echo gettext("save"); ?>" /></td>
					</tr></table>
					</form>
				</div>
			</div>
			<div class="log_post_foot">
			</div>
		</div>
		<?php
	}
	/* }}} */
	/* save_settings {{{ */
	/**
	 * save settings to db
	 *
	 * @param array $settings The form info from the show_blogsettings form
	 * @todo Describe settings layout
	 *
	 * @return null|boolean true
	 */
	public function save_settings($settings) {
		$queries = array();
		$settings_db = array();
		/* get current settings */
		$sql = "SELECT * FROM settings";
		$res = & $this->db->query($sql);
		while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
			$settings_db[$row["settingname"]]["id"]  = $row["id"];
			$settings_db[$row["settingname"]]["val"] = $row["settingvalue"];
		}
		/* construct queries to sync database with userinput */
		foreach ($settings as $key=>$val) {
			if (array_key_exists($this->db_escape($this->_strip_tags($key)), $settings_db)) {
				//setting was there, we need to UPDATE it
				$queries[] = sprintf("UPDATE settings SET settingvalue = '%s' WHERE settingname = '%s'", $this->db_escape($this->_strip_tags($val)), $this->db_escape(strip_tags($key)));
			} else {
				//setting was not there, we need to INSERT it
				$queries[] = sprintf("INSERT INTO settings (settingname, settingvalue) VALUES ('%s','%s')", $this->db_escape(strip_tags($key)), $this->db_escape($this->_strip_tags($val)));
			}
		}
		/* run queries against db */
		foreach ($queries as $q) {
			$res = & $this->db->exec($q);
			if (PEAR::isError($res)) {
				die($res->getUserInfo());
			}
		}
		/* repopulate the settings stuff */
		$this->get_settings();
		return true;
	}
	/* }}} */
	/* show_menuitems {{{ */
	/**
	 * show custom menu items
	 *
	 * @return void
	 */
	public function show_menuitems() {
		/* get max pos from db and add 1 for the default pos in new item */
		$sql = "SELECT MAX(sortorder) FROM menulinks";
		$res = & $this->db->query($sql);
		$row = $res->fetchRow();
		$nextsort = (int)$row[0] + 1;
		?>
		<div class="log_post">
			<div class="log_post_head">
				<h1 class="log_post_h1">
					<?php echo gettext("Menu items"); ?>
				</h1>
			</div>
			<div class="log_post_body">
				<div class="log_post_normal">
					<form name="settings" id="settingsform" method="post" action="index.php">
					<input type="hidden" name="action" value="save_menuitems" />
					<?php
                    foreach ($this->menuitems as $id => $values) {
                        ?>
						<table border="0" cellspacing="1" cellpadding="0"><tr>
							<td><?php echo gettext("name"); ?></td>
							<td><input style="width: 300px;" type="text" name="link[<?php echo $id; ?>][linktitle]" value="<?php echo stripslashes($values["linktitle"]); ?>" /></td>
						</tr><tr>
							<td><?php echo gettext("link"); ?></td>
							<td><input style="width: 300px;" type="text" name="link[<?php echo $id; ?>][url]" value="<?php echo stripslashes($values["url"]); ?>" /></td>
						</tr><tr>
							<td><?php echo gettext("image"); ?></td>
							<td><input style="width: 300px;" type="text" name="link[<?php echo $id; ?>][image]" value="<?php echo stripslashes($values["image"]); ?>" /></td>
						</tr><tr>
							<td><?php echo gettext("position"); ?></td>
							<td>
								<input style="width: 30px;" type="text" name="link[<?php echo $id; ?>][sortorder]" value="<?php echo (int)$values["sortorder"]; ?>" /><br />
								<input type="button" name="del" value="<?php echo gettext("delete"); ?>" onclick="document.getElementById('state_<?php echo $id; ?>').value='delete'; document.getElementById('settingsform').submit();">
							</td>
						</tr></table>
						<input type="hidden" id="state_<?php echo $id; ?>" name="link[<?php echo $id; ?>][state]" value="save" />
						<hr>
						<?php
                    }
                    ?>
					<table border="0" cellspacing="1" cellpadding="0"><tr>
						<td><?php echo gettext("name"); ?></td>
						<td><input style="width: 300px;" type="text" name="link[0][linktitle]" value="" /></td>
					</tr><tr>
						<td><?php echo gettext("link"); ?></td>
						<td><input style="width: 300px;" type="text" name="link[0][url]" value="" /></td>
					</tr><tr>
						<td><?php echo gettext("image"); ?></td>
						<td><input style="width: 300px;" type="text" name="link[0][image]" value="" /></td>
					</tr><tr>
						<td><?php echo gettext("position"); ?></td>
						<td>
							<input style="width: 30px;" type="text" name="link[0][sortorder]" value="<?php echo $nextsort; ?>" />
						</td>
					</tr></table>
					<input type="hidden" id="state_0" name="link[0][state]" value="save" />
					<hr>
					<input type="submit" value="<?php echo gettext("save all"); ?>" />
					</form>
				</div>
			</div>
		</div>
		<?php
    }
    /* }}} */
    /* save_menuitems {{{ */
    /**
     * store/remove database entries.
     * The links param has the following layout:
     *    [0] => Array
     *   (
     *       [linktitle] => title
     *       [url] => url
     *       [image] => image url (optional)
     *       [sortorder] => 1
     *       [state] => save
     *   )
     *   The toplevel key is the id of the link, or 0 for a new one.
     *
     * @param array $links menu items to store in database, see the function description for layout
     *
     * @return void
     */
    public function save_menuitems($links) {
        /* walk through all the links */
        if (is_array($links)) {
            foreach ($links as $k=>$v) {
                /* new item always has $k == 0 */
                if ($k == 0) {
                    if (strlen(trim($v["linktitle"])) && strlen(trim($v["url"]))) {
                        $sql  = "INSERT INTO menulinks (url, linktitle, image, sortorder) VALUES ";
                        $sql .= sprintf(
                            "('%s', '%s', '%s', %d)",
                            $this->db_escape(strip_tags($v["url"])),
                            $this->db_escape(strip_tags($v["linktitle"])),
                            $this->db_escape(strip_tags($v["image"])),
                            $v["sortorder"]
                        );
                        $res = $this->db->exec($sql);
                    }
                } else {
                    /* from db. this can be either be marked as delete or save */
                    if ($v["state"] == "delete") {
                        $sql = sprintf("DELETE FROM menulinks WHERE id = %d", $k);
                        $res = $this->db->exec($sql);
                    } elseif ($v["state"] == "save") {
                        $sql  = "UPDATE menulinks SET ";
                        $sql .= sprintf("url = '%s'", $this->db_escape(strip_tags($v["url"])));
                        $sql .= sprintf(", linktitle = '%s'", $this->db_escape(strip_tags($v["linktitle"])));
                        $sql .= sprintf(", image = '%s'", $this->db_escape(strip_tags($v["image"])));
                        $sql .= sprintf(", sortorder = %d", $v["sortorder"]);
                        $sql .= sprintf(" WHERE id = %d", $k);
                        $res = $this->db->exec($sql);
                    }
                }
            }
        }
        header("Location: index.php?action=show_menuitems");
    }
    /* }}} */
    /* show_import {{{ */
    /**
     * Show the import module screen with links to all the supported blogtools
     *
     * @return void
     */
    public function show_import() {
        $import = new MvBlog_import();
        $import->show_options();
    }
    /* }}} */
    /* import {{{ */
    /**
     * Run the blogtool import routine.
     *
     * @param string $type    The blogtool to import from. Can only be 'wordpress' right now
     * @param array  $options The additional parameters to pass to the specific import plugin called by run_module()
     *
     * @return void
     */
    public function import($type, $options = array()) {
        $import = new MvBlog_import();
        $import->run_module($type, $options);
    }
    /* }}} */
    /* show_userlog {{{ */
    /**
     * Show a log of what happened
     *
     * @return void
     */
    public function show_userlog() {
        $this->log->show_log(2);
    }
    /* }}} */
    /* show_adminlog {{{ */
    /**
     * Show a log of what happened
     *
     * @return void
     */
    public function show_adminlog() {
        $this->log->show_log(1);
    }
    /* }}} */
}
?>
