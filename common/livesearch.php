<?php
/**
     * MvBlog -- An open source no-nosense blogtool
     *
     * Copyright (C) 2005-2008, Michiel van Baak
     * Michiel van Baak <mvanbaak@users.sourceforge.net>
     *
     * See http://dev.mvblog.org for more information on MvBlog.
     * That page also provides Bugtrackers, Filereleases etc.
     *
     * This program is free software, distributed under the terms of
     * the GNU General Public License Version 2. See the LICENSE file
     * at the top of the source tree.
     *
     * PHP version 5
     *
     * @category  PHP
     * @package   MvBlog
     * @author    Michiel van Baak <mvanbaak@users.sourceforge.net>
     * @copyright 2005-2008 Michiel van Baak
     * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
     * @version   SVN: $Revision: 809 $
     * @link      http://www.mvblog.org
     */

/**
 * Livesearch script
 */
require "mvblog.php";
$mvblog = new MvBlog();
//get all the authors in an array
$res = & $mvblog->db->query("SELECT * FROM authors");

if (PEAR::isError($res)) {
    die($res->getMessage());
}

while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
    $authors[$row["id"]]["fullname"] = $row["fullname"];
    $authors[$row["id"]]["email"]    = $row["email"];
}

$searchstring = urldecode($_REQUEST["s"]);
$query  = "SELECT id,title FROM articles WHERE public=1 AND active=1 AND aside=0 AND";
$query .= sprintf(
    " (upper(title) LIKE '%%%s%%' OR upper(head) LIKE '%%%s%%' OR upper(body) LIKE '%%%s%%')",
    preg_quote(strip_tags(strtoupper($searchstring)), "'"),
    preg_quote(strip_tags(strtoupper($searchstring)), "'"),
    preg_quote(strip_tags(strtoupper($searchstring)), "'")
);
$query .= " ORDER BY date DESC";

$mvblog->db->setLimit(5);
$res = & $mvblog->db->query($query);
$ret = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
$ret .= "<channel>\n";
$ret .= "\t<title>search results</title>\n";

while ($row = $res->fetchRow()) {
    if (!trim($row[1])) {
        $row[1] = "[no title]";
    }
    $ret .= "\t<item>\n";
    $ret .= "\t\t<articleID>".$row[0]."</articleID>\n";
    $ret .= "\t\t<articleTitle>".stripslashes($row[1])."</articleTitle>\n";
    $ret .= "\t</item>\n";
}
$ret .= "</channel>\n";
header("Content-Type: text/xml");
echo $ret;
?>
