<?php
/**
 * MvBlog -- An open source no-nosense blogtool
 *
 * Copyright (C) 2005-2008, Michiel van Baak
 * Michiel van Baak <mvanbaak@users.sourceforge.net>
 *
 * See http://dev.mvblog.org for more information on MvBlog.
 * That page also provides Bugtrackers, Filereleases etc.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 * PHP version 5
 *
 * @category  PHP
 * @package   MvBlog
 * @author    Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright 2005-2008 Michiel van Baak
 * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @version   SVN: $Revision: 809 $
 * @link      http://www.mvblog.org
 */

/**
 * Class to read an ini file
 *
 * @category  PHP
 * @package   MvBlog
 * @author    Ferry Boender <void@example.com>
 * @copyright 2005-2008 Ferry Boender
 * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @version   Release: %%VERSION%%
 * @link      http://www.mvblog.org
 */
class MvBlog_IniFileReader extends MvBlog_IniParser {
	/**
	 * @var string $filename The ini file to read
	 */
	protected $filename;

	/**
	 * Class constructor that reads the ini file
	 *
	 * @param array  $availSettings Array with available settings
	 * @param string $filename      The ini file to read.
	 *
	 * @return void
	 */
	public function __construct($availSettings, $filename) {
		parent::__construct($availSettings);
		$this->filename = $filename;
		$contents = $this->read($this->filename);
		$settings = $this->parse($contents);
		$this->setSettings($settings);
	}

	/**
	 * Read the contents of the inifile
	 *
	 * @param string $filename The inifile to read
	 *
	 * @return string The contents of the inifile
	 */
	protected function read($filename) {
		if (($contents = @file_get_contents($filename)) === false) {
			throw new MvBlog_IniFileReaderException(1, $filename);
		}

		return($contents);
	}

	/**
	 * Custom parser so we get file &line numbers
	 *
	 * @param string $contents inifile contents
	 *
	 * @return array Assoc array with section, key and value
	 */
	protected function parse($contents) {
		$settings = array();

		// Find and replace dos line endings
		$contents = str_replace("\r\n", "\n", $contents);
		// Find and replace mac line endings
		$contents = str_replace("\r", "\n", $contents);

		$contents = explode("\n", $contents);
		$section = "";
		for ($i = 0; $i < count($contents); $i++) {
			// Look for a section
			if (preg_match('/^\[(.*?)\]*$/', $contents[$i], $match)) {
				$section = $match[1];
			}

			if (!preg_match("/^\s*#.*$/", $contents[$i])
			    && preg_match('/^\s*(.*?)\s*=\s*(.*?)\s*$/', $contents[$i], $match)
			) {
				$key = $match[1];
				$value = $match[2];
				$settings[$section][] = array("key" => $key, "value" => $value, "line" => $i);
			}
		}

		return($settings);
	}

	/**
	 * Set settings to class variable
	 *
	 * @param array $settings The settings as returned by the parser
	 *
	 * @return void
	 */
	protected function setSettings($settings) {
		foreach ($settings as $section => $data) {
			foreach ($data as $setting) {
				$key = $setting["key"];
				$value = $setting["value"];
				$file = $this->filename;
				$line = $setting["line"];

				try {
					$this->setSetting($section, $key, $value);
				} catch (MvBlog_IniFileReaderException $e) {
					switch($e->getCode()) {
					case 1:
						throw new MvBlog_IniFileReaderException(2, $key, $section, $file, $line);
						break;
					case 2:
						$type = MvBlog_IniParser::$typeNames[$this->availSettings[$section][$key]["type"]];
						throw new MvBlog_IniFileReaderException(3, $value, $key, $type, $section, $file, $line);
						break;
					case 3:
						throw new MvBlog_IniFileReaderException(4, $key, $value, $section, $file, $line);
						break;
					default:
						throw $e;
						break;
					}
				}
			}
		}
	}
}
?>
