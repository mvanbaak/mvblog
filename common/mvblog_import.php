<?php
/**
 * MvBlog -- An open source no-nosense blogtool
 *
 * Copyright (C) 2005-2008, Michiel van Baak
 * Michiel van Baak <mvanbaak@users.sourceforge.net>
 *
 * See http://dev.mvblog.org for more information on MvBlog.
 * That page also provides Bugtrackers, Filereleases etc.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 * PHP version 5
 *
 * @category  PHP
 * @package   MvBlog
 * @author    Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright 2005-2008 Michiel van Baak
 * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @version   SVN: $Revision: 809 $
 * @link      http://www.mvblog.org
 */

/**
 * Class that holds methods that can be used to import other blogs
 *
 * @category  PHP
 * @package   MvBlog
 * @author    Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright 2005-2008 Michiel van Baak
 * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @version   Release: %%VERSION%%
 * @link      http://www.mvblog.org
 */
Class MvBlog_import extends MvBlog_common {
	/* constants */

	/* variables */
	/**
	 * The supported blogtools
	 * @var array
	 */
	public $supported_blogs;

	/* methods */
	/* __construct {{{ */
	/**
	 * Class constructor that populates the supported_blogs variable.
	 *
	 * @todo read the supported blogtools from classes available
	 *
	 * @return void
	 */
	public function __construct() {
		$this->supported_blogs = array(
			"wordpress",
		);
	}
	/* }}} */
	/* show_options {{{ */
	/**
	 * Show a list with all supported blogtools with a link to the module main info page
	 *
	 * @return void
	 */
	public function show_options() {
		echo "Here are the supported import scripts. Please click on one.<br />";
		foreach ($this->supported_blogs as $v) {
			echo "<a href=\"index.php?action=import&amp;type=".$v."\">".$v."</a><br />";
		}
	}
	/* }}} */
	/* run_module {{{ */
	/**
	 * Create an object for the selected blogtool import
	 *
	 * @param string $type    The blogtool to import from. Can be 'wordpress' only for now
	 * @param array  $options Additional options to pass to the import module
	 *
	 * @return bool true on success, false on failure
	 */
	public function run_module($type, $options = array()) {
		if (!in_array($type, $this->supported_blogs)) {
			echo gettext("Unsupported import type");
			return false;
		}
		$class = "MvBlog_import_".$type;
		$importtype = new $class($options);
	}
	/* }}} */
}
?>
