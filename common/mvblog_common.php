<?php
/**
 * MvBlog -- An open source no-nosense blogtool
 *
 * Copyright (C) 2005-2008, Michiel van Baak
 * Michiel van Baak <mvanbaak@users.sourceforge.net>
 *
 * See http://dev.mvblog.org for more information on MvBlog.
 * That page also provides Bugtrackers, Filereleases etc.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 * PHP version 5
 *
 * @category  PHP
 * @package   MvBlog
 * @author    Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright 2005-2008 Michiel van Baak
 * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @version   SVN: $Revision$
 * @link      http://www.mvblog.org
 */

/**
 * Class that holds methods that can be used by every part of mvblog
 *
 * @category  PHP
 * @package   MvBlog
 * @author    Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright 2005-2008 Michiel van Baak
 * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @version   Release: %%VERSION%%
 * @link      http://www.mvblog.org
 */
Class MvBlog_common {
	/* constants */

	/* variables */
	/* {{{ */
	/**
	 * @var object $db The PEAR::DB connected database
	 */
	public $db;
	/**
	 * @var string $version The current program version
	 */
	public $version    = "%%VERSION%%";
	/**
	 * @var array $plugins Array with registered plugins.
	 * The array looks like this:
	 * <pre>
	 * array (
	 *   [$type] => "name",
	 *   [$type] => "name"
	 * )
	 * </pre>
	 * Where type can be:
	 * - text_output
	 */
	public $plugins    = array();
	/**
	 * @var array $authors Array with all active authors.
	 * The array looks like this:
	 * <pre>
	 * array (
	 *   [id] => "fullname",
	 *   [id] => "fullname"
	 * )
	 * </pre>
	 */
	public $authors    = array();
	/**
	 * @var array $users Array with all active users.
	 * The array looks like this:
	 * <pre>
	 * array (
	 *   [id] => "fullname",
	 *   [id] => "fullname"
	 * )
	 * </pre>
	 */
	public $users      = array();
	/**
	 * @var array $categories Array with all active categories.
	 * The array looks like this:
	 * <pre>
	 * array (
	 *   [id] => array(
	 *     "id" => id,
	 *     "login" => loginname,
	 *     "password" => password,
	 *     "email" => email address,
	 *     "fullname" => userfriendly name,
	 *     "active" => 1 if active, 0 or null otherwise,
	 *     "website" => full url to authors website
	 *   ),
	 *   [id] => ....
	 * )
	 * </pre>
	 */
	public $categories = array();
	/**
	 * @var array $dossiers Array with all the dossiers.
	 * The array looks like this:
	 * <pre>
	 * array (
	 *   [id] => array(
	 *      "id" => id,
	 *      "name" => name
	 *   ),
	 *   [id] => ...
	 * )
	 * </pre>
	 */
	public $dossiers = array();
	/**
	 * @var array $settings Array with all active settings.
	 * The array looks like this:
	 * <pre>
	 * array (
	 *   [settingname] => "settingvalue",
	 *   [settingname] => "settingvalue"
	 * )
	 * </pre>
	 * Current settingnames:
	 * - blogtitle
	 * - blogdescription
	 * - blogkeywords
	 * - postsperpage
	 * - allowanoncomments
	 * - cleanurl
	 */
	public $settings   = array();
	/**
	 * @var array $menuitems The user configured menuitems
	 */
	public $menuitems  = array();
	/**
	 * @var string $webroot The webroot for current blog
	 */
	public $webroot    = "";
	public $plugman;
	public $active_plugins = array();
	public $lang;

	/* }}} */
	/* methods */
	/* __construct {{{ */
	/**
	 * Setup stuff and handle settings etc and populate the data containers.
	 *
	 * If one of the default values for the function parameters is changed, 
	 * please also change the call in mvblog_upgrade constructor.
	 *
	 * @param string $plugindir Directory where the plugins are.
	 * @param int    $adminmode Must be 1 if in the admin interface.
	 * @param int    $upgrade   If set to 1 the data containers will not be populated to allow upgrades to them.
	 */
	public function __construct($plugindir="plugins/", $adminmode=0, $upgrade=0) {
		/* start session and output buffering */
		if (session_id() == "") {
			session_start();
		}
		ob_start();

		/* handle php bugs with globals overwrite */
		$this->handle_php_bugs();

		/* handle magic quotes */
		$this->handle_magic_quotes();

		/* get settings from ini file */
		if (array_key_exists("config", $GLOBALS)) {
			$inisettings = $GLOBALS["config"]->getSettings();
		} else {
			$inisettings = array();
		}

		if (!array_key_exists("database", $inisettings)) {
			$inisettings["database"] = "";
		}

		/* init database connection */
		$this->init_db($inisettings["database"]);

		/* populate the settings array */
		$this->get_settings();

		/* set the language etc */
		putenv("LANG=".$this->lang);
		setlocale(LC_ALL, $this->lang);
		$domain = 'messages';
		if ($adminmode) {
			bindtextdomain($domain, "../locale");
		} else {
			bindtextdomain($domain, "locale");
		}
		textdomain($domain);

		/* set the timezone (needed for php5.1 and newer) */
		if (function_exists("date_default_timezone_get") && function_exists("date_default_timezone_set")) {
			$tz = @date_default_timezone_get();
			date_default_timezone_set($tz);
		}

		/* get base href */
		$this->get_webroot();

		/* populate the active plugins array */
		$this->get_active_plugins();

		if (!$upgrade) {
			/* populate the authors array */
			$this->get_authors();

			/* populate the users array */
			$this->get_users();

			/* populate the categories array */
			$this->get_categories();

			/* populate the dossiers array */
			$this->get_dossiers();

			/* populate the menuitems array */
			$this->get_menuitems();
		}

		/* plugin handling */
		include_once "plugins.php";
		$plugman = new MvBlog_pluginmgr($plugindir, $this);
		$plugman->set_active_plugins($this->active_plugins);
		$this->plugman = $plugman;
	}
	/* }}} */
	/* data manipulation methods */
	/* handle_php_bugs {{{ */
	/**
	 * Handle some php bugs.
	 *
	 * There's some weird bugs when register_globals is on.
	 * You can clear them with stuff like this: ?GLOBALS&GLOBALS[bla]=test
	 * So what we do is detect this and bail out.
	 * We also make sure that if register_globals is on the gpc stuff will be removed from the globals stuff
	 *
	 * @return void
	 */
	protected function handle_php_bugs() {
		/**
		 * catch "PHP5 Globals Vulnerability".
		 * code taken from Advisory http://www.ush.it/2006/01/25/php5-globals-vulnerability/
		 */
		if (isset($HTTP_POST_VARS['GLOBALS']) || isset($_POST['GLOBALS']) || isset($HTTP_POST_FILES['GLOBALS'])
		    || isset($_FILES['GLOBALS']) || isset($HTTP_GET_VARS['GLOBALS']) || isset($_GET['GLOBALS'])
		    || isset($HTTP_COOKIE_VARS['GLOBALS']) || isset($_COOKIE['GLOBALS'])
		) {
			die("GLOBAL GPC hacking attemt!");
		}
		/**
		 * if register_globals is on, you cannot turn it off with ini_set.
		 * The vars will be registered before the ini_set is executed.
		 * We can fake register_globals is off by removing the GPCFR keys from
		 * the global var space :) I got the idea from Alan Hogan with his comment on php.net ini_set function docs.
		 * I rewrote it to match mvblog codestyle
		 */
		$register_globals = ini_get("register_globals");
		if ($register_globals === false || strtolower($register_globals) == "off" || $register_globals == "") {
			return;
		}

		foreach ($_GET as $key => $value) {
			if (preg_match("/^([a-z]|_){1}([a-z0-9]|_)*$/si", $key)) {
				unset($GLOBALS[$key]);
			}
		}

		foreach ($_POST as $key => $value) {
			if (preg_match('/^([a-zA-Z]|_){1}([a-zA-Z0-9]|_)*$/', $key)) {
				unset($GLOBALS[$key]);
			}
		}

		foreach ($_COOKIE as $key => $value) {
			if (preg_match('/^([a-zA-Z]|_){1}([a-zA-Z0-9]|_)*$/', $key)) {
				unset($GLOBALS[$key]);
			}
		}

		foreach ($_FILES as $key => $value) {
			if (preg_match('/^([a-zA-Z]|_){1}([a-zA-Z0-9]|_)*$/', $key)) {
				unset($GLOBALS[$key]);
			}
		}

		foreach ($_REQUEST as $key => $value) {
			if (preg_match('/^([a-zA-Z]|_){1}([a-zA-Z0-9]|_)*$/', $key)) {
				unset($GLOBALS[$key]);
			}
		}
	}
	/* }}} */
	/* handle_magic_quotes {{{ */
	/**
	 * Detect wether magic_quotes_gpc is on.
	 * If so, it will disable it and get rid of all the automagically added slashes etc
	 *
	 * @return void
	 */
	protected function handle_magic_quotes() {
		/* check for magic_quotes_gpc. If on, remove the escape slashes */
		set_magic_quotes_runtime(0);
		if (get_magic_quotes_gpc() || ini_get("magic_quotes_sybase")) {
			$_GET     = $this->magic_quotes_strip($_GET);
			$_POST    = $this->magic_quotes_strip($_POST);
			$_COOKIE  = $this->magic_quotes_strip($_COOKIE);
			$_REQUEST = array_merge($_GET, $_POST, $_COOKIE);
			$_FILES   = $this->magic_quotes_strip($_FILES);
			$_ENV     = $this->magic_quotes_strip($_ENV);
			$_SERVER  = $this->magic_quotes_strip($_SERVER);
		}
	}
	/* }}} */
	/* magic_quotes_strip() {{{ */
	/**
	 * strip escape \ signs when magic_quotes_gpc is on in php.ini
	 *
	 * @param mixed $mixed the input string or array
	 *
	 * @return mixed the string/array with the magic_quotes_gpc added slashes removed
	 */
	protected function magic_quotes_strip($mixed) {
		if (is_array($mixed)) {
			return array_map(array($this, "magic_quotes_strip"), $mixed);
		}
		return stripslashes($mixed);
	}
	/* }}} */
	/* init_db {{{ */
	/**
	 * Create database connection using the PEAR::MDB2 framework and puts this object in class variable db
	 *
	 * @param array $settings Database settings like type, database, hostname, username and password
	 *
	 * @return void
	 */
	protected function init_db($settings = "") {
		include_once "MDB2.php";
		//sqlite has a different scheme, because it's filebased
		if ($settings["type"] == "sqlite") {
			if (is_dir("db")) {
				$dsn = sprintf("%s:///db/%s.db?mode=0666", $settings["type"], $settings["database"]);
			} elseif (is_dir("../db")) {
				$dsn = sprintf("%s:///../db/%s.db?mode=0666", $settings["type"], $settings["database"]);
			}
		} else {
			$dsn = sprintf(
				"%s://%s:%s@tcp(%s)/%s",
				$settings["type"], $settings["username"], $settings["password"],
				$settings["hostname"], $settings["database"]
			);
		}

		$options = array(
			"debug"       => 2,
			"portability" => MDB2_PORTABILITY_ALL,
		);

		$db =& MDB2::connect($dsn, $options);
		if (PEAR::isError($db)) {
			die($db->getMessage());
		}
		$this->db = $db;
	}
	/* }}} */
	/* sanitize {{{ */
	/**
	 * Sanitize given data.
	 *
	 * This function will return an array if array was given, otherwise it will return the input.
	 * The array contents or the given string will be sanitized based on the optional options array.
	 * Key's with names that are outside a-zA-Z0-9_- will be removed.
	 *
	 * The optional options parameter is an array. The following keys will be handled:
	 * - bbcode: if true, besides the a-zA-Z0-9_- also @[]=/:+.? will be allowed
	 * - space: if true, also allow a whitespace character
	 * - url: if true, also allow :/+.?
	 * - email: if true, also allow @.
	 * if no options array is given, only a-zA-Z0-9_- will be allowed (that is without whitespace chars)
	 *
	 * @param mixed $data    The data to sanitize
	 * @param array $options Options, see description for possible items
	 *
	 * @return mixed See description of the function for array structure
	 */
	public function sanitize($data, $options=array()) {
		$allowed  = "a-zA-Z0-9_\-";
		$allowed .= preg_quote("(){}");
		/* create preg_replace string of characters we allow */
		if (array_key_exists("bbcode", $options)) {
			$allowed .= preg_quote("![]&@=+:/.?;,'\\\"", "/");
		}
		if (array_key_exists("space", $options)) {
			$allowed .= "\s";
		}
		if (array_key_exists("url", $options)) {
			$allowed .= preg_quote("+:/.?", "/");
		}
		if (array_key_exists("email", $options)) {
			$allowed .= preg_quote("@.");
		}

		$allowed = "/[^$allowed]/s";
		if (is_array($data)) {
			/* lets handle the array */
			foreach ($data as $k=>$v) {
				/* if the data is an array too, recurse */
				if (is_array($v)) {
					$v = $this->sanitize($v, $options);
				}

				if (!preg_match("/[^a-zA-Z0-9_-]/s", $k)) {
					/* do the actual sanitizing */
					$_data[$k] = preg_replace($allowed, "", $v);
				}
			}
		} else {
			/* it's a string, lets process it */
			$_data = preg_replace($allowed, "", $data);
		}
		return $_data;
	}
	/* }}} */
	/* db_quote {{{ */
	/**
	 * Quote a fieldname with the database specific quote style
	 *
	 * @param string $field fieldname to quote
	 *
	 * @return string the quoted version
	 */
	public function db_quote($field) {
		/* get the db type */
		$dbtype = $this->db->dbsyntax;
		switch ($dbtype) {
		case "mysql" :
			$return = "`".$field."`";
			break;
		case "pgsql" :
			$return = "\"".$field."\"";
			break;
		default :
			$return = $field;
			break;
		}
		return $return;
	}
	/* }}} */
	/* db_escape {{{ */
	/**
	 * Quote a string with the database specific escape function
	 *
	 * @param string $data The data to escape
	 *
	 * @return string Escaped string to be used in sql queries
	 */
	public function db_escape($data) {
		$dbtype = $this->db->dbsyntax;
		switch ($dbtype) {
		case "mysql" :
			$return = mysql_real_escape_string($data);
			break;
		case "pgsql" :
			$return = pg_escape_string($data);
			break;
		case "sqlite" :
			$return = sqlite_escape_string($data);
			break;
		default :
			$return = preg_quote($data, "'");
			break;
		}
		return $return;
	}
	/* }}} */
	/* strip_invalid_xml() {{{ */
	/**
	 * strip some stuff leftover from old editor
	 *
	 * @param string $data the text to process
	 *
	 * @return string the text with invalid xml stripped
	 */
	public function strip_invalid_xml($data) {
		$data = preg_replace("/ ref=\"[^\"].*\"/si", "", $data);
		return $data;
	}
	/* }}} */
	/* format parses */
	/* parse_body {{{ */
	/**
	 * Parse the body based on postformat.
	 *
	 * Supported formats with their processor:
	 * - BBC: bbcode - uses internal bbcode parser
	 * - HTML: html sourcecode - no processing is done
	 * - MW: MediaWiki sourcecode - uses PEAR::Text_Wiki_Mediawiki
	 *
	 * @param string $body   The raw body data
	 * @param string $format The body format.
	 *
	 * @return string the formatted body.
	 */
	public function parse_body($body, $format) {
		switch ($format) {
		case "BBC":
			$body = $this->parse_bbcode($body);
			break;
		case "MW":
			$body = $this->parse_mediawiki($body);
			break;
		}
		//fix leftovers from old editor
		$body = $this->strip_invalid_xml(stripslashes($body));
		return $body;
	}
	/* }}} */
	/* parse_bbcode($data) {{{ */
	/**
	 * Parse a string and intepret bbcode
	 *
	 * @param string $data The raw string to do bbcode substitution on
	 *
	 * @license LGPL
	 * @author Justin Palmer
	 * @url http://www.isolated-disigns.net/core
	 * @author Ferry Boender
	 *
	 * @return string the string with bbcode tags substituded
	 */
	public function parse_bbcode($data) {
		//     AUTHOR:  JUSTIN PALMER
		//     WEBSITE:  HTTP://WWW.ISOLATED-DESIGNS.NET/CORE
		//     LICENSE: GNU LESSER GENERAL PUBLIC LICENSE http://www.gnu.org/copyleft/lesser.html
		//     MODIFIED: Ferry Boender: optimized and function to strip bbcode
		/* Strip useless newlines so [code] will appear correct */
		$string = str_replace("\r", "", $data);
		$string = str_replace("\n", "<br />", $string);

		$patterns = array(
			'`\[ul\](.+?)\[/ul\]`is',
			'`\[li\](.+?)\[/li\]`is',
			'`\[quote\](.+?)\[/quote\]`is',
			'`\[indent](.+?)\[/indent\]`is',
			'`\[code](.+?)\[/code\]`is',
			'`\[b\](.+?)\[/b\]`is',
			'`\[i\](.+?)\[/i\]`is',
			'`\[u\](.+?)\[/u\]`is',
			'`\[strike\](.+?)\[/strike\]`is',
			'`\[color=#([0-9]{6})\](.+?)\[/color\]`is',
			'`\[email\](.+?)\[/email\]`is',
			'`\[img\](.+?)\[/img\]`is',
			'`\[url=([a-z0-9]+://)([\w\-]+\.([\w\-]+\.)*[\w]+(:[0-9]+)?(/[^ \"\n\r\t<]*?)?)\](.*?)\[/url\]`si',
			'`\[url\]([a-z0-9]+?://){1}([\w\-]+\.([\w\-]+\.)*[\w]+(:[0-9]+)?(/[^ \"\n\r\t<]*)?)\[/url\]`si',
			'`\[url\]((www|ftp)\.([\w\-]+\.)*[\w]+(:[0-9]+)?(/[^ \"\n\r\t<]*?)?)\[/url\]`si',
			'`\[flash=([0-9]+),([0-9]+)\](.+?)\[/flash\]`is',
			'`\[size=([1-6]+)\](.+?)\[/size\]`is'
		);

		$replaces =  array(
			'<ul>\1</ul>',
			'<li class="noblock">\1</li>',
			'<span class="quote">\1</span>',
			'<pre>\\1</pre>',
			'<pre class="code">\\1</pre>',
			'<strong>\\1</strong>',
			'<em>\\1</em>',
			'<span style="border-bottom: 1px dotted">\\1</span>',
			'<strike>\\1</strike>',
			'<span style="color:#\1;">\2</span>',
			'<a href="mailto:\1">\1</a>',
			'<img src="\1" alt="" style="border:0px;" />',
			'<a href="\1\2">\6</a>',
			'<a href="\1\2">\1\2</a>',
			'<a href="http://\1">\1</a>',
			'<object width="\1" height="\2"><param name="movie" value="\3"/><embed src="\3" width="\1" height="\2"></embed></object>',
			'<h\1>\2</h\1>'
		);

		$prev_string = "";
		while ($prev_string != $string) {
			$prev_string = $string;
			$string = preg_replace($patterns, $replaces, $string);
		}
		return(stripslashes($string));
	}
	/* }}} */
	/* parse_mediawiki {{{ */
	/**
	 * Parse Mediawiki formatted string into XHTML.
	 * This function uses PEAR::Text_Wiki and PEAR::Text_Wiki_Mediawiki.
	 * Because all plugins for Text_Wiki are alpha we include these 2 packages
	 * in the MvBlog sources.
	 *
	 * @param string $data The mediawiki source
	 *
	 * @return string XHTML representation of the wiki source
	 */
	public function parse_mediawiki($data) {
		//add our PEAR directory to the include path
		set_include_path(get_include_path().PATH_SEPARATOR.dirname(__FILE__)."/../lib/");
		//include the PEAR files
		include_once "Text/Wiki.php";
		//wiki object
		$wiki =& Text_Wiki::singleton("Mediawiki");
		$xhtml = $wiki->transform($data, "Xhtml");
		unset($wiki);
		return $xhtml;
	}
	/* }}} */
	/* data getters */
	/* get_settings {{{ */
	/**
	 * Get all the settings into an array
	 *
	 * @return void
	 */
	protected function get_settings() {
		$res =& $this->db->query("SELECT settingname, settingvalue FROM settings");
		if (PEAR::isError($res)) {
			die($res->getMessage());
		}

		// Add default settings as a fallback for when nothing's been set yet.
		$settings = array();
		$settings["blogtitle"]         = "";
		$settings["blogdescription"]   = "";
		$settings["blogkeywords"]      = "";
		$settings["postsperpage"]      = 20;
		$settings["cleanurl"]          = "";
		$settings["allowanoncomments"] = 0;
		$settings["dbversion"]         = 0;
		$settings["show_cat_icons"]    = 0;
		$settings["defaultpostformat"] = "HTML";
		$settings["wysiwyg"]           = 0;

		while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
			$settings[$row["settingname"]] = $row["settingvalue"];
		}
		if (array_key_exists("language", $settings)) {
			$this->lang     = $settings["language"];
		}
		$this->settings = $settings;
	}
	/* }}} */
	/* get_webroot {{{ */
	/**
	 * Get the webroot for the mvblog install
	 * Sets the value in class var webroot.
	 * This value will always end with a /
	 *
	 * @return void
	 */
	protected function get_webroot() {
		if (array_key_exists("bloglocation", $this->settings) && !empty($this->settings["bloglocation"])) {
			$webroot = $this->settings["bloglocation"];
		} else {
			if (array_key_exists("HTTPS", $_SERVER) && strtolower($_SERVER["HTTPS"]) == "on") {
				$webroot = "https://".strtolower($_SERVER["SERVER_NAME"]);
				$httpsmode = 1;
			} else {
				$webroot = "http://".strtolower($_SERVER["SERVER_NAME"]);
				$httpsmode = 0;
			}
			$webroot .= substr($_SERVER["SCRIPT_NAME"], 0, strpos($_SERVER["SCRIPT_NAME"], "index.php"));
		}
		/* if webroot does not end with a / append it */
		if (substr($webroot, -1, 1) != "/") {
			$webroot .= "/";
		}
		$this->webroot = $webroot;
	}
	/* }}} */
	/* get_authors() {{{ */
	/**
	 * Get all the authors into an array.
	 *
	 * @return void
	 */
	public function get_authors() {
		/* author with id 0 is for a new author. */
		$authors = array(
			0 => array(
				"login"    => "login",
				"password" => "",
				"email"    => "",
				"fullname" => "",
				"active"   => 1,
				"website"  => ""
			)
		);

		$res =& $this->db->query("SELECT id,login,password,email,fullname,active,website FROM authors");
		if (PEAR::isError($res)) {
			die($res->getMessage());
		}
		while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
			$authors[$row["id"]] = array(
				"login"    => $row["login"],
				"password" => $row["password"],
				"email"    => $row["email"],
				"fullname" => $row["fullname"],
				"active"   => $row["active"],
				"website"  => $row["website"]
			);
		}
		$this->authors = $authors;
	}
	/* }}} */
	/* get_users() {{{ */
	/**
	 * Get all the users into an array.
	 *
	 * @return void
	 */
	public function get_users() {
		$users = array();
		/* user with id 0 is for a new user. */
		$authors = array(
			0 => array(
				"login"    => "login",
				"password" => "",
				"email"    => "",
				"fullname" => "",
				"active"   => 1,
				"website"  => ""
			)
		);

		$res =& $this->db->query("SELECT * FROM blog_users");
		if (PEAR::isError($res)) {
			die($res->getMessage());
		}
		while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
			$users[$row["id"]] = array(
				"username" => $row["username"],
				"password" => $row["password"],
				"email"    => $row["email"],
				"realname" => $row["realname"],
				"active"   => $row["active"],
				"website"  => $row["website"]
			);
		}
		$this->users = $users;
	}
	/* }}} */
	/* get_active_plugins {{{ */
	/**
	 * Read active plugins from settings and unserialize it so plugmrg can handle it
	 *
	 * @return void
	 */
	protected function get_active_plugins() {
		if (array_key_exists("active_plugins", $this->settings)) {
			$this->active_plugins = unserialize(stripslashes($this->settings["active_plugins"]));
		} else {
			$this->active_plugins = array();
		}
	}
	/* }}} */
	/* get_categories {{{ */
	/**
	 * Get all the categories into an array
	 *
	 * @return void
	 */
	protected function get_categories() {
		/* empty one for create new */
		$categories = array(
			0 => array(
				"name"   => "Category Name",
				"desc"   => "Category Description",
				"public" => 1,
				"active" => 1,
				"icon"   => 0
			)
		);
		$res =& $this->db->query("SELECT * FROM categories");
		if (PEAR::isError($res)) {
			die($res->getMessage());
		}
		while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
			$categories[$row["id"]] = array(
				"name"   => $row["name"],
				"desc"   => $row["desc"],
				"public" => $row["public"],
				"active" => $row["active"],
				"icon"   => $row["icon"],
			);
		}
		$this->categories = $categories;
	}
	/* }}} */
	/* get_dossiers {{{ */
	/**
	 * Get all the dossiers into an array
	 *
	 * @return void
	 */
	protected function get_dossiers() {
		/* empty one for create new */
		$dossiers = array(
			0 => array(
				"id"     => 0,
				"name"   => "Dossier Name",
				"desc"   => "Dossier Description",
				"public" => 1,
				"active" => 1
			)
		);
		$res =& $this->db->query("SELECT * FROM dossiers");
		if (PEAR::isError($res)) {
			die($res->getMessage());
		}
		while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
			$dossiers[$row["id"]] = array(
				"id"     => $row["id"],
				"name"   => $row["name"],
				"desc"   => $row["desc"],
				"public" => $row["public"],
				"active" => $row["active"]
			);
		}
		$this->dossiers = $dossiers;
	}
	/* }}} */
	/* get_menuitems {{{ */
	/**
	 * Get all the user configured menuitems from db
	 *
	 * @return void
	 */
	protected function get_menuitems() {
		$sql = "SELECT * FROM menulinks ORDER BY sortorder,linktitle";
		$res =& $this->db->query($sql);

		$menuitems = array();
		while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
			$menuitems[$row["id"]] = array(
				"url"       => $row["url"],
				"linktitle" => $row["linktitle"],
				"image"     => $row["image"],
				"sortorder" => $row["sortorder"]
			);
		}
		$this->menuitems = $menuitems;
	}
	/* }}} */
	/* get_posts {{{ */
	/**
	 * Get posts based on optional options
	 *
	 * @param array $options Optional options posts should match
	 *
	 * @return array The posts data
	 */
	protected function get_posts($options = array()) {
		$desc = false;

		if (array_key_exists("archive", $options)) {
			$archive = $options["archive"];
		} else {
			$archive = 0;
		}

		if (array_key_exists("dossier", $options) && $options["dossier"]) {
			$dossier = $options["dossier"];
		} else {
			$dossier = 0;
		}

		if (array_key_exists("active", $options)) {
			$active = $options["active"];
		} else {
			$active = 1;
		}

		if (array_key_exists("public", $options)) {
			$public = $options["public"];
		} else {
			$public = 1;
		}

		if (array_key_exists("adminmode", $options) && $options["adminmode"]) {
			$adminmode = 1;
		} else {
			$adminmode = 0;
		}

		switch ($archive) {
		case 1 :
			if (!array_key_exists("timestamp_start", $options) && !array_key_exists("timestamp_end", $options)) {
				//backwards compat with frontend
				if (strlen($options["urlparams"]["m"]) == 6) {
					$time_month = substr($options["urlparams"]["m"], 0, 2);
					$time_year  = substr($options["urlparams"]["m"], 2, 4);
					$time_start = mktime(0, 0, 0, $time_month, 1, $time_year);
					$time_end   = mktime(0, 0, 0, $time_month+1, 1, $time_year);
					$archtitle  = date("F Y", $time_start);
				} elseif (strlen($options["urlparams"]["m"]) == 8) {
					$time_day   = substr($options["urlparams"]["m"], 0, 2);
					$time_month = substr($options["urlparams"]["m"], 2, 2);
					$time_year  = substr($options["urlparams"]["m"], 4, 4);
					$time_start = mktime(0, 0, 0, $time_month, $time_day, $time_year);
					$time_end   = mktime(0, 0, 0, $time_month, $time_day+1, $time_year);
					$archtitle  = date("d F Y", $time_start);
				} else {
					die("Invalid input");
				}
				$options["timestamp_start"] = $time_start;
				$options["timestamp_end"]   = $time_end;
			} else {
				$archtitle = date("F Y", $options["timestamp_start"]);
			}
			if ($adminmode) {
				$q = sprintf("AND date BETWEEN %d AND %d", $options["timestamp_start"], $options["timestamp_end"]);
			} else {
				$q = sprintf(
					"AND public=%d AND aside = 0 AND date BETWEEN %d AND %d",
					$public, $options["timestamp_start"], $options["timestamp_end"]
				);
			}
			$title = "<p><h1 class=\"log_archive_h1\">".gettext("Archive of")." ".$archtitle."</h1></p>";
			$url_prev = sprintf(
				"?action=archive&amp;m=%s&amp;top=%d",
				date("mY", $options["timestamp_start"]), $options["top"]-$options["limit"]
			);
			$url_next = sprintf(
				"?action=archive&amp;m=%s&amp;top=%d",
				date("mY", $options["timestamp_start"]), $options["top"]+$options["limit"]
			);
			break;
		case 2:
			if (!array_key_exists("category_id", $options)) {
				$options["category_id"] = (int)$_REQUEST["c"];
			}
			if ($adminmode) {
				$q = sprintf(
					"AND aside = 0
					AND date <= %d
					AND (
					categories_ids like '%%,%2\$d'
					OR categories_ids like '%2\$d,%%'
					OR categories_ids like '%%,%2\$d,%%' 
					OR categories_ids = '%2\$d'
					)",
					$options["max_time"], $options["category_id"]
				);
			} else {
				$q = sprintf(
					"AND public=%d 
					AND aside = 0 
					AND date <= %d 
					AND (
					categories_ids like '%%,%3\$d' 
					OR categories_ids like '%3\$d,%%' 
					OR categories_ids like '%%,%3\$d,%%'
					OR categories_ids = '%3\$d'
					)",
					$public, $options["max_time"], $options["category_id"]
				);
			}
			$title = sprintf(
				"<p><h1 class=\"log_archive_h1\">%s <i>%s</i></h1></p>",
				gettext("Archive of category"), $this->categories[$options["category_id"]]["name"]
			);
			if ($this->categories[$options["category_id"]]["icon"]) {
				$icon = sprintf(
					"<img src=\"images/categories/%s\" alt=\"%s\" /><br />",
					$this->categories[$options["category_id"]]["icon"], 
					substr(
						$this->categories[$options["category_id"]]["icon"], 
						0, 
						strlen($this->categories[$options["category_id"]]["icon"])-4
					)
				);
			} else {
				$icon = "";
			}
			$desc  = sprintf(
				"<br /><br /><div class=\"log_dossier_description\">%s</div><br />",
				$icon.$this->categories[$options["category_id"]]["desc"]
			);
			$url_prev = "?action=archive_cat&amp;c=".$options["category_id"]."&amp;top=".($options["top"]-$options["limit"]);
			$url_next = "?action=archive_cat&amp;c=".$options["category_id"]."&amp;top=".($options["top"]+$options["limit"]);
			break;
		case 3 :
			if ($adminmode) {
				$q = sprintf("AND date <= %d AND aside=1", $options["max_time"]);
			} else {
				$q = sprintf("AND public=%d AND date <= %d AND aside=1", $public, $options["max_time"]);
			}
			$title = "<p><h1 class=\"log_archive_h1\">".gettext("Archive of")." <i>\"asides\"</i></h1></p>";
			$url_prev = "?action=archive_cat&amp;c=aside&amp;top=".($options["top"]-$options["limit"]);
			$url_next = "?action=archive_cat&amp;c=aside&amp;top=".($options["top"]+$options["limit"]);
			break;
		case 4 :
			if (!array_key_exists("timestamp_start", $options)) {
				if (array_key_exists("fromts", $_REQUEST)) {
					$time_start = $_REQUEST["fromts"];
				} else {
					$time_start = mktime();
				}
				$options["timestamp_start"] = $time_start;
			}
			if ($adminmode) {
				$q = sprintf("AND date <= %d", $options["timestamp_start"]);
			} else {
				$q = sprintf("AND public = %d AND date <= %d", $public, $options["timestamp_start"]);
			}
			$title = sprintf(
				"<p><h1 class=\"log_archive_h1\">%s&nbsp;%s</h1></p>",
				gettext("Archive of articles posted before"), date("F Y", $options["timestamp_start"])
			);
			$url_prev = "?action=archive_old&amp;fromts=".$options["timestamp_start"]."&amp;top=".($options["top"]-$options["limit"]);
			$url_next = "?action=archive_old&amp;fromts=".$options["timestamp_start"]."&amp;top=".($options["top"]+$options["limit"]);
			break;
		default :
			if ($dossier) {
				//get dossierinfo
				$dq = sprintf("SELECT * FROM dossiers WHERE id = %d", $dossier);
				$dr =& $this->db->query($dq);
				$dossierinfo = $dr->fetchRow(MDB2_FETCHMODE_ASSOC);
				$title = $dossierinfo["name"];
				$desc  = "<br /><br /><div class=\"log_dossier_description\">".$dossierinfo["desc"]."</div><br />";
				$url_prev = "index.php?action=viewdossier&id=$dossier&top=".($options["top"]-$options["limit"]);
				$url_next = "index.php?action=viewdossier&id=$dossier&top=".($options["top"]+$options["limit"]);
				if ($adminmode) {
					$q = sprintf("AND date <= %d AND dossier_id = %d", $options["max_time"], $dossier);
				} else {
					$q = sprintf("AND date <= %d AND public=%d AND dossier_id = %d", $options["max_time"], $public, $dossier);
				}
			} else {
				if ($adminmode) {
					$q = sprintf("AND date <= %d", $options["max_time"]);
				} else {
					$q = sprintf("AND date <= %d AND public=%d", $options["max_time"], $public);
				}
				$title = "";
				$url_prev = "index.php?top=".($options["top"]-$options["limit"]);
				$url_next = "index.php?top=".($options["top"]+$options["limit"]);
			}
			break;
		}

		$posts = array(
			"title"    => $title,
			"url_prev" => $url_prev,
			"url_next" => $url_next
		);

		if ($desc) {
			$posts["desc"] = $desc;
		}

		if ($dossier) {
			$posts["is_dossier"] = 1;
		} else {
			$posts["is_dossier"] = 0;
		}

		if ($options["limit"]) {
			$q_count = sprintf("SELECT COUNT(*) FROM articles WHERE active = 1 %s", $q);
			$res_count =& $this->db->query($q_count);
			if (PEAR::isError($res_count)) {
				die($res_count->getUserInfo());
			}

			$row = $res_count->fetchRow();
			$posts["total_count"] = $row[0];
			$this->db->setLimit($options["limit"], $options["start"]);
			$sql = sprintf("SELECT * FROM articles WHERE active = 1 %s ORDER BY date DESC", $q);
			$res =& $this->db->query($sql);
		} else {
			$sql = sprintf("SELECT * FROM articles WHERE active = 1 %s", $q);
			$res =& $this->db->query($sql);
		}

		if (PEAR::isError($res)) {
			die($res->getMessage());
		}
		if (!array_key_exists("total_count", $posts)) {
			$posts["total_count"] = $res->numRows;
		}

		if ($res->numRows()) {
			$i = 0;
			while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
				if (array_key_exists("replace_references", $options) && $options["replace_references"] == 1) {
					$row["body_formatted"] = $this->replace_num_ref($row["body"]);
					$row["body_formatted"] = $this->replace_dossier_ref($row["body_formatted"]);
				} else {
					$row["body_formatted"] = $row["body"];
				}
				// parse body according to postformat
				$row["body_formatted"] = $this->parse_body($row["body_formatted"], $row["postformat"]);
				$posts["posts"][$i] = $row;
				$i++;
			}
		}
		return $posts;
	}
	/* }}} */
	/* replace_num_ref {{{ */
	/**
	 * Replace numeric references to other posts in a piece of data.
	 *
	 * You can reference to other posts in postdata by including
	 * a string like [#<postnumber>].
	 * The output will be: <a href="index.php?action=view&id=<postnumber>">posttitle</a>
	 *
	 * @param string $data The code to do the replacement in
	 *
	 * @return string input with the postreference code replaced
	 */
	public function replace_num_ref($data) {
		//replace [#number] with a link to this post and replace the number with the post title
		preg_match_all("/\[#(\d{1,})\]/s", $data, $matches);
		if (count($matches[0])) {
			// we have something to replace
			foreach ($matches[0] as $k => $v) {
				$q = sprintf("SELECT title FROM articles WHERE id = %d", $matches[1][$k]);
				$r = $this->db->query($q);
				$ref = $r->fetchRow(MDB2_FETCHMODE_ASSOC);
				$data = str_replace(
					$matches[0][$k], 
					sprintf(
						"<a href=\"%sindex.php?action=view&amp;id=%s\">%s</a>", 
						$this->webroot, $matches[1][$k], $ref["title"]
					),
					$data
				);
			}
		}
		return $data;
	}
	/* }}} */
	/* replace_dossier_ref {{{ */
	/**
	 * Replace references to dossiers in a piece of data.
	 *
	 * You can reference to dossiers in postdata by including
	 * a string like [#d<dossiernumber>].
	 * The output will be: <a href="index.php?action=viewdossier&id=<dossiernumber>">dossiertitle</a>
	 *
	 * @param string $data The code to do the replacement in
	 *
	 * @return string input with the postreference code replaced
	 */
	public function replace_dossier_ref($data) {
		//replace [#dnumber] with a link to this dossier and replace the number with the dossier title
		preg_match_all("/\[#d(\d{1,})\]/s", $data, $matches);
		if (count($matches[0])) {
			// we have something to replace
			foreach ($matches[0] as $k => $v) {
				$q = sprintf("SELECT name FROM dossiers WHERE id = %d", $matches[1][$k]);
				$r = $this->db->query($q);
				$ref = $r->fetchRow(MDB2_FETCHMODE_ASSOC);
				$data = str_replace(
					$matches[0][$k], 
					"<a href=\"".$this->webroot."index.php?action=viewdossier&amp;id=".$matches[1][$k]."\">".$ref["name"]."</a>",
					$data
				);
			}
		}
		return $data;
	}
	/* }}} */
	/* filesystem methods */
	/* copy_files {{{ */
	/**
	 * Copies files and directories recursive to dirs under site_images
	 *
	 * @param string $srcbase        Base directory
	 * @param string $src            The sourcefile or dir
	 * @param string $dest           The dir inside site_images where the files/folders should be copied to
	 * @param int    $overwrite_dest if set will overwrite the destination if it already exists
	 *
	 * @return void
	 */
	public function copy_files($srcbase, $src, $dest, $overwrite_dest = 0) {
		// calculate where the site_images dir is
		$basepath = realpath(dirname(__FILE__)."/../site_images/");
		$dst = sprintf("%s/%s", $basepath, $dest);
		//check if the dst dir is there
		if (!is_dir($dst)) {
			if (!mkdir($dst)) {
				die("could not create directory $dst");
			}
		}
		if ($handle = opendir(sprintf("%s/%s", $srcbase, $src))) { //open source directory
			while (false !== ($file = readdir($handle))) { //loop through all items in the opened source dir
				if ($file != "." && $file != "..") { //we wont copy the special 'current dir' and 'one dir up' items
					if ($src) {
						$path = sprintf("%s/%s", $src, $file);
					} else {
						$path = $file;
					}
					$s = sprintf("%s/%s", $srcbase, $path);
					$d = sprintf("%s/%s", $dst, $path);
					if (is_file($s)) {
						if (is_file($d) || $overwrite_dest) {
							if (!copy($s, $d)) {
								echo "Something went wrong while trying to copy $s to $d<br>";
							}
						}
					} elseif (is_dir($s)) {
						if (!is_dir($d)) {
							if (!mkdir($d)) {
								echo "Something went wrong while trying to create dir $d<br>\n";
							}
						}
						$this->copy_files($srcbase, $path, $dest, 1);
					}
				}
			}
			closedir($handle);
		}
	}
	/* }}} */
	/* output methods */
	/* html_header: {{{ */
	/**
	 * Send start of html document
	 *
	 * @param string $title The title to use in html title tag
	 * @param string $style The style to use
	 *
	 * @return nothing It uses echo to send the data to the client
	 */
	public function html_header($title="Home", $style="MvBlog") {
		/* Find the default style from either the GET/POST or the session. */
		if (array_key_exists("style", $_REQUEST)) {
			/* Overwrite the style set in the session from the request */
			$_SESSION["style"] = $this->sanitize($_REQUEST["style"]);
		}
		if (!array_key_exists("style", $_SESSION)) {
			/* If no style is set, set the default or what was passed to this function. */
			$_SESSION["style"] = $this->sanitize($style);
		}

		/* sanitize style var */
		$_SESSION["style"] = str_replace("../", "", $_SESSION["style"]);
		/* if title is given as argument for this function, overwrite setting */
		if ($title == "Home" || $title == "") {
			$title = $this->settings["blogtitle"];
		}

		header('Content-Type: text/html; charset=iso-8859-1');
		echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" ";
		echo "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n";
		echo "<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\" xml:lang=\"en\">\n";
		echo "<head>\n";
		if (substr($this->webroot, -6) != "admin/") {
			echo "\t<base href=\"".$this->webroot."\" />\n";
		}
		echo "\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n";
		echo "\t<meta name=\"robots\" content=\"ALL\" />\n";
		echo "\t<meta name=\"generator\" content=\"MvBlog ".$this->version."\" />\n";
		echo "\t<meta name=\"resource-type\" content=\"document\" />\n";
		echo "\t<meta name=\"audience\" content=\"general\" />\n";
		echo "\t<meta name=\"web-rev\" content=\"4.0\" />\n";
		echo "\t<meta name=\"last-modified\" content=\"".date("r")."\" />\n";
		echo "\t<meta name=\"x-language\" content=\"".$this->lang."\" />\n";
		/* if we have a description and/or keywords, put the meta tags in place */
		if ($this->settings["blogdescription"]) {
			echo sprintf(
				"\t<meta name=\"Description\" content=\"%s\" />\n",
				strip_tags(preg_replace("/\r|\n/si", "", str_replace("\"", "", $this->settings["blogdescription"])))
			);
		}
		if ($this->settings["blogkeywords"]) {
			echo sprintf(
				"\t<meta name=\"Keywords\" content=\"%s\" />\n",
				strip_tags(preg_replace("/\r|\n/si", "", $this->settings["blogkeywords"]))
			);
		}
		/* admin always has MvBlog style */
		if (substr($this->webroot, -6) == "admin/") {
			$css = "\t<link rel=\"stylesheet\" href=\"style/MvBlog/index.css\" type=\"text/css\" />\n";
		} else {
			$css = "\t<link rel=\"stylesheet\" href=\"style/".$_SESSION["style"]."/index.css\" type=\"text/css\" />\n";
		}
		$plugincss = $this->plugman->run_hooks("css_output", $css);
		echo $plugincss;
		echo "\t<link rel=\"stylesheet\" type=\"text/css\" media=\"print\" href=\"print.css\" />\n";
		echo "\t<link rel=\"alternate\" type=\"application/xml\" title=\"RSS\" href=\"index.php?action=rss\" />\n";
		echo "\t<link rel=\"icon\" href=\"favicon.ico\" type=\"image/x-icon\" />\n";
		echo "\t<link rel=\"shortcut icon\" href=\"favicon.ico\" type=\"image/x-icon\" />\n";
		echo "\t<title>".$this->sanitize($title, array("bbcode" => 1, "space" => 1))." - MvBlog powered</title>\n";
		echo "</head>\n";
		echo "<body>\n";
	}
	/* }}} */
	/* html_footer() {{{ */
	/**
	 * send the end of a html document
	 *
	 * @return void
	 */
	public function html_footer() {
		echo "\t\t\t<div id=\"if_page_footer\">\n";
		echo "\t\t\t\tWebsite engine is running <a href=\"http://www.mvblog.org\">MvBlog</a>";
		echo "-".$this->version." &copy;2005-2008 copyright by Michiel van Baak.<br />\n";
		echo "\t\t\t\t<a href=\"http://www.mvblog.org\">MvBlog</a> is a free software released under the GNU/GPL.\n";
		echo "\t\t\t</div>\n";
		echo "\t\t</div>\n";
		echo "\t</div>\n";
	}
	/* }}} */
}
