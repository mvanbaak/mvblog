<?php
/**
 * MvBlog -- An open source no-nosense blogtool
 *
 * Copyright (C) 2005-2008, Michiel van Baak
 * Michiel van Baak <mvanbaak@users.sourceforge.net>
 *
 * See http://dev.mvblog.org for more information on MvBlog.
 * That page also provides Bugtrackers, Filereleases etc.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 * PHP version 5
 *
 * @category  PHP
 * @package   MvBlog
 * @author    Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright 2005-2008 Michiel van Baak
 * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @version   SVN: $Revision: 809 $
 * @link      http://www.mvblog.org
 */
require "mvblog.php";
$mvblog = new MvBlog();

//generate url to the root of our website
if (array_key_exists("HTTPS", $_SERVER) && $_SERVER["HTTPS"] == "on") {
	$proto = "https";
} else {
	$proto = "http";
}
$url = $proto."://".$_SERVER["SERVER_NAME"].(substr($_SERVER["REQUEST_URI"], 0, strpos($_SERVER["REQUEST_URI"], "common/")));

$max_time = mktime(0, 0, 0, date("m"), date("d")+1, date("Y"));

$sql = "SELECT * FROM articles WHERE date <= $max_time AND active=1 AND public=1 ORDER BY date DESC";
$comments = false;
$mvblog->db->setLimit(20);
$res =& $mvblog->db->query($sql);

if (PEAR::isError($res)) {
	die($res->getMessage());
}

/* fetch all items */
$i = 0;
$items = array();
while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
	$items[$i]["title"] = htmlspecialchars($row["title"]);
	if (array_key_exists("aside", $row) && $row["aside"] == 1) {
		$items[$i]["link"] = $url."index.php?aside=".$row["id"];
		$items[$i]["category"] = "asides";
		$items[$i]["comments"] = "0";
	} else {
		$items[$i]["link"] = $url."index.php?action=view&amp;id=".$row["id"];
		$categories = explode(",", $row["categories_ids"]);
		$category_names = array();
		foreach ($categories as $v) {
			if (array_key_exists($v, $mvblog->categories)) {
				$category_names[] = $mvblog->categories[$v]["name"];
			}
		}
		$items[$i]["category"] = implode(", ", $category_names);
	}
	$items[$i]["creator"] = htmlspecialchars($mvblog->authors[$row["authors_id"]]["fullname"]);
	$items[$i]["description"] = $mvblog->strip_bbcode(
		strip_tags(str_replace("##BREAKPOINT##", "", stripslashes($row["body"])))
	);
	$items[$i]["date"] = date("r", $row["date"]);
	$i++;
}

/* start outputting the xml/rdf */
header("Content-Type: text/xml");
echo "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n";
echo "<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" ";
echo "xmlns=\"http://purl.org/rss/1.0/\" xmlns:slash=\"http://purl.org/rss/1.0/modules/slash/\" ";
echo "xmlns:taxo=\"http://purl.org/rss/1.0/modules/taxonomy/\" xmlns:dc=\"http://purl.org/dc/elements/1.1/\" ";
echo "xmlns:syn=\"http://purl.org/rss/1.0/modules/syndication/\" ";
echo "xmlns:admin=\"http://webns.net/mvcb/\" xmlns:feedburner=\"http://rssnamespace.org/feedburner/ext/1.0\">\n\n";
echo "\t<channel rdf:about=\"".$url."\">\n";
echo "\t\t<title>".$mvblog->settings["blogtitle"]."</title>\n";
echo "\t\t<description>".$mvblog->settings["blogdescription"]."</description>\n";
echo "\t\t<link>".$url."</link>\n";
echo "\t\t<dc:date>".date("c")."</dc:date>\n";
echo "\t\t<dc:subject>Blog</dc:subject>\n";
echo "\t\t<syn:updatePeriod>daily</syn:updatePeriod>\n";
echo "\t\t<syn:updateFrequency>1</syn:updateFrequency>\n";
echo "\t\t<syn:updateBase>1970-01-01T00:00+00:00</syn:updateBase>\n";
echo "\t\t<items>\n";
echo "\t\t\t<rdf:Seq>\n";
foreach ($items as $item) {
	echo "\t\t\t\t<rdf:li rdf:resource=\"".$item["link"]."\" />\n";
}
echo "\t\t\t</rdf:Seq>\n";
echo "\t\t</items>\n";
echo "\t</channel>\n";

foreach ($items as $item) {
	echo "\t<item rdf:about=\"".$item["link"]."\">\n";
	echo "\t\t<title>".$item["title"]."</title>\n";
	echo "\t\t<link>".$item["link"]."</link>\n";
	echo "\t\t<description>".htmlspecialchars($item["description"])."</description>\n";
	echo "\t\t<dc:creator>".$item["creator"]."</dc:creator>\n";
	echo "\t\t<dc:date>".$item["date"]."</dc:date>\n";
	echo "\t\t<dc:subject>".$item["category"]."</dc:subject>\n";
	echo "\t\t<feedburner:origLink>".$item["link"]."</feedburner:origLink>\n";
	echo "\t</item>\n";
}
echo "</rdf:RDF>\n";
?>
