<?php
/**
 * MvBlog -- An open source no-nosense blogtool
 *
 * Copyright (C) 2005-2008, Michiel van Baak
 * Michiel van Baak <mvanbaak@users.sourceforge.net>
 *
 * See http://dev.mvblog.org for more information on MvBlog.
 * That page also provides Bugtrackers, Filereleases etc.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 * PHP version 5
 *
 * @category  PHP
 * @package   MvBlog
 * @author    Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright 2005-2008 Michiel van Baak
 * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @version   SVN: $Revision$
 * @link      http://www.mvblog.org
 */

/**
 * Debugging class that takes care of error reporting, etc.
 *
 * @category  PHP
 * @package   MvBlog
 * @author    Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright 2005-2008 Michiel van Baak
 * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @version   Release: %%VERSION%%
 * @link      http://www.mvblog.org
 */
Class MvBlog_debug {
	/**
	 * @var array $development_hostnames Hostnames that will turn on strict errors.
	 */
	public static $development_hostnames = array(
		"dev",
		"localhost",
		"127.0.0.1"
	);

	/**
	 * @var array $error_names List of errors in int and string format.
	 */
	public static $error_names = array(
		1    => "E_ERROR",
		2    => "E_WARNING",
		4    => "E_PARSE",
		8    => "E_NOTICE",
		16   => "E_CORE_ERROR",
		32   => "E_CORE_WARNING",
		64   => "E_COMPILE_ERROR",
		128  => "E_COMPILE_WARNING",
		256  => "E_USER_ERROR",
		512  => "E_USER_WARNING",
		1024 => "E_USER_NOTICE",
		2048 => "E_STRICT",
		4096 => "E_RECOVERABLE_ERROR",
		8191 => "E_ALL",
	);

	/**
	 * Starts the development error reporting, which is much stricter than
	 * normal. This is to ensure that developers fix errors and notices that
	 * occur. This will only be set when the current hostname exists in the
	 * MvBlog_debug::$development_hostnames array.
	 *
	 * @param bool $warn_strict (optional) Warn/stop on strict errors?
	 *
	 * @return void
	 */
	public static function start_development($warn_strict = true) {
		/* Turn on heavy error reporting so that no errors go unnoticed, but 
		   only for development hostnames. This way end-users won't accidentally get
		   errors they shouldn't be seeying. */
		if (array_key_exists("SERVER_NAME", $_SERVER)
		    && in_array($_SERVER["SERVER_NAME"], MvBlog_debug::$development_hostnames)
		) {
			MvBlog_debug::start_pendantic_errors($warn_strict);
		}

	}
	/**
	 * Starts the pendantic error logging mode. Every error will stop execution
	 * of the script straight away. You can specify if you DON'T want Future
	 * errors to also stop the script.
	 *
	 * @param bool $warn_strict (optional) Warn/stop on strict errors?
	 *
	 * @return void
	 */
	public static function start_pendantic_errors($warn_strict = true) {
		// Asserts can be used to test preconditions in, e.g. functions. They
		// are off by default. Turn them on here.
		assert_options(ASSERT_ACTIVE, 1);
		assert_options(ASSERT_WARNING, 1);
		assert_options(ASSERT_BAIL, 1);

		// Turn on the highest level of error reporting.
		if ($warn_strict && defined("E_STRICT")) {
			error_reporting(E_ALL | E_STRICT);
		} else {
			error_reporting(E_ALL);
		}

		// Show errors in the source code (to annoy developers)
		ini_set("display_errors", "1");

		// Set a custom error handler that's stricter than the normal one.
		set_error_handler(array("MvBlog_debug", "user_error_handler"));
	}

	/**
	 * The MvBlog custom error handler. This error handler will does basically
	 * the same as the normal error, except that it also exists on when an
	 * error or notice is encountered.
	 *
	 * @param int    $errno   The php error number
	 * @param string $errstr  The php error message
	 * @param string $errfile The php file where the error is
	 * @param int    $errline The line where the error is
	 *
	 * @return void
	 */
	public static function user_error_handler($errno, $errstr, $errfile, $errline) {
		$error_reporting_level = error_reporting();

		// Only show errors for which error reporting level is turned on.
		if ($error_reporting_level & $errno) {
			$errtype = MvBlog_debug::$error_names[$errno];
			echo($errtype." error in <b>".$errfile."</b>:<b>".$errline."</b> - <i>".$errstr."</i>");
			exit();
		}
	}
}
