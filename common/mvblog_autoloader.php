<?php
/**
 * MvBlog -- An open source no-nosense blogtool
 *
 * Copyright (C) 2005-2008, Michiel van Baak
 * Michiel van Baak <mvanbaak@users.sourceforge.net>
 *
 * See http://dev.mvblog.org for more information on MvBlog.
 * That page also provides Bugtrackers, Filereleases etc.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 * PHP version 5
 *
 * @category  PHP
 * @package   MvBlog
 * @author    Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright 2005-2008 Michiel van Baak
 * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @version   SVN: $Revision: 809 $
 * @link      http://www.mvblog.org
 */

/**
 * AutoLoader class handles auto loading of classes/objects.
 *
 * Example:
 * <code>
 * require_once("MvBlog_AutoLoader.php");
 * 
 * $MvBlog_AutoLoader = new MvBlog_AutoLoader();
 *
 * // Add the directory in which the current file resides to the autoloader.
 * $pathInfo = pathinfo(__FILE__);
 * $MvBlog_AutoLoader->registerPath($pathInfo["dirname"]);
 *
 * // Register the AutoLoader object as the autoloader.
 * function __autoload($className) {
 *   global $MvBlog_AutoLoader; 
 *   $MvBlog_AutoLoader->autoload($className);
 * }
 * </code>
 *
 * @category  PHP
 * @package   MvBlog
 * @author    Ferry Boender <void@example.com>
 * @copyright 2005-2008 Ferry Boender
 * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @version   Release: %%VERSION%%
 * @link      http://www.mvblog.org
 */
class MvBlog_AutoLoader {
	const OPT_LOWERCASE = 1; 

	protected $paths = array();

	/**
	 * Create a new MvBlog_AutoLoader object. It will automatically parse
	 * your PHP ini include_path for paths in which is should look
	 * for objects. The default format it will look for is "CLASSNAME.php"
	 * (case-sensitive). The current directory is usually included in
	 * your include_path, so you won't have to add that. Add new
	 * paths using ->registerPath($path, $format).
	 *
	 * Each element in $paths is an associative array with "path", "format" and "options" keys. 
	 * I.e. 
	 * <code>
	 * new MvBlog_AutoLoader(array(
	 *	array(
	 *		"path"=>"/usr/share/lib/app", 
	 *		"format" => "class_%s.php", 
	 *		"options" => MvBlog_AutoLoader::OPT_LOWERCASE
	 *	)
	 *	));
	 * </code>
	 *
	 * @param array $paths (optional) An array containing paths and formats. 
	 */
	public function __construct($paths = null) {
		// Add paths in include_path ini setting of PHP.
		$phpIncludePath = ini_get("include_path");
		foreach (explode(':', $phpIncludePath) as $path) {
			$this->registerPath($path, "%s.php");
		}
		// Add user-defined paths
		if (isset($paths)) {
			foreach ($paths as $path) {
				$realpath = realpath($path["path"]);
				$this->registerPath($realpath, $path["format"]);
			}
		}
	}

	/**
	 * Add a new path to the list of paths where MvBlog_AutoLoader will look for classes.
	 *
	 * The paths variable will be automatically expanded to an absolute pathname.
	 * (i.e. ../../foo will become /var/www/foo/, depending on your current path).
	 *
	 * The format variable will have
	 * %s be expanded to the classname that needs to be loaded. Example: "class_%s.php".
	 *
	 * @param string $path    The path.
	 * @param string $format  (optional) The format of filenames to look for. 
	 * @param string $options (optional) Options to store in the pathInfo
	 *
	 * @return bool false if path is not a dir or not set.
	 */
	public function registerPath($path, $format = "%s.php", $options = 0) {
		$path = realpath($path);
		if ($path === false || !is_dir($path)) {
			return(false);
		}

		$pathInfo = array("path" => $path, "format" => $format, "options" => $options);
		$this->paths[] = $pathInfo;

		return(true);
	}

	/**
	 * The autoloader. It looks through all the paths and tries to find a file that contains the class to include.
	 * This method should be called from an __autoload() function in your initializing code.
	 * 
	 * @param string $className The name of the class to load.
	 *
	 * @return bool true if file is present and loaded
	 */
	public function autoload($className) {
		foreach ($this->paths as $path) {
			if ($path["options"] & MvBlog_AutoLoader::OPT_LOWERCASE) {
				$finalClassName = strtolower($className);
			} else {
				$finalClassName = $className;
			}

			$filename = $path["path"] . '/' . sprintf($path["format"], $finalClassName);
			if (file_exists($filename)) {
				include_once $filename;
				return(true);
			}
		}
	}
}
?>
