<?php
/**
 * MvBlog -- An open source no-nosense blogtool
 *
 * Copyright (C) 2005-2008, Michiel van Baak
 * Michiel van Baak <mvanbaak@users.sourceforge.net>
 *
 * See http://dev.mvblog.org for more information on MvBlog.
 * That page also provides Bugtrackers, Filereleases etc.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 * PHP version 5
 *
 * @category  PHP
 * @package   MvBlog
 * @author    Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright 2005-2008 Michiel van Baak
 * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @version   SVN: $Revision: 809 $
 * @link      http://www.mvblog.org
 */
require "mvblog.php";
$mvblog = new MvBlog();
$comment = "<b><i>Comment preview: </i></b>";
$commentdata["title"]   = htmlspecialchars(stripslashes($_REQUEST["title"]));
$commentdata["comment"] = nl2br($mvblog->parse_bbcode($mvblog->strip_invalid_xml(stripslashes($_REQUEST["comment"]))));
$commentdata["name"]    = htmlspecialchars(stripslashes($_REQUEST["name"]));
$commentdata["ip"]      = $mvblog->obfuscate_ip($_SERVER["REMOTE_ADDR"]);
$commentdata["email"]   = htmlspecialchars(stripslashes($_REQUEST["email"]));
$commentdata["website"] = htmlspecialchars(stripslashes($_REQUEST["website"]));
$comment .= $mvblog->show_comment($commentdata);
echo $comment;
?>
