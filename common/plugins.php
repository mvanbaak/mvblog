<?php
/**
 * MvBlog -- An open source no-nosense blogtool
 *
 * Copyright (C) 2005-2008, Michiel van Baak
 * Michiel van Baak <mvanbaak@users.sourceforge.net>
 *
 * See http://dev.mvblog.org for more information on MvBlog.
 * That page also provides Bugtrackers, Filereleases etc.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 * PHP version 5
 *
 * @category  PHP
 * @package   MvBlog
 * @author    Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright 2005-2008 Michiel van Baak
 * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @version   SVN: $Revision: 846 $
 * @link      http://www.mvblog.org
 */

/**
 * Class that holds methods to manage plugins etc.
 *
 * @category  PHP
 * @package   MvBlog
 * @author    Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright 2005-2008 Michiel van Baak
 * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @version   Release: %%VERSION%%
 * @link      http://www.mvblog.org
 */
Class MvBlog_pluginmgr {

	/* constants */

	/* variables */
	/**
	 * @var array $plugins Contains all loaded plugin objects
	 */
	public $plugins = array();
	/**
	 * @var string $plugindir The directory that holds all the plugins
	 */
	public $plugindir = "";

	/* methods */
	/* __construct {{{ */
	/**
	 * Load all plugins into plugins array
	 *
	 * @param string $plugindir The dir to load
	 * @param object &$mvblog   The main mvblog object
	 *
	 * @return void
	 */
	public function __construct($plugindir, &$mvblog) {
		/* make sure the dir is set and that it contains sane data */
		if (!strlen($plugindir)) {
			die("Plugindir not given");
		}
		$plugindir = str_replace("../../", "", $plugindir); // get rid of ../
		$plugindir = preg_replace("/^\//si", "", $plugindir);   // get rid of .
		/* check if it is there, if not look 1 level up, not more */
		if (!is_dir($plugindir)) {
			$plugindir = "../".$plugindir;
		}
		$this->plugindir = $plugindir;

		/* read all files and init the objects */
		$handle = opendir($this->plugindir);
		while (false !== ($file = readdir($handle))) {
			if (strpos($file, ".") === 0) {
				continue; // dont process . .. or hidden files/dirs
			}
			if (is_dir($this->plugindir.$file)) {
				continue; // dont process directories
			}
			/* get the filename */
			/* all files should be: pluginname.php */
			$plugin = explode(".", $file);
			$filename = $plugin[0];
			include_once $this->plugindir.$file;
			$this->plugins[$filename] = new $filename($mvblog);
			// FB: 20070304: Eval isn't needed here. Commented out and replaced
			// by the line above. This fixed 'undefined constant' errors.
			//eval("\$this->plugins[".$filename."] = new ".$filename."(\$mvblog);");
		}
		/* sort plugins */
		ksort($this->plugins);

	}
	/* }}} */
	/* activate_plugin {{{ */
	/**
	 * Activate givin plugin
	 * If the plugin is not loaded, do nothing
	 *
	 * @param string $plugin The plugin name to activate
	 *
	 * @return bool true on success, false on failure
	 */
	public function activate_plugin($plugin) {
		if (array_key_exists($plugin, $this->plugins)) {
			$this->plugins[$plugin]->active = true;
			$this->plugins[$plugin]->activate();
			return true;
		} else {
			return false;
		}
	}
	/* }}} */
	/* deactivate_plugin {{{ */
	/**
	 * deActivate givin plugin
	 * If the plugin is not loaded, do nothing
	 *
	 * @param string $plugin The plugin name to deactivate
	 *
	 * @return bool true on success, false on failure
	 */
	public function deactivate_plugin($plugin) {
		if (array_key_exists($plugin, $this->plugins)) {
			$this->plugins[$plugin]->active = false;
			$this->plugins[$plugin]->deactivate();
			return true;
		} else {
			return false;
		}
	}
	/* }}} */
	/* set_active_plugins {{{ */
	/**
	 * Activate plugins using an external array.
	 * This can be usefull if you store your activate plugins in a database
	 *
	 * @param array $active_plugins Array with plugin names to activate
	 *
	 * @return void
	 */
	public function set_active_plugins($active_plugins) {
		foreach ($active_plugins as $activate) {
			$this->activate_plugin($activate);
		}
	}
	/* }}} */
	/* get_active_plugins {{{ */
	/**
	 * Return array of active plugins
	 *
	 * @return array Active plugins
	 */
	public function get_active_plugins() {
		$return = array();
		foreach ($this->plugins as $name=>$plugin) {
			if ($plugin->active) {
				$return[] = $name;
			}
		}
		return $return;
	}
	/* }}} */
	/* run_hooks {{{ */
	/**
	 * Loop through all the plugins and run functions registered to given hooktype
	 *
	 * @param string $type The hooktype to run
	 * @param string $data The data to run the hook on
	 *
	 * @return string The processed data
	 */
	public function run_hooks($type, $data) {
		foreach ($this->plugins as $plugin) {
			/* only run when the plugin is active */
			if ($plugin->active) {
				foreach ($plugin->hooks as $hook=>$function) {
					if ($hook == $type) {
						$data = eval("return \$plugin->$function(\$data);");
					}
				}
			}
		}
		return $data;
	}
	/* }}} */
}

/**
 * Default Plugin class with predefined variables and addHook function.
 * All plugins should extend on this one
 *
 * @category  PHP
 * @package   MvBlog
 * @author    Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright 2005-2008 Michiel van Baak
 * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @version   Release: %%VERSION%%
 * @link      http://www.mvblog.org
 */
Class MvBlog_plugin {
	/**
	 * @var array All hooks with their functions
	 */
	public $hooks  = array();
	/**
	 * @var bool Is this plugin active? By default they are all deactivated
	 */
	public $active = false;

	/**
	 * Add a hook
	 *
	 * @param string $hook_name     The hook to attach to
	 * @param string $function_name The function to attach
	 *
	 * @return void
	 */
	public function addHook($hook_name, $function_name) {
		$this->hooks[$hook_name] = $function_name;
		$this->description = htmlspecialchars($this->description);
	}
}

/**
 * Plugin interface.
 * A plugin should implement this to assure at least the basics are correct
 *
 * @category  PHP
 * @package   MvBlog
 * @author    Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright 2005-2008 Michiel van Baak
 * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @version   Release: %%VERSION%%
 * @link      http://www.mvblog.org
 */
interface MvBlog_pluginiface {
	/**
	 * Method to add a plugin hook
	 *
	 * @param string $hook_name     The hook to attach to
	 * @param string $function_name The function to run in this hook
	 *
	 * @return void
	 */
	public function addHook($hook_name, $function_name);
	/**
	 * Method to activate a plugin
	 *
	 * @return void
	 */
	public function activate();
	/**
	 * Method to deactivate a plugin
	 *
	 * @return void
	 */
	public function deactivate();
}
?>
