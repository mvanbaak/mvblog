function ajaxfilemanager(field_name, url, type, win) {
	var ajaxfilemanagerurl = "../../../tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php";
	switch (type) {
		case "image":
			ajaxfilemanagerurl += "?type=img";
			break;
		case "media":
			ajaxfilemanagerurl += "?type=media";
			break;
		case "flash": //for older versions of tinymce
			ajaxfilemanagerurl += "?type=media";
			break;
		case "file":
			ajaxfilemanagerurl += "?type=files";
			break;
		default:
			return false;
	}
	var fileBrowserWindow = new Array();
	fileBrowserWindow["file"]           = ajaxfilemanagerurl;
	fileBrowserWindow["title"]          = "TinyMCE Ajax File Manager";
	fileBrowserWindow["width"]          = "782";
	fileBrowserWindow["height"]         = "440";
	fileBrowserWindow["close_previous"] = "no";
	tinyMCE.openWindow(fileBrowserWindow, {
		window    : win,
		input     : field_name,
		resizable : "yes",
		inline    : "yes",
		editor_id : tinyMCE.getWindowArg("editor_id")
	});

	return false;
}
