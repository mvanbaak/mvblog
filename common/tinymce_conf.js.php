<?php
/**
 * MvBlog -- An open source no-nosense blogtool
 *
 * Copyright (C) 2005-2008, Michiel van Baak
 * Michiel van Baak <mvanbaak@users.sourceforge.net>
 *
 * See http://dev.mvblog.org for more information on MvBlog.
 * That page also provides Bugtrackers, Filereleases etc.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 * PHP version 5
 *
 * @category  PHP
 * @package   MvBlog
 * @author    Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright 2005-2008 Michiel van Baak
 * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @version   SVN: $Revision: 809 $
 * @link      http://www.mvblog.org
 */
?>
tinyMCE.init({
	<?php
	if (array_key_exists("autoload", $_REQUEST) && $_REQUEST["autoload"]) {
		echo "mode : \"exact\",\n";
	} else {
		echo "mode : \"none\",\n";
	}
	?>
	elements                           : "editor_area",
	theme                              : "advanced",
	<?php
	if (array_key_exists("mode", $_REQUEST) && $_REQUEST["mode"] == "BBC") {
		echo "plugins : \"spellchecker,inlinepopups,table,advhr,advimage,advlink,flash,paste,noneditable,contextmenu,bbcode\",\n";
	} else {
		echo "plugins : \"spellchecker,inlinepopups,table,advhr,advimage,advlink,flash,paste,noneditable,contextmenu\",\n";
	}
	?>
	theme_advanced_buttons1_add_before : "newdocument,separator",
	theme_advanced_buttons1_add        : "fontselect,fontsizeselect",
	theme_advanced_buttons2_add        : "separator,forecolor,backcolor,liststyle",
	theme_advanced_buttons2_add_before : "cut,copy,paste,pastetext,pasteword,separator,",
	theme_advanced_buttons3_add_before : "tablecontrols,separator",
	theme_advanced_buttons3_add        : "flash,advhr,separator,spellchecker",
	theme_advanced_toolbar_location    : "top",
	theme_advanced_toolbar_align       : "left",
	extended_valid_elements            : "hr[class|width|size|noshade]",
	file_browser_callback              : "ajaxfilemanager",
	paste_use_dialog                   : false,
	theme_advanced_resizing            : true,
	theme_advanced_resize_horizontal   : true,
	apply_source_formatting            : true,
	force_br_newlines                  : true,
	force_p_newlines                   : false,       
	relative_urls                      : false,
	width                              : "740",
	height                             : "420"
});
