var inputId = 'search';
					// This is the id on the input/textarea that you want to use as the query.

var outputId = 'searchresults';
 					// use this to have the results populate your own ID'd tag.
					// leave it blank and a div tag will automatically be added
					// with an id="liveSearchResults"

var processURI    = 'common/livesearch.php';
					// this is the file that you request data from.

var emptyString   = '';
					// What to display in the results field when there's nothing
					// Leaving this null will cause the results field to be set to display: none

/*--------------------------------------
	Script Stuff
--------------------------------------*/
var liveReq = false;
var t = null;
var liveReqLast = "";
var isIE = false;

var inputElement;
var outputElement;

var isIE = false;

// on !IE we only have to initialize it once
if (window.XMLHttpRequest) {
	liveReq = new XMLHttpRequest();
} else {
	isIE = true;
}

function addLoadEvent(func) {
	var oldonload = window.onload;
	if (typeof window.onload != 'function') {
		window.onload = func;
	} else {
		window.onload = function() {
			oldonload();
			func();
		}
	}
}

function liveReqInit() {

	inputElement  = document.getElementById(inputId);
	outputElement = document.getElementById(outputId);

	if( inputElement == null || outputElement == null )
		return;


	if (navigator.userAgent.indexOf("Safari") > 0) {
		inputElement.addEventListener("keydown",liveReqStart,false);

	} else if (navigator.product == "Gecko") {
		inputElement.addEventListener("keypress",liveReqStart,false);

	} else {
		inputElement.attachEvent('onkeydown',liveReqStart);
		isIE = true;
	}

	if(emptyString == '') {
		// set the result field to hidden, or to default string
		outputElement.style.display = "none";
	} else {
		outputElement.innerHTML = emptyString;
	}
}

addLoadEvent(liveReqInit);

function liveReqStart() {
	if (t) {
		window.clearTimeout(t);
	}
	t = window.setTimeout("liveReqDoReq()",400);
}

function liveReqDoReq() {
	if (liveReqLast != inputElement.value && inputElement.value != "") {
		if (liveReq && liveReq.readyState < 4) {
			liveReq.abort();
		}
		if (window.XMLHttpRequest) {
		// branch for IE/Windows ActiveX version
		} else if (window.ActiveXObject) {
			liveReq = new ActiveXObject("Microsoft.XMLHTTP");
		}

		liveReq.onreadystatechange = liveReqProcessReqChange;
		var uri = processURI + "?s=" + encodeURI(inputElement.value);
		liveReq.open("GET", processURI + "?s=" + encodeURI(inputElement.value));
		liveReqLast = inputElement.value;
		liveReq.send(null);
	} else if(inputElement.value == "") {
		if(emptyString == '') {
			outputElement.innerHTML = '';
			outputElement.style.display = "none";
		} else {
			outputElement.innerHTML = emptyString;
		}
	}
}

function liveReqProcessReqChange() {
	if (liveReq.readyState == 4) {
		var content = new String("<ul>");
		var items = liveReq.responseXML.getElementsByTagName("item");
		for (var i = 0; i < items.length; i++) {
			content += new String("<li><a href=\"index.php?action=view&amp;id=");
			content += new String(getElementTextNS("","articleID",items[i],0));
			content += new String("\">");
			content += new String(getElementTextNS("","articleTitle",items[i],0));
			content += new String("</a></li>");
		}
		content += new String("</ul>");
		outputElement.innerHTML = content;
		if(emptyString == '') {
			outputElement.style.display = "block";
		}
	}
}

function getElementTextNS(prefix, local, parentElem, index) {
	var result = "";
	if (isIE) {
		// IE/Windows way of handling namespaces
		result = parentElem.getElementsByTagName(local)[index];
	} else {
		// the namespace versions of this method
		// (getElementsByTagNameNS()) operate
		// differently in Safari and Mozilla, but both
		// return value with just local name, provided
		// there aren't conflicts with non-namespace element
		// names
		result = parentElem.getElementsByTagName(local)[index];
	}
	if (result) {
		// get text, accounting for possible
		// whitespace (carriage return) text nodes
		if (result.childNodes.length > 1) {
			return result.childNodes[1].nodeValue;
		} else {
			return result.firstChild.nodeValue;
		}
	} else {
		return "n/a";
	}
}
