/**
 * MvBlog -- An open source no-nosense blogtool
 *
 * Copyright (C) 2005-2007, Michiel van Baak
 * Michiel van Baak <mvanbaak@users.sourceforge.net>
 *
 * See http://www.mvblog.org for more information on MvBlog.
 * That page also provides Bugtrackers, Filereleases etc.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 * @package MvBlog
 * @author Michiel van Baak
 * @version %%VERSION%%
 * @copyright 2005-2007 Michiel van Baak
 */
function createXMLHttpObject() {
	if (window.ActiveXObject) {
		try {
			/* new method */
			var ax = new ActiveXObject("MSXML2.XMLHTTP.3.0");
		} catch(e) {
			/* old method */
			var ax = new ActiveXObject("Microsoft.XMLHttp");
		}
		return ax;
	} else if (window.XMLHttpRequest) {
		return new XMLHttpRequest();
	} else {
		return false;
	}
}

function loadXMLContent(url) {
	var xmlhttp = createXMLHttpObject();
	/* exec the call */
	xmlhttp.open("GET", url, false)
	xmlhttp.send(' ')
	if (typeof(xmlhttp.status) == "undefined" || xmlhttp.status == 200 || xmlhttp.status == 205) {
		var ret = new String(xmlhttp.responseText);
		if (ret == 'null') {
			ret = '';
		}
		return ret;
	}
}
