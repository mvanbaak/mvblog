<?php
/**
 * MvBlog -- An open source no-nosense blogtool
 *
 * Copyright (C) 2005-2008, Michiel van Baak
 * Michiel van Baak <mvanbaak@users.sourceforge.net>
 *
 * See http://dev.mvblog.org for more information on MvBlog.
 * That page also provides Bugtrackers, Filereleases etc.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 * PHP version 5
 *
 * @category  PHP
 * @package   MvBlog
 * @author    Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright 2005-2008 Michiel van Baak
 * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @version   SVN: $Revision: 809 $
 * @link      http://www.mvblog.org
 */
require "mvblog.php";
$mvblog = new MvBlog();

/**
 * Send response to a trackback request
 *
 * @param int    $error    if set, send error response
 * @param string $errorstr The error to send
 *
 * @return void
 */
function tb_response($error=0, $errorstr="") {
	header("Content-Type: text/xml; charset=UTF-8");
	if ($error) {
		echo "<?xml version=\"1.0\" encoding=\"utf-8\"?".">\n";
		echo "<response>\n";
		echo "<error>1</error>\n";
		echo "<message>$errorstr</message>\n";
		echo "</response>";
		die();
	} else {
		echo "<?xml version=\"1.0\" encoding=\"utf-8\"?".">\n";
		echo "<response>\n";
		echo "<error>0</error>\n";
		echo "</response>";
	}
	exit();
}
$tb_id = $_GET["id"];
/* check to see if this article is here */
$sql = sprintf("SELECT COUNT(*) AS count FROM articles WHERE active = 1 AND public = 1 AND id = %d", $tb_id);
$res =& $mvblog->db->query($sql);
$row = $res->fetchRow(MDB2_FETCHMODE_ASSOC);
if (!$row["count"]) {
	tb_response(1, "Invalid post.");
}
$tb_url    = $_POST["url"];
$title     = $_POST["title"];
$excerpt   = $_POST["excerpt"];
$blog_name = $_POST["blog_name"];
$charset   = $_POST["charset"];

if (!$tb_id) {
	tb_response(1, "You need to supply a postID.");
}

if (empty($title) && empty($tb_url) && empty($blog_name)) {
	tb_response(1, "You need to supply some data.");
}

$sql = sprintf(
	"INSERT INTO comments (name, website, email, comment, date, articles_id, title) 
	VALUES ('%s', '%s', '%s', '%s', %d, %d, '%s'",
	preg_quote(strip_tags($blog_name), "'"),
	preg_quote(strip_tags($tb_url), "'"),
	" ",
	"<strong>".preg_quote(strip_tags($title), "'")."</strong>\n\n".preg_quote(strip_tags($excerpt), "'"),
	mktime(),
	$tb_id,
	preg_quote(strip_tags($title), "'")
);
$res =& $mvblog->db->exec($sql);
if (PEAR::isError($res)) {
	tb_response(1, "Something went wrong with inserting your trackback in the comments system. error:".$res->getMessage());
}
tb_response();
?>
