<?php
/**
 * MvBlog -- An open source no-nosense blogtool
 *
 * Copyright (C) 2005-2008, Michiel van Baak
 * Michiel van Baak <mvanbaak@users.sourceforge.net>
 *
 * See http://dev.mvblog.org for more information on MvBlog.
 * That page also provides Bugtrackers, Filereleases etc.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 * PHP version 5
 *
 * @category  PHP
 * @package   MvBlog
 * @author    Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright 2005-2008 Michiel van Baak
 * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @version   SVN: $Revision: 809 $
 * @link      http://www.mvblog.org
 */
/*
 * Start the autoloader, so we never have to include anything
 */
require_once "mvblog_autoloader.php";
$MvBlog_AutoLoader = new MvBlog_AutoLoader();

$pathInfo = pathinfo(__FILE__);
$MvBlog_AutoLoader->registerPath($pathInfo["dirname"], "%s.php", MvBlog_AutoLoader::OPT_LOWERCASE);

/**
 * Register the AutoLoader object as the autoloader.
 *
 * @param string $className The class to load
 *
 * @return void
 */
function __autoload($className) {
	global $MvBlog_AutoLoader;
	$MvBlog_AutoLoader->autoload($className);
}

/* Start heavy error reporting if we're on a dev site */
MvBlog_debug::start_development(false);

/* Read the configuration file */
$configfile = dirname(dirname(__FILE__)."../")."/conf/mvblog.ini";
$availSettings = array(
	"general" => array(
		"debug"    => array("type" => MvBlog_IniFileReader::TYPE_BOOL, "default" => "no"),
	),
	"database" => array(
		"database" => array("type" => MvBlog_IniFileReader::TYPE_STRING, "default" => "mvblog"),
		"hostname" => array("type" => MvBlog_IniFileReader::TYPE_STRING, "default" => "localhost"),
		"username" => array("type" => MvBlog_IniFileReader::TYPE_STRING, "default" => "mvblog"),
		"password" => array("type" => MvBlog_IniFileReader::TYPE_STRING, "default" => "mvblog"),
		"type"     => array("type" => MvBlog_IniFileReader::TYPE_STRING, "default" => "mysql"),
	),
);
$config = new MvBlog_IniFileReader($availSettings, $configfile);
/**
 * Uprade mvblog to the latest version
 *
 * @category  PHP
 * @package   MvBlog
 * @author    Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright 2005-2008 Michiel van Baak
 * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @version   Release: %%VERSION%%
 * @link      http://www.mvblog.org
 */
Class Mvblog_upgrade extends Mvblog_common {
	/* variables */
	/**
	 * @var int Latest patch that was applied
	 */
	public $lastpatch = 0;
	/**
	 * @var string The language to use
	 */
	public $lang;
	/* methods */
	/* __construct {{{ */
	/**
	 * Class constructor
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct("plugins/", 0, 1);
	}
	/* }}} */
	/* get_patchfiles {{{ */
	/**
	 * Find out what patchfiles we need to commit.
	 *
	 * @param string $mode Can be svn or release and is used to pick the filenameformat.
	 *
	 * @return array Patchfiles that should be applied to the database
	 */
	public function get_patchfiles($mode = "release") {
		/* get current database version */
		if (!array_key_exists("dbversion", $this->settings)) {
			$current_version = 0;
		} else {
			$current_version = $this->settings["dbversion"];
		}
		/* read the db patchfiles into an array */
		$patchfiles = array();
		if ($dh = opendir(sprintf("upgrades/%s", $this->db->phptype))) {
			while (false !== ($v = readdir($dh))) {
				if (!is_dir($v) && $v != "README") {
					if ($mode == "release") {
						if (strpos($v, "to") && substr($v, 0, strpos($v, "to")) >= $current_version) {
							$patchfiles[] = $v;
						}
					} else {
						if (!strpos($v, "to") && basename($v, ".php") > $current_version) {
							$patchfiles[] = $v;
						}
					}
				}
			}
		}
		return $patchfiles;
	}
	/* }}} */
	/* sql_addslashes {{{ */
	/**
	 * Better addslashes for SQL queries.
	 * Taken from phpMyAdmin and modified by me.
	 *
	 * @param string $a_string the string to escape
	 * @param bool   $is_like  if set to true this string is going to be used in a like
	 *
	 * @return string the escaped string
	 */
	function sql_addslashes($a_string = '', $is_like = false) {
		if ($is_like) {
			$a_string = str_replace('\\', '\\\\\\\\', $a_string);
		} else {
			$a_string = str_replace('\\', '\\\\', $a_string);
		}
		$a_string = str_replace('\"', '\\\"', $a_string);
		return str_replace('\'', '\\\'', $a_string);
	} 
	/* }}} */
	/* create_backups {{{ */
	/**
	 * Create a database backup before running the pathes against it so we can rollback when something goes wrong
	 *
	 * @return bool true if the backup is created, false otherwise
	 */
	public function create_backups() {
		$backuplocation = "backups/";
		$dbname         = $this->db->database_name;
		if (!is_dir($backuplocation) && !is_writable($backuplocation)) {
			die("Backupdir not available. Please create a directory 'backups' and give the webserver user write permissions on it.");
		}
		// make backup
		switch ($this->db->phptype) {
		case "sqlite" :
			//copy old database
			$src = sprintf("%s", $dbname);
			$dst = sprintf("backups/%s-%s", $dbname, date("YmdHi"));
			if (copy($src, $dst)) {
				return true;
			} else {
				return false;
			}
			break;
		case "mysql" :
			//create database dump
			return $this->_mysql_backup_db($dbname, $backuplocation);
			break;
		case "pgsql" :
			//create database dump
			return $this->_psql_backup_db($dbname, $backuplocation);
			break;
		default :
			die("unknown database type. Please report on http://dev.mvblog.org");
			break;
		}
	}
	/* }}} */
	/* _mysql_backup_db {{{ */
	/**
	 * Create mysql backup
	 *
	 * @param string $database       The database name to backup
	 * @param string $backuplocation The directory to store the backup in
	 *
	 * @return bool true on success, false on failure
	 */
	private function _mysql_backup_db($database, $backuplocation) {
		//variable to hold the database
		$dbdata  = "-- MvBlog upgrade backup dump\n";
		$dbdata .= "--\n";
		$dbdata .= sprintf("-- Host: %s\n", $this->db->dsn["hostspec"]);
		$dbdata .= sprintf("-- Database: %s\n", $database);
		$dbdata .= sprintf("-- Dump started on %s\n\n", date("Y-m-d H:i:s"));
		$dbdata .= "/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;\n";
		$dbdata .= "/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;\n";
		$dbdata .= "/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;\n";
		$dbdata .= "/*!40101 SET NAMES utf8 */;\n";
		$dbdata .= "/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;\n";
		$dbdata .= "/*!40103 SET TIME_ZONE='+00:00' */;\n";
		$dbdata .= "/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;\n";
		$dbdata .= "/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;\n";
		$dbdata .= "/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;\n";
		$dbdata .= "/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;\n\n";
		// get the tables in the database
		$sql = "SHOW TABLES";
		$res = $this->db->query($sql);
		while ($row = $res->fetchRow()) {
			$table = $row[0];
			$dbdata .= sprintf("--\n-- Table structure for table '%s'\n--\n\n", $table);
			$dbdata .= sprintf("DROP TABLE IF EXISTS %s;\n", $table);
			$q = sprintf("SHOW CREATE TABLE %s", $table);
			$r = $this->db->query($q);
			while ($tabledata = $r->fetchRow()) {
				$dbdata .= $tabledata[1].";\n\n";
			}
			$dbdata .= sprintf("--\n-- Dumping data for table '%s'\n--\n\n", $table);
			$dbdata .= sprintf("LOCK TABLES `%s` WRITE;\n", $table);
			$dbdata .= sprintf("/*!40000 ALTER TABLE `%s` DISABLE KEYS */;\n", $table);

			// get table structure
			$q = sprintf("DESCRIBE %s", $table);
			$r = $this->db->query($q);
			$tablestruct = array();
			while ($ts = $r->fetchRow(MDB2_FETCHMODE_ASSOC)) {
				$tablestruct[] = $ts;
			}
			// find fields that dont need ' as data enclosure
			$ints = array();
			foreach ($tablestruct as $struct) {
				if (strpos($struct["type"], "bigint") === 0
				    || strpos($struct["type"], "int") === 0
				    || strpos($struct["type"], "mediumint") === 0
				    || strpos($struct["type"], "smallint") === 0
				    || strpos($struct["type"], "tinyint") === 0
				    || strpos($struct["type"], "timestamp") === 0
				) {
					$ints[strtolower($struct["field"])] = 1;
				}
			}
			$q = sprintf("SELECT * FROM %s", $table);
			$r = $this->db->query($q);
			// \x08\\x09, not required
			$search = array("\x00", "\x0a", "\x0d", "\x1a");
			$replace = array('\0', '\n', '\r', '\Z');
			while ($tabledata = $r->fetchRow(MDB2_FETCHMODE_ASSOC)) {
				$dbdata .= sprintf("INSERT INTO `%s` VALUES (", $table);
				$values = array();
				foreach ($tabledata as $k=>$v) {
					if (array_key_exists($k, $ints)) {
						$values[] = $v;
					} else {
						$values[] = "'".str_replace($search, $replace, $this->sql_addslashes($v))."'";
					}
				}
				$dbdata .= implode(",", $values);
				$dbdata .= ");\n";
			}
			$dbdata .= sprintf("/*!40000 ALTER TABLE `%s` ENABLE KEYS */;\n", $table);
			$dbdata .= "UNLOCK TABLES;\n\n";
		}
		$dbdata .= "/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;\n\n";
		$dbdata .= "/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;\n";
		$dbdata .= "/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;\n";
		$dbdata .= "/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;\n";
		$dbdata .= "/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;\n";
		$dbdata .= "/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;\n";
		$dbdata .= "/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;\n";
		$dbdata .= "/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;\n\n";
		$dbdata .= sprintf("-- Dump completed on %s", date("Y-m-d H:i:s"));

		$backupfile = sprintf($backuplocation."%s-%s.mysql", $database, date("YmdHi"));
		return file_put_contents($backupfile, $dbdata);
	}
	/* }}} */
	/* _psql_backup_db {{{ */
	/**
	 * Create postgresql backup. Code and ideas borrowed from phpPgAdmin.
	 *
	 * @param string $database       The database name to backup
	 * @param string $backuplocation The directory to store the backup in
	 *
	 * @return bool true on success, false on failure
	 */
	private function _psql_backup_db($database, $backuplocation) {
		// set enviroment vars needed by pg_dump
		$server_info = array();
		$server_info["username"] = $this->db->dsn["username"];
		$server_info["password"] = $this->db->dsn["password"];
		$server_info["host"]     = $this->db->dsn["hostspec"];
		$server_info["port"]     = $this->db->dsn["port"];

		putenv('PGPASSWORD=' . $server_info['password']);
		putenv('PGUSER=' . $server_info['username']);
		$hostname = $server_info['host'];
		if ($hostname !== null && $hostname != '') {
			putenv('PGHOST=' . $hostname);
		}
		$port = $server_info['port'];
		if ($port !== null && $port != '') {
			putenv('PGPORT=' . $port);
		}

		$backupfile = sprintf($backuplocation."%s-%s.psql", $database, date("YmdHi"));
		$cmd = "pg_dump -i ";
		$cmd .= sprintf("-f %s %s", $backupfile, $database);
		$output = system($cmd, $retval);
		if ($retval === 0) {
			return true;
		} else {
			return false;
		}
	}
	/* }}} */
	/* set_latest_version {{{ */
	/**
	 * Update/create dbversion setting in the database
	 *
	 * @param int $version The latest patchlevel we applied to the database
	 *
	 * @return bool true on success, false on failure
	 */
	public function set_latest_version($version = 0) {
		if (array_key_exists("dbversion", $this->settings) && $this->settings["dbversion"]) {
			/* update the entry */
			$sql = sprintf("UPDATE settings SET settingvalue = '%s' WHERE settingname = 'dbversion'", $version);
		} else {
			/* create the entry */
			$sql = sprintf("INSERT INTO settings (settingname, settingvalue) VALUES ('dbversion', '%s')", $version);
		}
		$res = $this->db->exec($sql);
		if (PEAR::isError($res)) {
			return false;
		} else {
			return true;
		}
	}
	/* }}} */
	/* apply_dbpatch {{{ */
	/**
	 * Apply a specific database patch
	 *
	 * @param int    $patch The patch to apply
	 * @param string $mode  empty or relase
	 *
	 * @return bool true on success, false on failure
	 */
	public function apply_dbpatch($patch, $mode = "release") {
		$patchfile = sprintf("upgrades/%s/%s", $this->db->phptype, $patch);
		if (file_exists($patchfile)) {
			include_once $patchfile;
			foreach ($sql as $query) {
				$res = $this->db->exec($query);
				if (PEAR::isError($res)) {
					return false;
				}
			}
		}
		if ($mode == "release") {
			$this->lastpatch = substr(basename($patch, ".php"), strpos($patch, "to")+2);
		} else {
			$this->lastpatch = basename($patch, ".php");
		}
		return true;
	}
	/* }}} */
}
?>
