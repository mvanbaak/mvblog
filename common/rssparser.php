<?php
/**
 * MvBlog -- An open source no-nosense blogtool
 *
 * Copyright (C) 2005-2008, Michiel van Baak
 * Michiel van Baak <mvanbaak@users.sourceforge.net>
 *
 * See http://dev.mvblog.org for more information on MvBlog.
 * That page also provides Bugtrackers, Filereleases etc.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 * PHP version 5
 *
 * @category  PHP
 * @package   MvBlog
 * @author    Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright 2005-2008 Michiel van Baak
 * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @version   SVN: $Revision: 809 $
 * @link      http://www.mvblog.org
 */
/**
 * Class to parse rssfeeds
 *
 * @category  PHP
 * @package   MvBlog
 * @author    Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright 2005-2008 Michiel van Baak
 * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @version   Release: %%VERSION%%
 * @link      http://www.mvblog.org
 */
class RSSParser {

	var $saveItems = array(
		"title"       => "string",
		"link"        => "string",
		"description" => "string",
		"dc:creator"  => "string",
		"dc:date"     => "date",
		"dc:subject"  => "string",
	);

	/**
	 * Read and parse a RSS feed which $url points at (may be an URL or local file)
	 *
	 * @param string $rssdata string containing the URL to the RSS feed. May be an URL or local file
	 *
	 * @return void
	 */
	function RSSParser($rssdata) {
		$this->rssData = $rssdata;
		$this->readItems();

	}

	/**
	 * Construct a new empty RSS item
	 *
	 * @return a new empty assoc. array representing an rss item
	 */
	function newItem() {
		$retItem = array();

		foreach (array_keys($this->saveItems) as $key) {
			$retItem[$key] = "";
		}

		return($retItem);
	}

	/**
	 * Read <item> segments from the RSS file and stuff them in an array.
	 *
	 * @return void
	 */
	function readItems() {
		if (!isset($this->rssData)) {
			return(-1);
		}

		$this->rssItems = array();

		$parser = xml_parser_create();

		xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
		xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);

		xml_parse_into_struct($parser, $this->rssData, $values, $tags);
		xml_parser_free($parser);

		// Loop through all the elements in the RSS XML file.  If an <item> tag
		// is found, it's children will be added to a array until the closing
		// tag is found. Then the array is added to a list of items
		// ($this->rssItems).
		for ($i=1; $i < count($values); $i++) {

			$tagName = "";
			$tagType = "";
			$tagValue = "";

			if (array_key_exists("tag", $values[$i])) {
				$tagName = $values[$i]["tag"];
			}
			if (array_key_exists("type", $values[$i])) {
				$tagType = $values[$i]["type"];
			}
			if (array_key_exists("value", $values[$i])) {
				$tagValue = $values[$i]["value"];
			}

			if ($values[$i]["tag"] == "item" && $values[$i]["type"] == "open") {
				// Looks like we found an <item> tag. Create a new array to
				// store it's children values as they will be found on the next
				// iteration of the loop.
				$rssItem = $this->newItem();
			}
			if ($values[$i]["tag"] == "item" && $values[$i]["type"] == "close" && isset($rssItem)) {
				// </item> tag closed. Store the read item information.
				$this->rssItems[] = $rssItem;
				unset($rssItem); // No item information will be saved when this doesn't exist.
			}

			if (array_key_exists($tagName, $this->saveItems) && isset($rssItem)) {
				// Found a tag that we want to store and that's part of an
				// <item>. Save it.
				switch($this->saveItems[$tagName]) {
					case "string":
						$rssItem[$tagName] = $tagValue;
						break;
					case "date":
						$rssItem[$tagName] = strtotime($tagValue);
						break;
					default:
						print("Don't know how to handle type ".$this->saveItems[$tagName].". Aborting.");
						exit(1);
						break;
				}
			}
		}

	}
}
?>
