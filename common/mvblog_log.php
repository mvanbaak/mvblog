<?php
/**
 * MvBlog -- An open source no-nosense blogtool
 *
 * Copyright (C) 2005-2008, Michiel van Baak
 * Michiel van Baak <mvanbaak@users.sourceforge.net>
 *
 * See http://dev.mvblog.org for more information on MvBlog.
 * That page also provides Bugtrackers, Filereleases etc.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 * PHP version 5
 *
 * @category  PHP
 * @package   MvBlog
 * @author    Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright 2005-2008 Michiel van Baak
 * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @version   SVN: $Revision: 809 $
 * @link      http://www.mvblog.org
 */
/**
 * Class that holds methods to create/read action log.
 *
 * @category  PHP
 * @package   MvBlog
 * @author    Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright 2005-2008 Michiel van Baak
 * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @version   Release: %%VERSION%%
 * @link      http://www.mvblog.org
 */
Class MvBlog_log extends MvBlog_common {
	/* constants */
	/* variables */
	/* methods */
	/* __construct {{{ */
	/**
	 * Class constructor.
	 *
	 * @param string $basedir   The basedir where plugins etc live
	 * @param string $adminmode If set, run in adminmode for the backend
	 *
	 * @return void
	 */
	public function __construct($basedir = "", $adminmode = 0) {
		parent::__construct($basedir."plugins/", $adminmode);
	}
	/* }}} */
	/* data setters */
	/* add_log {{{ */
	/**
	 * Add a logentry to the database
	 *
	 * @param int    $datetime  Entry timestamp
	 * @param int    $user_id   The userid who generated event
	 * @param int    $user_type The type of user. 1 for admin, 2 for bloguser
	 * @param string $msg       The log message
	 *
	 * @return bool true on success, false on failure
	 */
	public function add_log($datetime, $user_id, $user_type, $msg) {
		$sql = sprintf(
			"INSERT INTO log VALUES (%d, %d, %d, '%s');",
			$datetime, $user_id, $user_type, $msg
		);
		$res = $this->db->query($sql);
		if (PEAR::isError($res)) {
			return false;
		}
		return true;

	}
	/* }}} */
	/* data getters */
	/* get_log {{{ */
	/**
	 * Get logrecords
	 *
	 * @param int $user_type If 1 only show author actions, if 2 only show user actions, if 0 show all actions.
	 * @param int $count     If set only show this many logrecords
	 * @param int $ts_start  If set, start showing records from this time
	 * @param int $ts_end    If set, stop showing records at this time
	 *
	 * @return array All logrecords matching the criteria
	 */
	public function get_log($user_type = 0, $count = 25, $ts_start = 0, $ts_end = 0) {
		$logrecords = array();
		switch ($user_type) {
		case 1:
		case 2:
			$sql = sprintf("SELECT * FROM log WHERE user_type = %d ORDER BY time DESC", $user_type);
			break;
		default:
			$sql = "SELECT * FROM log ORDER BY time DESC";
			break;
		}
		$res = $this->db->query($sql);
		while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
			$row["human_time"] = date("Y-m-d H:i:s", $row["time"]);
			if ($row["user_type"] == 1) {
				$row["username"] = $this->authors[$row["user_id"]]["login"];
				$row["fullname"] = $this->authors[$row["user_id"]]["fullname"];
			} elseif ($row["user_type"] == 2) {
				$row["username"] = $this->users[$row["user_id"]]["username"];
				$row["fullname"] = $this->users[$row["user_id"]]["realname"];
			}
			$logrecords[] = $row;
		}
		return $logrecords;
	}
	/* }}} */
	/* output functions */
	/* show_log {{{ */
	/**
	 * Show log overview
	 *
	 * @param int $user_type 1 for admin, 2 for users, 0 for everything
	 *
	 * @return void
	 */
	public function show_log($user_type = 1) {
		$logrecords = $this->get_log($user_type);
		echo "<table style=\"border: 1px solid black;\"><tr>\n";
		echo "\t<td style=\"border: 1px solid black;\">date</td>";
		echo "<td style=\"border: 1px solid black;\">login</td><td style=\"border: 1px solid black;\">fullname</td>\n";
		echo "</tr>";
		foreach ($logrecords as $logrecord) {
			echo "<tr>\n";
			echo sprintf("\t<td style=\"border: 1px solid black;\">%s</td>", $logrecord["human_time"]);
			echo sprintf("<td style=\"border: 1px solid black;\">%s</td>", $logrecord["username"]);
			echo sprintf("<td style=\"border: 1px solid black;\">%s</td>\n", $logrecord["fullname"]);
			echo "</tr><tr>\n";
			echo sprintf("\t<td colspan=\"3\" style=\"border: 1px solid black;\">%s</td>\n", $logrecord["msg"]);
			echo "</tr>";
		}
		echo "</table>";
	}
	/* }}} */
}
