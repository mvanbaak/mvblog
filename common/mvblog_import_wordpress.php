<?php
/**
 * MvBlog -- An open source no-nosense blogtool
 *
 * Copyright (C) 2005-2008, Michiel van Baak
 * Michiel van Baak <mvanbaak@users.sourceforge.net>
 *
 * See http://dev.mvblog.org for more information on MvBlog.
 * That page also provides Bugtrackers, Filereleases etc.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 * PHP version 5
 *
 * @category  PHP
 * @package   MvBlog
 * @author    Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright 2005-2008 Michiel van Baak
 * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @version   SVN: $Revision: 809 $
 * @link      http://www.mvblog.org
 */

/**
 * Class that holds methods for WordPress import
 *
 * @category  PHP
 * @package   MvBlog
 * @author    Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright 2005-2008 Michiel van Baak
 * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @version   Release: %%VERSION%%
 * @link      http://www.mvblog.org
 */
Class MvBlog_import_wordpress extends MvBlog_common {
	/**
	 * Setup class and decide what to do
	 *
	 * @param array $options Some options
	 *
	 * @return void
	 */
	public function __construct($options) {
		if (!array_key_exists("importaction", $options)) {
			$this->show_main();
		} else {
			switch ($options["importaction"]) {
			case "run_import" :
				$this->run_import($_REQUEST);
				break;
			default:
				echo "nothing to do";
				break;
			}
		}
	}

	/**
	 * Show screen to start import
	 *
	 * @return void
	 */
	public function show_main() {
		echo "<p>This importer allows you to import WordPress data into MvBlog.<br />";
		echo "Your Mileage may vary.<br /><br /></p>\n";
		echo "<p>Your WordPress database settings<br />\n";
		echo "<form name=\"wp_import\" action=\"index.php\" method=\"post\">\n";
		echo "<input type=\"hidden\" name=\"action\" value=\"import\" />\n";
		echo "<input type=\"hidden\" name=\"type\" value=\"wordpress\" />\n";
		echo "<input type=\"hidden\" name=\"importaction\" value=\"run_import\" />\n";
		echo "<table border=\"0\"><tr>\n";
		echo "<td>WordPress Database server:</td>";
		echo "<td><input type=\"text\" style=\"width: 200px;\" name=\"dbserver\" /></td>\n";
		echo "</tr><tr>\n";
		echo "<td>WordPress Database name:</td>";
		echo "<td><input type=\"text\" style=\"width: 200px;\" name=\"dbname\" /></td>\n";
		echo "</tr><tr>\n";
		echo "<td>WordPress Database user:</td>";
		echo "<td><input type=\"text\" style=\"width: 200px;\" name=\"dbuser\" /></td>\n";
		echo "</tr><tr>\n";
		echo "<td>WordPress Database password:</td>";
		echo "<td><input type=\"password\" style=\"width: 200px;\" name=\"dbpass\" /></td>\n";
		echo "</tr><tr>\n";
		echo "<td>WordPress Table prefix:</td>";
		echo "<td><input type=\"text\" style=\"width: 200px;\" name=\"dbprefix\" /></td>\n";
		echo "</tr><tr>\n";
		echo "<td>WordPress Base URL:</td>";
		echo "<td><input type=\"text\" style=\"width: 200px;\" name=\"orig_url\" /></td>\n";
		echo "</tr><tr>\n";
		echo "<td>WordPress Filesystem Location:</td>";
		echo "<td><input type=\"text\" style=\"width: 200px;\" name=\"fs_location\" /></td>\n";
		echo "</tr></table>\n";
		echo "<input type=\"submit\" value=\"".gettext("import")."\" />\n";
		echo "</form>\n";
	}

	/**
	 * Do the actual import
	 *
	 * @param array $data Database information
	 *
	 * @return void
	 */
	public function run_import($data) {
		// first try to copy all files
		$this->copy_files($data["fs_location"]."wp-content/uploads", "", "wp-uploads", 1);
		$category = array();
		// find out if we have enough info for a database connection
		if (!array_key_exists("dbserver", $data) || $data["dbserver"] == ""
		    || !array_key_exists("dbname", $data) || $data["dbname"] == ""
		    || !array_key_exists("dbuser", $data) || $data["dbuser"] == ""
		    || !array_key_exists("dbpass", $data) || $data["dbpass"] == ""
		) {
			echo "not enough information.";
			return false;
		}
		$db = mysql_connect($data["dbserver"], $data["dbuser"], $data["dbpass"]);
		mysql_select_db($data["dbname"], $db);
		//grab WP db version as we only support 2.3 and newer
		$sql = sprintf("SELECT option_value FROM %soptions WHERE option_name='db_version'", $data["dbprefix"]);
		$res = mysql_query($sql);
		$wpdbversion = mysql_result($res, 0);
		if ($wpdbversion < 5539) {
			die("You need at least WP 2.3.0");
		}
		//import categories and maintain array with oldid=>newid so we can link the articles to the correct ids
		/*
		 * WP categories are stored in the table <prefix>terms
		 * The table <prefix>term_taxonomy holds a count for them
		 * The table <prefix>term_relationships links articles and term_taxonomy records together
		 * <posts> -> <term_relationships> -> <term_taxonomy> -> <terms>
		 */
		$sql = sprintf(
			"SELECT %1\$sterms.term_id, %1\$sterms.name, %1\$sterm_taxonomy.term_taxonomy_id 
			FROM %1\$sterms 
			LEFT JOIN %1\$sterm_taxonomy ON %1\$sterm_taxonomy.term_id = %1\$sterms.term_id", $data["dbprefix"]
		);
		$res = mysql_query($sql) or die(mysql_error());
		while ($row = mysql_fetch_assoc($res)) {
			$cat_sql = sprintf(
				"INSERT INTO categories (name, `desc`, public, active) VALUES ('%1\$s', '%1\$s', 1, 1)",
				$row["name"]
			);
			$r = $GLOBALS["admin"]->db->query($cat_sql);
			$category[$row["term_taxonomy_id"]] = $GLOBALS["admin"]->db->lastInsertID("categories", "id");
		}
		//select all articles
		$sql = sprintf("SELECT * FROM %sposts WHERE post_type='post'", $data["dbprefix"]);
		$res = mysql_query($sql);
		while ($row = mysql_fetch_assoc($res)) {
			if ($row["comment_status"] == "open") {
				$allow_anon_comment = 1;
			} else {
				$allow_anon_comment = 0;
			}
			if ($row["post_status"] == "publish" || $row["post_status"] == "static") {
				$public = 1;
			} else {
				$public = 0;
			}
			if (strtotime($row["post_modified"])) {
				$last_modified = strtotime($row["post_modified"]);
			} else {
				$last_modified = 0;
			}
			//get categories
			$q = sprintf("SELECT term_taxonomy_id FROM %sterm_relationships WHERE object_id=%d", $data["dbprefix"], $row["ID"]);
			$r = mysql_query($q);
			$categories = array();
			while ($catr = mysql_fetch_assoc($r)) {
				$categories[] = $category[$catr["term_taxonomy_id"]];
			}
			$data["oldsite"] = $data["orig_url"];
			if (array_key_exists("oldsite", $data) && $data["oldsite"]) {
				$wpuploadurl = sprintf("%s%s", $data["oldsite"], "wp-content/uploads/");
				//find all links pointing to internal links and replace them with the new structure
				preg_match_all("/<[img|a][^>]*[.]*>/si", $row["post_content"], $matches);
				foreach ($matches[0] as $match) {
					//get the href attribute
					preg_match_all("/href=[\"|\'](.+?)[\"|\']/si", $match, $m);
					$filtermatch = $m[1];
					foreach ($filtermatch as $v) {
						if (strstr($v, $data["oldsite"]) && strstr($v, $wpuploadurl)) {
							$row["post_content"] = str_replace(
								$v,
								"site_images/wp-uploads/".str_replace($wpuploadurl, "", $v),
								$row["post_content"]
							);
						}
					}
					unset($m);
					//get the src attribute
					preg_match_all("/src=[\"|\'](.+?)[\"|\']/si", $match, $m);
					$filtermatch = $m[1];
					foreach ($filtermatch as $v) {
						if (strstr($v, $data["oldsite"]) && strstr($v, $wpuploadurl)) {
							$row["post_content"] = str_replace(
								$v,
								"site_images/wp-uploads/".str_replace($wpuploadurl, "", $v),
								$row["post_content"]
							);
						}
					}
				}
			}
			$post_sql = sprintf(
				"INSERT INTO articles 
				(date, title, head, body, authors_id, allowanoncomments, active, public, last_modified, modified_by, categories_ids)
			   	VALUES (%d, '%s', '%s', '%s', %d, %d, 1, %d, %d, %d, '%s')",
				strtotime($row["post_date"]), addslashes($row["post_title"]), addslashes($row["post_excerpt"]), 
				addslashes($row["post_content"]), $_SESSION["author_id"],
				$allow_anon_comment, $public, $last_modified, $_SESSION["author_id"], implode(",", $categories)
			);
			$r = $GLOBALS["admin"]->db->query($post_sql);
			if (PEAR::isError($r)) {
				die($r->getDebugInfo());
			}
		}
		echo "All done !";
	}
}
?>
