<?php
/**
 * MvBlog -- An open source no-nosense blogtool
 *
 * Copyright (C) 2005-2008, Michiel van Baak
 * Michiel van Baak <mvanbaak@users.sourceforge.net>
 *
 * See http://dev.mvblog.org for more information on MvBlog.
 * That page also provides Bugtrackers, Filereleases etc.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 * PHP version 5
 *
 * @category  PHP
 * @package   MvBlog
 * @author    Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright 2005-2008 Michiel van Baak
 * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @version   SVN: $Revision: 809 $
 * @link      http://www.mvblog.org
 */

/**
 * Class that holds methods for exception handling
 *
 * @category  PHP
 * @package   MvBlog
 * @author    Ferry Boender <void@example.com>
 * @copyright 2005-2008 Ferry Boender
 * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @version   Release: %%VERSION%%
 * @link      http://www.mvblog.org
 */
class MvBlog_exception extends Exception {
	/**
	 * @var array $messages Array with exception messages
	 */
	protected $messages = array();

	/**
	 * Class constructor
	 *
	 * @param string $code The error message code
	 *
	 * @return void
	 */
	public function __construct($code) { 
		$args = func_get_args();
		array_shift($args);
		if (array_key_exists($code, $this->messages)) {
			$message = vsprintf($this->messages[$code], $args);
		} else {
			$message = "Unknown error ".$code;
		}

		parent::__construct($message, $code);
	}
}
?>
