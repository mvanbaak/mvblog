<?php
/**
 * MvBlog -- An open source no-nosense blogtool
 *
 * Copyright (C) 2005-2008, Michiel van Baak
 * Michiel van Baak <mvanbaak@users.sourceforge.net>
 *
 * See http://dev.mvblog.org for more information on MvBlog.
 * That page also provides Bugtrackers, Filereleases etc.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 * PHP version 5
 *
 * @category  PHP
 * @package   MvBlog
 * @author    Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright 2005-2008 Michiel van Baak
 * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @version   SVN: $Revision: 846 $
 * @link      http://www.mvblog.org
 */
/**
 * Uprade mvblog to the latest version
 */
if (array_key_exists("mode", $_REQUEST)) {
	$mode = $_REQUEST["mode"];
} else {
	$mode = "";
}

require_once "common/mvblog_upgrade.php";
$updater = new Mvblog_upgrade();

if (array_key_exists("dbversion", $updater->settings)) {
	$latest_version = $updater->settings["dbversion"];
} else {
	$latest_version = 0;
}

echo "looking up files for db type <b>";
echo $updater->db->phptype;
echo "</b> starting at version <b>".$latest_version."</b>...<br />";


$patches = $updater->get_patchfiles($mode);
if (!count($patches)) {
	die("nothing to do. Your mvblog is already up-to-date.");
}

echo "Creating backup of your current database<br />";
$backup = $updater->create_backups();

foreach ($patches as $patch) {
	if (!$updater->apply_dbpatch($patch, $mode)) {
		die("something went wrong with patchfile $patch. Rollback to backup.");
	}
	echo "applied patchfile $patch. <br>";
	/* put last version in var so we can update settings table later */
	if ($updater->lastpatch > $latest_version) {
		$latest_version = $updater->lastpatch;
	}
}
$updater->set_latest_version($latest_version);
echo "Your MvBlog installation is updated. Have fun.";
?>
