<?php
/**
 * MvBlog -- An open source no-nosense blogtool
 *
 * Copyright (C) 2005-2008, Michiel van Baak
 * Michiel van Baak <mvanbaak@users.sourceforge.net>
 *
 * See http://dev.mvblog.org for more information on MvBlog.
 * That page also provides Bugtrackers, Filereleases etc.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 * PHP version 5
 *
 * @category  PHP
 * @package   MvBlog
 * @author    Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright 2005-2008 Michiel van Baak
 * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @version   SVN: $Revision: 809 $
 * @link      http://www.mvblog.org
 */
require_once "PHPUnit/Framework.php";
require_once "../common/mvblog.php";
$mvblog = new mvblog();

/**
 * Class to test the MvBlog_IniParser class
 *
 * @category  PHP
 * @package   MvBlog
 * @author    Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright 2005-2008 Michiel van Baak
 * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @version   Release: %%VERSION%%
 * @link      http://www.mvblog.org
 */
class MvBlog_IniParser_test extends PHPUnit_Framework_TestCase {

	/**
	 * Setup class
	 *
	 * @return void
	 */
	public function __construct() {
		$this->availableSettings = array(
			"section1" => array(
				"string1" => array("type" => MvBlog_IniParser::TYPE_STRING, "default" => "string1"),
				"bool1"   => array("type" => MvBlog_IniParser::TYPE_BOOL,   "default" => "1"),
				"bool2"   => array("type" => MvBlog_IniParser::TYPE_BOOL,   "default" => "on"),
				"bool3"   => array("type" => MvBlog_IniParser::TYPE_BOOL,   "default" => "true"),
				"int1"    => array("type" => MvBlog_IniParser::TYPE_INT,    "default" => "1"),
			)
		);
	}

	/**
	 * test getting a default setting
	 *
	 * @return void
	 */
	public function testGetSetting_Default() {
		$ip = new MvBlog_IniParser($this->availableSettings);
		$this->assertEquals($ip->getSetting("section1", "string1"), "string1");
	}

	/**
	 * test Setting a string value
	 *
	 * @return void
	 */
	public function testSetSetting_String() {
		$ip = new MvBlog_IniParser($this->availableSettings);
		$ip->setSetting("section1", "string1", "string1");
		$this->assertEquals($ip->getSetting("section1", "string1"), "string1");
	}

	/**
	 * test setting a bool to true
	 *
	 * @return void
	 */
	public function testSetSetting_BoolTrue() {
		$ip = new MvBlog_IniParser($this->availableSettings);
		$ip->setSetting("section1", "bool1", true);
		$this->assertTrue($ip->getSetting("section1", "bool1"));
	}

	/**
	 * test setting a bool to on
	 *
	 * @return void
	 */
	public function testSetSetting_BoolOn() {
		$ip = new MvBlog_IniParser($this->availableSettings);
		$ip->setSetting("section1", "bool1", "on");
		$this->assertTrue($ip->getSetting("section1", "bool1"));
	}

	/**
	 * test setting a bool to 1
	 *
	 * @return void
	 */
	public function testSetSetting_BoolOneInt() {
		$ip = new MvBlog_IniParser($this->availableSettings);
		$ip->setSetting("section1", "bool1", 1);
		$this->assertTrue($ip->getSetting("section1", "bool1"));
	}

	/**
	 * test setting a string in a bool
	 *
	 * @return void
	 */
	public function testSetSetting_BoolOneString() {
		$ip = new MvBlog_IniParser($this->availableSettings);
		$ip->setSetting("section1", "bool1", "1");
		$this->assertTrue($ip->getSetting("section1", "bool1"));
	}

	/**
	 * test setting a bool to yes
	 *
	 * @return void
	 */
	public function testSetSetting_BoolYes() {
		$ip = new MvBlog_IniParser($this->availableSettings);
		$ip->setSetting("section1", "bool1", "yes");
		$this->assertTrue($ip->getSetting("section1", "bool1"));
	}

	/**
	 * test setting a bool to int 10
	 *
	 * @return void
	 */
	public function testSetSetting_BoolIntInt() {
		$ip = new MvBlog_IniParser($this->availableSettings);
		$ip->setSetting("section1", "int1", 10);
		$this->assertEquals($ip->getSetting("section1", "int1"), 10);
	}

	/**
	 * test setting a bool to string 10
	 *
	 * @return void
	 */
	public function testSetSetting_BoolIntString() {
		$ip = new MvBlog_IniParser($this->availableSettings);
		$ip->setSetting("section1", "int1", "10");
		$this->assertEquals($ip->getSetting("section1", "int1"), 10);
	}

	/**
	 * test adding a section to the config structure
	 *
	 * @return void
	 */
	public function AddSetting_Section() {
		$ip = new MvBlog_IniParser($this->availableSettings);
		$ip->addAvailableSetting("debug", "log", MvBlog_IniParser::TYPE_STRING, "mvblog.debug.log");
		$ip->setSetting("debug", "log", "test.log");
		$test->assertEquals($ip->getSetting("debug", "log"), "test.log");
	}
}
?>
