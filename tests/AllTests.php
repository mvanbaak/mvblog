<?php
/**
 * MvBlog -- An open source no-nosense blogtool
 *
 * Copyright (C) 2005-2008, Michiel van Baak
 * Michiel van Baak <mvanbaak@users.sourceforge.net>
 *
 * See http://dev.mvblog.org for more information on MvBlog.
 * That page also provides Bugtrackers, Filereleases etc.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 * PHP version 5
 *
 * @category  PHP
 * @package   MvBlog
 * @author    Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright 2005-2008 Michiel van Baak
 * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @version   SVN: $Revision: 809 $
 * @link      http://www.mvblog.org
 */
require_once "PHPUnit/Framework.php";
require_once "test_mvblog_IniParser.php";
require_once "test_mvblog_IniFileReader.php";
require_once "test_mvblog_log.php";

/**
 * Class to setup testsuite
 *
 * @category  PHP
 * @package   MvBlog
 * @author    Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright 2005-2008 Michiel van Baak
 * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @version   Release: %%VERSION%%
 * @link      http://www.mvblog.org
 */
class AllTests {
	/**
	 * Setup testsuite
	 *
	 * @return mixed PHPUnit_Framework_TestSuite instance
	 */
	public static function suite() {
		$suite = new PHPUnit_Framework_TestSuite("MvBlog Testsuite");
		$suite->addTestSuite("MvBlog_IniParser_test");
		$suite->addTestSuite("MvBlog_IniFileReader_test");
		$suite->addTestSuite("MvBlog_Log_test");
		return $suite;
	}
}
?>
