<?php
/**
 * MvBlog -- An open source no-nosense blogtool
 *
 * Copyright (C) 2005-2008, Michiel van Baak
 * Michiel van Baak <mvanbaak@users.sourceforge.net>
 *
 * See http://dev.mvblog.org for more information on MvBlog.
 * That page also provides Bugtrackers, Filereleases etc.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 * PHP version 5
 *
 * @category  PHP
 * @package   MvBlog
 * @author    Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright 2005-2008 Michiel van Baak
 * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @version   SVN: $Revision: 809 $
 * @link      http://www.mvblog.org
 */
require_once "PHPUnit/Framework.php";
require_once "../common/mvblog.php";
$mvblog = new mvblog();

/**
 * Class to test the MvBlog_IniFileReader class
 *
 * @category  PHP
 * @package   MvBlog
 * @author    Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright 2005-2008 Michiel van Baak
 * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @version   Release: %%VERSION%%
 * @link      http://www.mvblog.org
 */
class MvBlog_IniFileReader_test extends PHPUnit_Framework_TestCase {
	/**
	 * Setup test
	 *
	 * @return void
	 */
	public function __construct() {
		$this->availableSettings = array(
			"general" => array(
				"debug"    => array("type" => MvBlog_IniParser::TYPE_STRING,    "default" => "on"),
			),
			"database" => array(
				"database" => array("type" => MvBlog_IniFileReader::TYPE_STRING, "default" => "mvblog"),
				"hostname" => array("type" => MvBlog_IniFileReader::TYPE_STRING, "default" => "localhost"),
				"username" => array("type" => MvBlog_IniFileReader::TYPE_STRING, "default" => "mvblog"),
				"password" => array("type" => MvBlog_IniFileReader::TYPE_STRING, "default" => "mvblog"),
				"type"     => array("type" => MvBlog_IniFileReader::TYPE_STRING, "default" => "mysql"),
			),
		);
	}

	/**
	 * test getting default settings
	 *
	 * @return void
	 */
	public function testGetSetting_Default() {
		$ip = new MvBlog_IniFileReader($this->availableSettings, "mvblogIniFileParser.ini");
		$this->assertEquals($ip->getSetting("general", "debug"), "on");
	}

	/**
	 * test getting specific setting
	 *
	 * @return void
	 */
	public function testGetSetting_Config() {
		$ip = new MvBlog_IniFileReader($this->availableSettings, "mvblogIniFileParser.ini");
		$this->assertEquals($ip->getSetting("database", "type"), "sqlite");
	}
}
?>
