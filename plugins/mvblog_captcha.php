<?php
Class mvblog_captcha extends MvBlog_plugin implements MvBlog_pluginiface {
	/* variables */
	public $name    = "mvblog_captcha";
	public $author  = "Michiel van Baak";
	public $license = "GPL";
	public $website = "http://www.mvblog.org";
	public $description = "Basic captcha plugin.";

	private $_mvblog;

	/* methods */
	public function __construct(&$mvblog) {
		$this->addHook("captcha_output", "show_captcha");
		$this->addHook("captcha_check", "check_captcha");
		$this->_mvblog =& $mvblog;
	}

	public function activate() {
		/* get data, do whatever is needed on plugin activation */
	}

	public function deactivate() {
	}

	/* show_captcha {{{ */
	/**
	 * Show a captcha and hidden input and user input field
	 */
	public function show_captcha() {
		// generate random text string
		$txt = "";
		for ($i=0; $i<6; $i++) {
			$set = array(rand(65,90), rand(97,122));
			$txt .= chr($set[rand(0,1)]);
		}
		$_SESSION["txt"] = $txt;
		$output = "<img src=\"plugins/mvblog_captcha_files/image.php\">";
		//create tmp image file
		$output .= "<input type=\"hidden\" name=\"caphid\" value=\"".md5($txt)."\" />\n";
		$output .= "<input type=\"text\" name=\"captcha\" style=\"width: 60px;\" />&nbsp;".gettext("Type the text as shown on the left.")."<br /><br />\n";
		return $output;
	}
	/* }}} */
	public function check_captcha($data) {
		if ($data["caphid"] == md5($data["captcha"])) {
			return true;
		} else {
			return false;
		}
	}
}
?>
