<?php
Class acronyms extends MvBlog_plugin implements MvBlog_pluginiface {
	/* variables */
	public $name    = "Acronyms";
	public $author  = "Michiel van Baak";
	public $license = "GPL";
	public $website = "http://www.mvblog.org";
	public $description = "Acronym replacement module. Searches for know Acronyms and put <acronym> tag around them.";

	private $_mvblog;
	private $_acronyms;

	/* methods */
	/* __construct {{{ */
	public function __construct(&$mvblog) {
		$this->addHook("text_output", "acroreplace");
		$this->_mvblog =& $mvblog;
	}
	/* }}} */
	/* activate {{{ */
	public function activate() {
		/* get all acronyms from database */
		$res =& $this->_mvblog->db->query("SELECT id,acronym,description FROM acronyms ORDER BY UPPER(acronym)");
		if (PEAR::isError($res)) {
			die($res->getMessage());
		}
		while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
			$this->_acronyms[$row["id"]] = array($row["acronym"], $row["description"]);
		}
	}
	/* }}} */
	/* deactivate {{{ */
	public function deactivate() {
		unset($this->_acronyms);
	}
	/* }}} */
	/* acroreplace {{{ */
	public function acroreplace($text) {
		foreach ($this->_acronyms as $acro) {
			$acronym     = $acro[0];
			$description = $acro[1];
			//replace all the supplied acronyms
			$text = preg_replace("|(?!<[^<>]*?)(?<![?./&])\b$acronym\b(?!:)(?![^<>]*?>)|imsU","<acronym class=\"blog_acro\" title=\"$description\">$acronym</acronym>" , $text);
			//allow user to overwrite acronym replacing by putting the term between $ signs
			$text = preg_replace("|[$]<acronym class=\"blog_acro\" title=\"$description\">$acronym</acronym>[$]|imsU" , "$acronym" , $text);
		}
		return trim($text);
	}
	/* }}} */
	/* show_settings {{{ */
	public function show_settings() {

		?><a href="index.php?action=edit_plugin_setting&amp;plugin=acronyms&id=0"><?php echo gettext("create new"); ?></a><?php
		foreach ($this->_acronyms as $id => $acro) {
			?>
			<div class="log_post">
				<div class="log_post_head">
					<h1 class="log_post_h1"><a href="?action=edit_plugin_setting&plugin=acronyms&amp;id=<?php echo $id; ?>"><?php echo stripslashes($acro[0]); ?></a></h1>
				</div>
				<div class="log_post_body">
					<div class="log_post_normal">
						<?php echo stripslashes($acro[1]); ?>
					</div>
				</div>
				<div class="log_post_foot">
				</div>
			</div>
			<?php
		}
	}
	/* }}} */
	/* edit_setting {{{ */
	public function edit_setting($requestdata) {
		$id = $requestdata["id"];
		if ($id == 0) {
			$acro["id"]          = 0;
			$acro["acronym"]     = "acronym name";
			$acro["description"] = "acronym description";
		} else {
			$acro = array(
				"id"          => $id,
				"acronym"     => $this->_acronyms[$id][0],
				"description" => $this->_acronyms[$id][1]
			);
		}
		?>
		<form name="acronym" method="post" action="index.php">
		<input type="hidden" name="action" value="save_plugin_setting" />
		<input type="hidden" name="plugin" value="acronyms" />
		<input type="hidden" name="acro[action]" id="act" value="" />
		<input type="hidden" name="acro[id]" value="<?php echo $acro["id"]; ?>" />
		<div class="log_post">
			<div class="log_post_head">
				<h1 class="log_post_h1"><input type="text" name="acro[acronym]" value="<?php echo stripslashes($acro["acronym"]); ?>" /></h1>
			</div>
			<div class="log_post_body">
				<input type="text" name="acro[description]" value="<?php echo stripslashes($acro["description"]); ?>" style="width: 500px;" /><br />
				<input type="submit" value="<?php echo gettext("save"); ?>" />
				<?php if ($id) { ?>
					<input type="button" value="<?php echo gettext("delete"); ?>" onClick="document.getElementById('act').value='delete';document.forms.acronym.submit();" />
				<?php } ?>
			</div>
			<div class="log_postfoot">
			</div>
		</div>
		</form>
		<?php
	}
	/* }}} */
	/* save_setting {{{ */
	public function save_setting($requestdata) {
		$acro = $requestdata["acro"];
		if ($acro["action"] == "delete") {
			/* remove acro */
			unset($this->_acronyms[$acro["id"]]);
			$sql = sprintf("DELETE FROM acronyms WHERE id=%d", $acro["id"]);
			$this->_mvblog->db->exec($sql);
		} else {
			if ($acro["id"]) {
				$this->_acronyms[$acro["id"]] = array($acro["acronym"], $acro["description"]);
				/* store it in db */
				$sql = sprintf("UPDATE acronyms SET acronym='%s', description='%s' WHERE id=%d", $acro["acronym"], $acro["description"], $acro["id"]);
				$this->_mvblog->db->exec($sql);
			} else {
				$sql = sprintf("INSERT INTO acronyms (acronym, description) values ('%s', '%s')", $acro["acronym"], $acro["description"]);
				$res = $this->_mvblog->db->exec($sql);
				/* fetch the same record so we can get the id */
				$sql = sprintf("SELECT id FROM acronyms WHERE acronym='%s' AND description='%s'", $acro["acronym"], $acro["description"]);
				$res = $this->_mvblog->db->query($sql);
				$row = $res->fetchRow();
				$this->_acronyms[$row[0]] = array($acro["acronym"], $acronym["description"]);
			}
		}
		$this->show_settings();
	}
	/* }}} */
}
?>
