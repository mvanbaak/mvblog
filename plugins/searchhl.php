<?php
Class searchhl extends MvBlog_plugin implements MvBlog_pluginiface {
	/* variables */
	public $name    = "SearchHighlight";
	public $author  = "Michiel van Baak";
	public $license = "GPL";
	public $website = "http://www.mvblog.org";
	public $description = "Plugin to highlight the words typed in at searchengine.";

	private $_mvblog;

	/* methods */
	public function __construct(&$mvblog) {
		$this->addHook("text_output", "highlightsearch");
		$this->addHook("css_output" , "searchhlCSS");
		$this->_mvblog =& $mvblog;
	}

	public function activate() {
		/* get data, do whatever is needed on plugin activation */
	}

	public function deactivate() {
	}

	public function highlightsearch($text) {
		if (array_key_exists("HTTP_REFERER", $_SERVER) && $_SERVER["HTTP_REFERER"]) {
			//this is for google only right now.
			//TODO: implement some searchengine abstraction
			preg_match("/[\?|&]q=(.*)[$|&]/", $_SERVER["HTTP_REFERER"], $matches);
			if (array_key_exists(1, $matches) && $matches[1]) {
				//some stuff in the post has to be left alone. replace them with markers so we can put them back later
				preg_match_all("/<a[^>](.*?)>/si", $text, $txtmatches);
				foreach ($txtmatches[0] as $txtmatchk=>$txtmatchv) {
					$text = str_replace($txtmatchv, "##".$txtmatchk."##", $text);
				}
				$searchwords = explode("+", $matches[1]);
				foreach ($searchwords as $k=>$v) {
					$text = str_replace($v, "<span class=\"searchhl_".$k."\">".$v."</span>", $text);
				}
				//put the skipped stuff back in the buffer
				foreach ($txtmatches[0] as $txtmatchk=>$txtmatchv) {
					$text = str_replace("##".$txtmatchk."##", $txtmatchv, $text);
				}
			}
		}
		return $text;
	}

	public function searchhlCSS($data) {
		$output  = $data;
		$output .= "\t<style type=\"text/css\">\n";
		$output .= "\t\t.searchhl_0 { background-color: #fcc; }\n";
		$output .= "\t\t.searchhl_1 { background-color: #ff0; }\n";
		$output .= "\t\t.searchhl_2 { background-color: #f0f; }\n";
		$output .= "\t\t.searchhl_3 { background-color: #0ff; }\n";
		$output .= "\t\t.searchhl_4 { background-color:#FF9900; }\n";
		$output .= "\t\t.searchhl_5 { background-color:#99FF66; }\n";
		$output .= "\t\t.searchhl_6 { background-color:#99CCFF; }\n";
		$output .= "\t\t.searchhl_7 { background-color:#FF3333; }\n";
		$output .= "\t\t.searchhl_8 { background-color:#FF99FF; }\n";
		$output .= "\t\t.searchhl_9 { background-color:#FFFFCC; }\n";
		$output .= "\t</style>\n";
		return $output;
	}
}
?>
