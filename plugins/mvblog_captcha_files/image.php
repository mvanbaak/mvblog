<?php
/**
 * MvBlog -- An open source no-nosense blogtool
 *
 * Copyright (C) 2005-2008, Michiel van Baak
 * Michiel van Baak <mvanbaak@users.sourceforge.net>
 *
 * See http://www.mvblog.org for more information on MvBlog.
 * That page also provides Bugtrackers, Filereleases etc.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 * @package MvBlog
 * @author Michiel van Baak
 * @version %%VERSION%%
 * @copyright 2005-2008 Michiel van Baak
 */
if (!session_id()) {
	session_start();
}
$txt = $_SESSION["txt"];
header("Content-type: image/png");
$img = @imagecreatetruecolor(70, 20);
$bg_color  = imagecolorallocate($img, 255, 255, 255);
imagefilledrectangle($img, 0, 0, 70, 20, $bg_color);
$txt_color = imagecolorallocate($img, 0, 0, 0);
imagestring($img, 5, 8, 2, $txt, $txt_color);
imagepng($img);
imagedestroy($img);
?>
