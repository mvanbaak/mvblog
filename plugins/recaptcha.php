<?php
Class recaptcha extends MvBlog_plugin implements MvBlog_pluginiface {
	/* variables */
	public $name    = "recaptcha";
	public $author  = "Michiel van Baak";
	public $license = "GPL";
	public $website = "http://www.mvblog.org";
	public $description = "reCAPTCHA captcha plugin.";

	private $_mvblog;
	private $_settings = array(
		"recaptcha_pubkey"  => "",
		"recaptcha_privkey" => ""
	);

	/* methods */
	public function __construct(&$mvblog) {
		$this->addHook("captcha_output", "show_captcha");
		$this->addHook("captcha_check", "check_captcha");
		$this->_mvblog =& $mvblog;
	}

	public function activate() {
		/* populate some settings */
		$sql = "SELECT * FROM settings WHERE settingname LIKE 'recaptcha_%'";
		$res = $this->_mvblog->db->query($sql);
		while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
			$this->_settings[$row["settingname"]] = $row["settingvalue"];
		}

	}

	public function deactivate() {
		/* destroy settings */
		unset($this->_settings);
	}

	public function show_captcha() {
		require_once("recaptcha_files/recaptchalib.php");
		return recaptcha_get_html($this->_settings["recaptcha_pubkey"]);
	}

	public function check_captcha($data) {
		require_once("recaptcha_files/recaptchalib.php");
		$res = recaptcha_check_answer($this->_settings["recaptcha_privkey"],
			$_SERVER["REMOTE_ADDR"],
			$_POST["recaptcha_challenge_field"],
			$_POST["recaptcha_response_field"]);
		return $res->is_valid;
	}
	/* show_settings {{{ */
	public function show_settings() {
		$output  = "<form name=\"recaptcha_settings\" method=\"post\" action=\"index.php\">";
		$output .= "<input type=\"hidden\" name=\"action\" value=\"save_plugin_setting\" />";
		$output .= "<input type=\"hidden\" name=\"plugin\" value=\"recaptcha\" />";
		$output .= "<table style=\"width: 220px;\"><tr>";
		$output .= "<td>".gettext("Public reCAPTCHA key")."</td>";
		$output .= "<td><input type=\"text\" name=\"recaptcha_pubkey\" value=\"".$this->_settings["recaptcha_pubkey"]."\" /></td>";
		$output .= "</tr><tr>";
		$output .= "<td>".gettext("Private reCAPTCHA key")."</td>";
		$output .= "<td><input type=\"text\" name=\"recaptcha_privkey\" value=\"".$this->_settings["recaptcha_privkey"]."\" /></td>";
		$output .= "</tr><tr>";
		$output .= "<td colspan=\"2\"><input type=\"submit\" value=\"".gettext("Save")."\" /></td>";
		$output .= "</tr></table>";

		$output .= "</form>";
		echo $output;
	}
	/* }}} */
	/* save_setting {{{ */
	public function save_setting($requestdata) {
		/* first look if the setting is already there */
		$sql = "SELECT COUNT(*) FROM settings WHERE settingname = 'recaptcha_privkey'";
		$res = $this->_mvblog->db->query($sql);
		$row = $res->fetchRow();
		if ($row[0]) {
			/* yes, so update */
			$sql = sprintf("UPDATE settings SET settingvalue='%s' WHERE settingname='recaptcha_privkey'", $requestdata["recaptcha_privkey"]);
		} else {
			/* no, so insert */
			$sql = sprintf("INSERT INTO settings (settingname, settingvalue) VALUES ('recaptcha_privkey', '%s')", $requestdata["recaptcha_privkey"]);
		}
		$res = $this->_mvblog->db->exec($sql);
		$this->_settings["recaptcha_privkey"] = sprintf("%s", $requestdata["recaptcha_privkey"]);
		/* first look if the setting is already there */
		$sql = "SELECT COUNT(*) FROM settings WHERE settingname = 'recaptcha_pubkey'";
		$res = $this->_mvblog->db->query($sql);
		$row = $res->fetchRow();
		if ($row[0]) {
			/* yes, so update */
			$sql = sprintf("UPDATE settings SET settingvalue='%s' WHERE settingname='recaptcha_pubkey'", $requestdata["recaptcha_pubkey"]);
		} else {
			/* no, so insert */
			$sql = sprintf("INSERT INTO settings (settingname, settingvalue) VALUES ('recaptcha_pubkey', '%s')", $requestdata["recaptcha_pubkey"]);
		}
		$res = $this->_mvblog->db->exec($sql);
		$this->_settings["recaptcha_pubkey"] = sprintf("%s", $requestdata["recaptcha_pubkey"]);
		$this->show_settings();
	}
	/* }}} */
}
?>
