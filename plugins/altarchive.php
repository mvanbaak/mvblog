<?php
Class altarchive extends MvBlog_plugin implements MvBlog_pluginiface {
	/* variables */
	public $name    = "altarchive";
	public $author  = "Michiel van Baak";
	public $license = "GPL";
	public $website = "http://www.mvblog.org";
	public $description = "Replaces the normal month archive menu with a nice calendar.";

	private $_mvblog;

	private $_count    = array();
	private $_settings = array(
		"altarchive_noitems_bg"   => "#FFFFFF",
		"altarchive_items_bg"     => "#8bb7b5",
		"altarchive_noitems_text" => "#000000",
		"altarchive_items_text"   => "#FFFFFF"
	);

	/* methods */
	/* __construct {{{ */
	public function __construct(&$mvblog) {
		// FB: 20070304: Renamed 'altarchive' to 'genaltarchive' to avoid name collisions with constructor.
		$this->addHook("menu_archive_output", "genaltarchive");
		$this->addHook("css_output", "altarchiveCSS");
		$this->_mvblog =& $mvblog;
	}
	/* }}} */
	/* activate {{{ */
	public function activate() {
		/* get the months where we have articles */
		$sql = "SELECT date FROM articles WHERE active=1 AND public=1 ORDER BY date DESC";
		$res = $this->_mvblog->db->query($sql);
		while ($row = $res->fetchRow()) {
			$month = date("m", $row[0]);
			$year  = date("Y", $row[0]);
			$key   = $month.$year;
			if (array_key_exists($key, $this->_count)) {
				$this->_count[$key]++;
			} else {
				$this->_count[$key] = 1;
			}
		}
		/* populate some settings */
		$sql = "SELECT * FROM settings WHERE settingname LIKE 'altarchive_%'";
		$res = $this->_mvblog->db->query($sql);
		while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
			$this->_settings[$row["settingname"]] = $row["settingvalue"];
		}
	}
	/* }}} */
	/* deactivate {{{ */
	public function deactivate() {
		/* clear local variables */
		unset($this->_count);
	}
	/* }}} */
	/* genaltarchive {{{ */
	// FB: 20070304: Renamed 'altarchive' to 'genaltarchive' to avoid name collisions with constructor.
	public function genaltarchive($defaultmenu) {
		/* create url schema */
		if ($this->_mvblog->settings["cleanurl"])
			$url = "archive/";
		else
			$url = "index.php?action=archive&m=";
		/* years to show */
		if (array_key_exists("altarchive_showyears", $this->_settings))
			$years = $this->_settings["altarchive_showyears"];
		else
			$years  = 3;
		$output = "";
		for ($i=date("Y"); $i>date("Y")-$years; $i--) {
			$output .= "<b>".$i."</b><br />\n";
			$output .= "<table><tr>\n";
			$newline = 0;
			for ($m = 1; $m <= 12; $m++) {
				$newline++;
				if ($newline > 3) {
					$output .= "</tr><tr>\n";
					$newline = 1;
				}
				if ($m < 10) $m = "0".$m;
				$output .= "\t<td ";
				if (array_key_exists($m.$i, $this->_count)) {
					$output .= "class=\"altarchive_month_items\">";
					$output .= "<a href=\"$url".$m.$i."\" title=\"".$this->_count[$m.$i]." ".gettext("articles in this month.")."\">".strftime("%B", mktime(0,0,0,$m,1,$i))."</td>\n";
				} else {
					$output .= "class=\"altarchive_month_noitems\">";
					$output .= strftime("%B", mktime(0,0,0,$m,1,$i))."</td>\n";
				}
			}
			$output .= "</tr></table>\n";
		}
		return $output;
	}
	/* }}} */
	/* altarchiveCSS {{{ */
	public function altarchiveCSS($data) {
		if (array_key_exists("altarchive_showdefaultcss", $this->_settings) && $this->_settings["altarchive_showdefaultcss"]) {
			$output  = $data."\n\t<style type=\"text/css\">\n";
			$output .= "\t\t.altarchive_month_noitems {\n";
			$output .= "\t\t\ttext-align: center;\n";
			$output .= "\t\t\tbackground-color: ".$this->_settings["altarchive_noitems_bg"].";\n";
			$output .= "\t\t\tcolor: ".$this->_settings["altarchive_noitems_text"].";\n";
			$output .= "\t\t\tborder: 1px solid #b0b0b0;\n";
			$output .= "\t\t}\n";
			$output .= "\t\t.altarchive_month_items {\n";
			$output .= "\t\t\ttext-align: center;\n";
			$output .= "\t\t\tbackground-color: ".$this->_settings["altarchive_items_bg"].";\n";
			$output .= "\t\t\tborder: 1px solid #b0b0b0;\n";
			$output .= "\t\t}\n";
			$output .= "\t\t.altarchive_month_items a {\n";
			$output .= "\t\t\tcolor: ".$this->_settings["altarchive_items_text"].";\n";
			$output .= "\t\t}\n";
			$output .= "\t</style>\n";
		} else {
			$output = $data;
		}
		return $output;
	}
	/* }}} */
	/* show_settings {{{ */
	public function show_settings() {
		$output  = "<form name=\"altarchive_settings\" method=\"post\" action=\"index.php\">";
		$output .= "<input type=\"hidden\" name=\"action\" value=\"save_plugin_setting\" />";
		$output .= "<input type=\"hidden\" name=\"plugin\" value=\"altarchive\" />";
		$output .= "<table style=\"width: 220px;\"><tr>";
		$output .= "<td>".gettext("use default stylesheet?")."</td>";
		$output .= "<td><select name=\"altarchive_showdefaultcss\">";
		$output .= "<option value=\"1\"";
		if (array_key_exists("altarchive_showdefaultcss", $this->_settings) && $this->_settings["altarchive_showdefaultcss"] == 1)
			$output .= " selected=\"selected\"";
		$output .= ">".gettext("yes")."</option>";
		$output .= "<option value=\"0\"";
		if (!array_key_exists("altarchive_showdefaultcss", $this->_settings) || array_key_exists("altarchive_showdefaultcss", $this->_settings) && $this->_settings["altarchive_showdefaultcss"] == 0)
			$output .= " selected=\"selected\"";
		$output .= ">".gettext("no")."</option>";
		$output .= "</select></td>\n";
		$output .= "</tr><tr>";
		$output .= "<td>".gettext("years to show")."</td>";
		$output .= "<td><select name=\"altarchive_showyears\">";
		for ($i=1;$i<10;$i++) {
			$output .= "<option value=\"$i\"";
			if (array_key_exists("altarchive_showyears", $this->_settings) && $this->_settings["altarchive_showyears"] == $i)
				$output .= " selected=\"selected\"";
			$output .= ">$i</option>";
		}
		$output .= "</select></td>";
		$output .= "</tr><tr>";
		$output .= "<td colspan=\"2\"><input type=\"submit\" value=\"".gettext("Save")."\" /></td>";
		$output .= "</tr></table>";

		$output .= "</form>";
		echo $output;
	}
	/* }}} */
	/* save_setting {{{ */
	public function save_setting($requestdata) {
		/* first look if the setting is already there */
		$sql = "SELECT COUNT(*) FROM settings WHERE settingname = 'altarchive_showdefaultcss'";
		$res = $this->_mvblog->db->query($sql);
		$row = $res->fetchRow();
		if ($row[0]) {
			/* yes, so update */
			$sql = sprintf("UPDATE settings SET settingvalue='%d' WHERE settingname='altarchive_showdefaultcss'", $requestdata["altarchive_showdefaultcss"]);
		} else {
			/* no, so insert */
			$sql = sprintf("INSERT INTO settings (settingname, settingvalue) VALUES ('altarchive_showdefaultcss', '%d')", $requestdata["altarchive_showdefaultcss"]);
		}
		$res = $this->_mvblog->db->exec($sql);
		$this->_settings["altarchive_showdefaultcss"] = sprintf("%d", $requestdata["altarchive_showdefaultcss"]);
		/* first look if the setting is already there */
		$sql = "SELECT COUNT(*) FROM settings WHERE settingname = 'altarchive_showyears'";
		$res = $this->_mvblog->db->query($sql);
		$row = $res->fetchRow();
		if ($row[0]) {
			/* yes, so update */
			$sql = sprintf("UPDATE settings SET settingvalue='%d' WHERE settingname='altarchive_showyears'", $requestdata["altarchive_showyears"]);
		} else {
			/* no, so insert */
			$sql = sprintf("INSERT INTO settings (settingname, settingvalue) VALUES ('altarchive_showyears', '%d')", $requestdata["altarchive_showyears"]);
		}
		$res = $this->_mvblog->db->exec($sql);
		$this->_settings["altarchive_showyears"] = sprintf("%d", $requestdata["altarchive_showyears"]);
		$this->show_settings();
	}
	/* }}} */
}
?>
