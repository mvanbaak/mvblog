<?php
/* include the geshi stuff */
require_once("syntaxhl_files/geshi.php");

Class syntaxhl extends MvBlog_plugin implements MvBlog_pluginiface {
	/* variables */
	public $name    = "Syntax color highlighter";
	public $author  = "Michiel van Baak";
	public $license = "GPL";
	public $website = "http://www.mvblog.org";
	public $description = "Provides syntax color highlighting inside posts.";

	private $_mvblog;

	/* methods */
	public function __construct(&$mvblog) {
		$this->addHook("text_output", "syntaxhighlight");
		$this->_mvblog =& $mvblog;
	}

	public function activate() {
		/* get data, do whatever is needed on plugin activation */
	}

	public function deactivate() {
	}

	public function syntaxhighlight($text) {
		/* make copy of input because we are going to modify it */
		$pluginoutput = $text;

		/* find all blocks of [code:<language> .... :code] */
		preg_match_all("/\[code:(.*):code\]/Us", $text, $matches);
		foreach ($matches[0] as $codeblock) {
			$data = "";
			/* get the language for the code block */
			preg_match("/\[code:(\w{1,})/", $codeblock, $langmatch);
			$language = $langmatch[1];
			/* remove the special code to identify code blocks */
			$data = str_replace("[code:$language", "", $codeblock);
			$data = str_replace(":code]", "", $data);
			$data = str_replace("<br />", "", html_entity_decode($data));
			/* use geshi to syntax highlight the code */
			$geshi = new Geshi(trim($data), strtolower($language));
			$geshi->enable_classes();
			/* geshi comes with some style tags, so load the inline stylesheets */
			$data =  "<style type=\"text/css\">\n";
			$data .= $geshi->get_stylesheet();
			$data .= "</style>\n";
			/* enable line numbers */
			$geshi->enable_line_numbers(GESHI_NORMAL_LINE_NUMBERS);
			/* the actual highlighting is here */
			$data .= $geshi->parse_code();
			/* replace the original code with the highlighted code */
			$pluginoutput = str_replace($codeblock, $data, $pluginoutput);
		}
		/* return the modified code with highlighting */
		return $pluginoutput;
	}
}
?>
