<?php
Class altarchive_onemonth extends MvBlog_plugin implements MvBlog_pluginiface {
	/* variables */
	public $name    = "altarchive_onemonth";
	public $author  = "Michiel van Baak";
	public $license = "GPL";
	public $website = "http://www.mvblog.org";
	public $description = "Replaces the normal month archive menu with a nice calendar.";

	private $_mvblog;

	private $_count    = array();
	private $_settings = array(
		"altarchive_onemonth_noitems_bg"   => "#FFFFFF",
		"altarchive_onemonth_items_bg"     => "#8bb7b5",
		"altarchive_onemonth_noitems_text" => "#000000",
		"altarchive_onemonth_items_text"   => "#FFFFFF"
	);

	/* methods */
	/* __construct {{{ */
	public function __construct(&$mvblog) {
		$this->addHook("menu_archive_output", "genaltarchive_onemonth");
		$this->addHook("css_output", "altarchive_onemonthCSS");
		$this->_mvblog =& $mvblog;
	}
	/* }}} */
	/* activate {{{ */
	public function activate() {
		/* get the months where we have articles */
		$sql = "SELECT date FROM articles WHERE active=1 AND public=1 ORDER BY date DESC";
		$res = $this->_mvblog->db->query($sql);
		while ($row = $res->fetchRow()) {
			$day   = date("d", $row[0]);
			$month = date("m", $row[0]);
			$year  = date("Y", $row[0]);
			$key   = $day.$month.$year;
			if (array_key_exists($key, $this->_count)) {
				$this->_count[$key]++;
			} else {
				$this->_count[$key] = 1;
			}
		}
		/* populate some settings */
		$sql = "SELECT * FROM settings WHERE settingname LIKE 'altarchive_onemonth%'";
		$res = $this->_mvblog->db->query($sql);
		while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
			$this->_settings[$row["settingname"]] = $row["settingvalue"];
		}
	}
	/* }}} */
	/* deactivate {{{ */
	public function deactivate() {
		/* clear local variables */
		unset($this->_count);
	}
	/* }}} */
	/* genaltarchive_onemonth {{{ */
	public function genaltarchive_onemonth($defaultmenu) {
		/* find out what the oldest post date is */
		$q = "SELECT date FROM articles WHERE active = 1 AND public = 1 ORDER BY date LIMIT 1";
		$res = $this->_mvblog->db->query($q);
		$row = $res->fetchRow(MDB2_FETCHMODE_ASSOC);
		$oldest = $row["date"];

		$output   = "";
		$url_next = "";
		$url_prev = "";
		/* create url schema */
		if ($this->_mvblog->settings["cleanurl"])
			$url = "archive/";
		else
			$url = "index.php?action=archive&m=";
		/* read url params so we know what to show */
		if (array_key_exists("action", $_REQUEST) && $_REQUEST["action"] == "archive") {
			if (array_key_exists("m", $_REQUEST)) {
				if (strlen($_REQUEST["m"]) == 6) {
					/* month */
					$_month = substr($_REQUEST["m"], 0, 2);
					$_year  = substr($_REQUEST["m"], 2, 4);
				} elseif (strlen($_REQUEST["m"]) == 8) {
					/* day */
					$_month = substr($_REQUEST["m"], 2, 2);
					$_year  = substr($_REQUEST["m"], 4, 4);
				}
			} else {
				$_month = date("m");
				$_year  = date("Y");
			}
		} else {
			$_month = date("m");
			$_year  = date("Y");
		}
		$url_next = $url.date("m", mktime(0, 0, 0, $_month+1, 1, $_year)).date("Y", mktime(0, 0, 0, $_month+1, 1, $_year));
		$url_prev = $url.date("m", mktime(0, 0, 0, $_month-1, 1, $_year)).date("Y", mktime(0, 0, 0, $_month-1, 1, $_year));
		$output .= "<b>".strftime("%B %Y", mktime(0, 0, 0, $_month, 1, $_year))."</b><br />\n";
		$output .= "<table><tr>\n";
		$output .= "<td class=\"altarchive_onemonth_month_noitems\">s</td>\n";
		$output .= "<td class=\"altarchive_onemonth_month_noitems\">m</td>\n";
		$output .= "<td class=\"altarchive_onemonth_month_noitems\">t</td>\n";
		$output .= "<td class=\"altarchive_onemonth_month_noitems\">w</td>\n";
		$output .= "<td class=\"altarchive_onemonth_month_noitems\">t</td>\n";
		$output .= "<td class=\"altarchive_onemonth_month_noitems\">f</td>\n";
		$output .= "<td class=\"altarchive_onemonth_month_noitems\">s</td>\n";
		$output .= "</tr><tr>";
		/* calendar start on sunday, calculate what day this month started */
		$skipdays = date("w", mktime(0, 0, 0, $_month, 1, $_year));
		$dow = $skipdays;
		/* skip the days before this month started */
		for ($i=0;$i!=$skipdays;$i++)
			$output .= "<td class=\"altarchive_onemonth_month_noitems\">&nbsp;</td>\n";
		for ($i=0; $i!=date("t",mktime(0,0,0,$_month,1,$_year)); $i++) {
			/* look if we have a post today */
			$today = date("dmY", mktime(0, 0, 0, $_month, $i+1, $_year));
			if (array_key_exists($today, $this->_count) && $this->_count[$today] > 0)
				$output .= "<td class=\"altarchive_onemonth_month_items\"><a href=\"".$url.$today."\">".($i+1)."</a></td>\n";
			else
				$output .= "<td class=\"altarchive_onemonth_month_noitems\">".($i+1)."</td>\n";
			$dow++;
			/* start new row every week */
			if ($dow == 7) {
				$output .= "</tr><tr>\n";
				$dow = 0;
			}
		}
		/* if last day of month is not saturday, complete the table */
		if ($dow) {
			for ($i=0;$i!=7-$dow;$i++)
				$output .= "<td class=\"altarchive_onemonth_month_noitems\">&nbsp;</td>\n";
		}
		$output .= "</tr><tr>\n";
		if ($oldest < mktime(0, 0, 0, $_month, 1, $_year))
			$output .= "<td class=\"altarchive_onemonth_month_noitems\" colspan=\"3\"><a href=\"$url_prev\"><< ".strftime("%b", mktime(0, 0, 0, $_month-1, 1, $_year))."</a></td>\n";
		else
			$output .= "<td class=\"altarchive_onemonth_month_noitems\" colspan=\"3\">&nbsp;</td>\n";
		$output .= "<td class=\"altarchive_onemonth_month_noitems\">&nbsp;</td>\n";
		if (date("m") == $_month)
			$output .= "<td class=\"altarchive_onemonth_month_noitems\" colspan=\"3\">&nbsp;</td>\n";
		else
			$output .= "<td class=\"altarchive_onemonth_month_noitems\" colspan=\"3\"><a href=\"$url_next\">".strftime("%b", mktime(0, 0, 0, $_month+1, 1, $_year))." >></a></td>\n";
		$output .= "</tr></table>\n";
		return $output;
	}
	/* }}} */
	/* altarchiveCSS {{{ */
	public function altarchive_onemonthCSS($data) {
		if (array_key_exists("altarchive_onemonth_showdefaultcss", $this->_settings) && $this->_settings["altarchive_onemonth_showdefaultcss"]) {
			$output  = $data."\n\t<style type=\"text/css\">\n";
			$output .= "\t\t.altarchive_onemonth_month_noitems {\n";
			$output .= "\t\t\ttext-align: center;\n";
			$output .= "\t\t\tbackground-color: ".$this->_settings["altarchive_onemonth_noitems_bg"].";\n";
			$output .= "\t\t\tcolor: ".$this->_settings["altarchive_onemonth_noitems_text"].";\n";
			$output .= "\t\t\tborder: 1px solid #b0b0b0;\n";
			$output .= "\t\t}\n";
			$output .= "\t\t.altarchive_onemonth_month_items {\n";
			$output .= "\t\t\ttext-align: center;\n";
			$output .= "\t\t\tbackground-color: ".$this->_settings["altarchive_onemonth_items_bg"].";\n";
			$output .= "\t\t\tborder: 1px solid #b0b0b0;\n";
			$output .= "\t\t}\n";
			$output .= "\t\t.altarchive_onemonth_month_items a {\n";
			$output .= "\t\t\tcolor: ".$this->_settings["altarchive_onemonth_items_text"].";\n";
			$output .= "\t\t}\n";
			$output .= "\t</style>\n";
		} else {
			$output = $data;
		}
		return $output;
	}
	/* }}} */
	/* show_settings {{{ */
	public function show_settings() {
		$output  = "<form name=\"acronym\" method=\"post\" action=\"index.php\">";
		$output .= "<input type=\"hidden\" name=\"action\" value=\"save_plugin_setting\" />";
		$output .= "<input type=\"hidden\" name=\"plugin\" value=\"altarchive_onemonth\" />";
		$output .= "<table style=\"width: 220px;\"><tr>";
		$output .= "<td>".gettext("use default stylesheet?")."</td>";
		$output .= "<td><select name=\"altarchive_onemonth_showdefaultcss\">";
		$output .= "<option value=\"1\"";
		if (array_key_exists("altarchive_onemonth_showdefaultcss", $this->_settings) && $this->_settings["altarchive_onemonth_showdefaultcss"] == 1)
			$output .= " selected=\"selected\"";
		$output .= ">".gettext("yes")."</option>";
		$output .= "<option value=\"0\"";
		if (!array_key_exists("altarchive_onemonth_showdefaultcss", $this->_settings) || 
			array_key_exists("altarchive_onemonth_showdefaultcss", $this->_settings) && $this->_settings["altarchive_onemonth_showdefaultcss"] == 0)
			$output .= " selected=\"selected\"";
		$output .= ">".gettext("no")."</option>";
		$output .= "</select></td>\n";
		$output .= "</tr><tr>";
		$output .= "<td colspan=\"2\"><input type=\"submit\" value=\"".gettext("Save")."\" /></td>";
		$output .= "</tr></table>";

		$output .= "</form>";
		echo $output;
	}
	/* }}} */
	/* save_setting {{{ */
	public function save_setting($requestdata) {
		/* first look if the setting is already there */
		$sql = "SELECT COUNT(*) FROM settings WHERE settingname = 'altarchive_onemonth_showdefaultcss'";
		$res = $this->_mvblog->db->query($sql);
		$row = $res->fetchRow();
		if ($row[0]) {
			/* yes, so update */
			$sql = sprintf("UPDATE settings SET settingvalue='%d' WHERE settingname='altarchive_onemonth_showdefaultcss'", $requestdata["altarchive_onemonth_showdefaultcss"]);
		} else {
			/* no, so insert */
			$sql = sprintf("INSERT INTO settings (settingname, settingvalue) VALUES ('altarchive_onemonth_showdefaultcss', '%d')", $requestdata["altarchive_onemonth_showdefaultcss"]);
		}
		$res = $this->_mvblog->db->exec($sql);
		$this->_settings["altarchive_onemonth_showdefaultcss"] = sprintf("%d", $requestdata["altarchive_onemonth_showdefaultcss"]);
		$this->show_settings();
	}
	/* }}} */
}
?>
