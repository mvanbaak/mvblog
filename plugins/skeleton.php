<?php
Class skeleton extends MvBlog_plugin implements MvBlog_pluginiface {
	/* variables */
	public $name    = "skeleton";
	public $author  = "Michiel van Baak";
	public $license = "GPL";
	public $website = "http://www.mvblog.org";
	public $description = "Example plugin. Replaces all occurences of the word \"mafkees\" with \"The MvBlog Projectleader\".";

	private $_mvblog;

	/* methods */
	public function __construct(&$mvblog) {
        // FB: 20070304: Renamed from 'test' to 'plugintest' to avoid name collision with constructor.
		$this->addHook("text_output", "plugintest");
		$this->_mvblog =& $mvblog;
	}

	public function activate() {
		/* get data, do whatever is needed on plugin activation */
	}

	public function deactivate() {
	}

    // FB: 20070304: Renamed from 'test' to 'plugintest' to avoid name collision with constructor.
	public function plugintest($text) {
		$output = str_ireplace("michiel", "The MvBlog Projectleader", $text);
		$output = str_ireplace("mafkees", "The MvBlog Projectleader", $output);
		return $output;
	}
}
?>
