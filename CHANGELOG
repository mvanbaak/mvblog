MvBlog-trunk:
-----------
* Added input formats in the backend.

MvBlog 3.0.1:
-------------
* Fixed links to post readmore and comments
* Dont show settings link when logged in as author

MvBlog 3.0:
-----------
* Added category icons
* new version of tinymce
* Added user settings interface
* Users can now decide wether to show their email address on comments
* Added import from WordPress blogsoftware
* inactive users can now be deleted

MvBlog 2.3:
-----------
* Added story linking: use [#<postnumber>] to include link to other post
* Added dossiers (implements #115)
* removed the default limit of 4000 characters on posts in main list. You _MUST_ use the ##BREAKPOINT## magic now to get a 'read more' link
* plugin system now ignores directories in plugins/ to prevent errors
* added syntaxhl plugin to do syntax highlighting of post based on [code:<language> ..... :code] syntax. See COLOR_HIGHLIGHT.txt (implements #113)

MvBlog 2.2.1:
-----------
* Last release borked a lot of images. This one is correct

MvBlog 2.2:
-----------
* Make sure users cannot download mvblog.ini
* Support multiple categories per post
* Added backup routines to update progress
* Added WP dashboard like index page for admin interface.
* Added swedish language

MvBlog 2.1:
-----------
* Fixed rss item description
* Added link to archive for older articles
* removed common/hosts.php in favour of conf/mvblog.ini
* we no longer support one codebase with multiple blogs
* switched from xinha to tiny_mce
* Rewrote plugin stuff and plugins to be completely object oriented
* Created api docs
* added some more correct info to the rss.php
* added rdf.php to output nice rdf feed.
* Added custom 404 page (#62)

mvblog 2.0:
-----------
* Rewrote whole thing as classes.
* More security updates
* 2 new default themes, one dropped

mvblog 1.8:
-----------
* Blog authors that are logged in to the admin interface will be logged in as comment author automagically
* Add mysql support
* Fix index.php and css to get rid of the hardcoded table properties

mvblog 1.7:
-----------
*added plugin system (ticket #52)
*added user system.
*added options to disallow anonymous comments (ticket #60)

mvblog 1.6:
-----------
*added styleswitcher function (ticket #17)
*fixed stylesheet errors and added some meta tags
*logged in author's info is used in comments (ticket #42)
*added admin interface for comments (ticket #41)
*Added RSS for comments, both all and per article (ticket #4)

*****IMPORTANT*****
todsah identified those, and was kind enough to notify me as soon as he saw them.
*Fixed several XSS (crossite scripting attacks) issues (ticket #55)
*Fixed several sql-injection issues (ticket #54)


mvblog 1.5:
-----------
*added new styles
*fixed date options
*fixed issues in ImageManager
*added delete option for categories
*added icons for comment website and mail link
*added link to admin interface when author is logged in and visiting frontend
*added links admin and function to display user configurable links to other sites in a menu
*lots of documantation is build
*all bugs are fixed

mvblog 1.2:
-----------

*added tagging button in the html editor
*moved SCM from sf.net to svn.three-dimensional.net/mvblog
*moved to trac on http://dev.mvblog.org
*added <div> around next/prev buttons on homepage (ticket #6)
*catch enter key when the search box is there (ticket #7)
*posts will start showing on the day the date is set.
  this means setting the post date to the future will not show it till that date comes.
  (ticket #3)
*added <div> around comment footer so it looks better.
*fixed creating of folders in the image system in the editor
*fixed the RSS when characters like & are in the title
*Implemented settings system

mvblog 1.0:
-----------

*added CSS file with list/tree of element id's and classes. (leonieke)
*added INSTALL instructions.
*added AJAX based livesearch
*changed default index.php and style and images to make the blog wider. 750 now.
*added miniposts AKA asides (RFE #1306100)
*removed category in admins postlist for asides (RFE #1380875)
*the whole admins postlist now is limited to 15 per page (RFE #1380988)
*fixed the image upload stuff in the html editor
*public site: now also the various archives are paged. (RFE #1370404)
*removed links to asides posts in rss. (issue #1380604)
*updated FCKEditor to 2.1.1 This fixes an issue where you had to type something in
	the editor before being able to delete something from it.


mvblog 0.6:
-----------

*added little buttons like the w3c compat stuff (leonieke)
*fixed some issues in the logo (sofie)
*posts without title can now be edited (issue #1371151)
*added breakpoint marker in post admin (RFE #1364952)
*authors can be en/disabled (RFE #1367667)
*added text/html counter in the editor (RFE #1370369)
*XHTML 1.0 compatibility updates
*added paging links on main view (RFE #1370404)
*added style to "Read More" link (RFE #1364952)
*added "save my info" in comment form (RFE #1369460)
*added email- and webaddress fields to comments (RFE #1308749)

mvblog 0.5:
-----------

*added Trackback system as specified in MT docs
*added ability to change a posts date
*added 'modified' info to posts
*new logo design by Sofie van Tendeloo
*added GPL headers and copyright info to all php files
*give focus to some formfields when entering forms
*when changing a post from non-active to active, don't update 'modified' info
*removed debug info on several places
*added Copyright info to admin interface and default public site
*added 2 functions to show archive-structures for categories and dates
*fixed layout on several places
*added logout ability to admin interface
*better comments in source
*public post overview now limits post length
