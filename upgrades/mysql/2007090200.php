<?php
$sql[] = "CREATE TABLE dossiers (
	`id` int(11) NOT NULL auto_increment,
	`name` varchar(255) NOT NULL,
	`desc` varchar(255),
	`public` smallint(3) default '1',
	`active` smallint(3) default '1',
	PRIMARY KEY  (`id`)
);";
$sql[] = "alter table articles add column dossier_id int(11);"
?>
