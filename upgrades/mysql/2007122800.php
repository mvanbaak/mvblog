<?php
$sql = array(
	"ALTER TABLE articles ADD COLUMN postformat varchar(10) DEFAULT 'HTML';",
	"UPDATE articles SET postformat = 'HTML' WHERE postformat IS NULL OR postformat = '';"
);
