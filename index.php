<?php
/**
 * MvBlog -- An open source no-nosense blogtool
 *
 * Copyright (C) 2005-2008, Michiel van Baak
 * Michiel van Baak <mvanbaak@users.sourceforge.net>
 *
 * See http://dev.mvblog.org for more information on MvBlog.
 * That page also provides Bugtrackers, Filereleases etc.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 * PHP version 5
 *
 * @category  PHP
 * @package   MvBlog
 * @author    Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright 2005-2008 Michiel van Baak
 * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @version   SVN: $Revision: 809 $
 * @link      http://www.mvblog.org
 */

require "common/mvblog.php";

$mvblog = new MvBlog();
$mvblog->html_header();
if (file_exists("index-alt.php")) {
	include "index-alt.php";
} else {
	?>
	<div id="if_container">
		<div id="if_title"></div>
		<div id="if_bar1"></div>
		<div id="if_page_header">
			<h1 class="page_title"><a href="index.php" class="page_header"><?php echo $mvblog->blog_get_title(); ?></a></h1>
		</div>
		<div id="if_page">
			<table><tr>
				<td valign="top" width="90%" id="if_content_container">
					<p class="first"><?php echo $mvblog->blog_get_description(); ?></p>
					<?php $mvblog->blog_content(0, 0); ?>
				</td>

				<td valign="top" id="if_sidebar_container">
					<ul>
						<li>
							<h2>Users</h2>
							<?php $mvblog->blog_show_login(); ?>
						</li>
						<li>
							<h2>Site</h2>
							<?php $mvblog->blog_default_links(); ?>
						</li>
						<li>
							<h2>Style</h2>
							<?php $mvblog->blog_show_styleswitcher(); ?>
						</li>
						<li>
							<h2>Monthly Archive</h2>
							<?php $mvblog->blog_archive_links(); ?>
						</li>
						<li>
							<h2>Category Archive</h2>
							<?php $mvblog->blog_cats_links(); ?>
						</li>
						<li>
							<h2>Dossiers</h2>
							<?php $mvblog->blog_show_dossierlinks(); ?>
						</li>
						<li>
							<h2>Search</h2>
							<?php $mvblog->blog_show_search(); ?>
						</li>
						<li>
							<h2>Links etc.</h2>
							<?php $mvblog->blog_show_menulinks(); ?>
						</li>
					</ul>
				</td>

			</tr></table>
	<?php 
	$mvblog->html_footer();
}
?>
</body>
</html>
