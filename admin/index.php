<?php
/**
 * MvBlog -- An open source no-nosense blogtool
 *
 * Copyright (C) 2005-2008, Michiel van Baak
 * Michiel van Baak <mvanbaak@users.sourceforge.net>
 *
 * See http://dev.mvblog.org for more information on MvBlog.
 * That page also provides Bugtrackers, Filereleases etc.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 * PHP version 5
 *
 * @category  PHP
 * @package   MvBlog
 * @author    Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright 2005-2008 Michiel van Baak
 * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @version   SVN: $Revision: 809 $
 * @link      http://www.mvblog.org
 */

/**
 * Start the autoloader, so we never have to include anything
 */
require_once "../common/mvblog_autoloader.php";
$MvBlog_AutoLoader = new MvBlog_AutoLoader();

$pathInfo = pathinfo(__FILE__);
$MvBlog_AutoLoader->registerPath($pathInfo["dirname"], "%s.php", MvBlog_AutoLoader::OPT_LOWERCASE);
$MvBlog_AutoLoader->registerPath("../common/", "%s.php", MvBlog_AutoLoader::OPT_LOWERCASE);

/**
 * Register the AutoLoader object as the autoloader.
 *
 * @param string $className The classname to load
 *
 * @return void
 */
function __autoload($className) {
    global $MvBlog_AutoLoader; 
    $MvBlog_AutoLoader->autoload($className);
}

/* Start heavy error reporting if we're on a dev site */
MvBlog_debug::start_development(false);

/* Read the configuration file */
$configfile = dirname(dirname(__FILE__) . "../") . "/conf/mvblog.ini";
$availSettings = array(
    "general" => array(
        "debug"    => array("type" => MvBlog_IniFileReader::TYPE_BOOL, "default" => "no"),
    ),
    "database" => array(
        "database" => array("type" => MvBlog_IniFileReader::TYPE_STRING, "default" => "mvblog"),
        "hostname" => array("type" => MvBlog_IniFileReader::TYPE_STRING, "default" => "localhost"),
        "username" => array("type" => MvBlog_IniFileReader::TYPE_STRING, "default" => "mvblog"),
        "password" => array("type" => MvBlog_IniFileReader::TYPE_STRING, "default" => "mvblog"),
        "type"     => array("type" => MvBlog_IniFileReader::TYPE_STRING, "default" => "mysql"),
    ),
);

$config = new MvBlog_IniFileReader($availSettings, $configfile);

if (!array_key_exists("options", $_REQUEST)) {
    $_REQUEST["options"] = "";
}
$admin = new MvBlog_admin();
$admin->html_header("Admin interface", "../");
    ?>
	<div id="if_container">
		<div id="if_title"></div>
		<div id="if_bar1"></div>
		<div id="if_page_header">
			<a href="index.php"><h1 class="page_title">admin</h1></a>
		</div>
		<?php $admin->show_admin_menu(); ?>
		<div id="if_page">
			<?php
            if (!array_key_exists("action", $_REQUEST)) {
                $_REQUEST["action"] = "";
            }
            switch ($_REQUEST["action"]) {
            case "check_login" : 
                $admin->check_login((array_key_exists("login", $_POST)) ? $_POST["login"] : "");
                break;
            case "logout" : 
                $admin->logout();
                break;
            case "show_cats" :
                $admin->show_cats();
                break;
            case "edit_cat" :
                $admin->edit_cat((array_key_exists("id", $_REQUEST)) ? $_REQUEST["id"] : 0);
                break;
            case "save_cat" :
                $admin->save_cat($_POST["cat"]);
                break;
            case "delete_cat" :
                $admin->delete_cat($_REQUEST["cat"]["id"]);
                break;
            case "show_dossiers" :
                $admin->show_dossiers();
                break;
            case "edit_dossier" :
                $admin->edit_dossier((array_key_exists("id", $_REQUEST)) ? $_REQUEST["id"] : 0);
                break;
            case "save_dossier" :
                $admin->save_dossier($_POST["dossier"]);
                break;
            case "delete_dossier" :
                $admin->delete_dossier($_REQUEST["dossier"]["id"]);
                break;
            case "show_posts" :
                $admin->show_posts((array_key_exists("options", $_REQUEST)) ? $_REQUEST["options"] : "");
                break;
            case "edit_post" :
                $admin->edit_post((array_key_exists("id", $_REQUEST)) ? $_REQUEST["id"] : 0);
                break;
            case "save_post" :
                $admin->save_post($_POST["post"]);
                break;
            case "delete_post" :
                $admin->delete_post($_REQUEST["post"]["id"]);
                break;
            case "show_authors" :
                $admin->show_authors();
                break;
            case "edit_author" :
                $admin->edit_author((array_key_exists("id", $_REQUEST)) ? $_REQUEST["id"] : 0);
                break;
            case "save_author" :
                $admin->save_author($_POST["author"]);
                break;
            case "show_users" :
                $admin->show_users();
                break;
            case "edit_user" :
                $admin->edit_user((array_key_exists("id", $_REQUEST)) ? $_REQUEST["id"] : 0);
                break;
            case "save_user" :
                $admin->save_user($_POST["user"]);
                break;
            case "delete_inactive_users" :
                $admin->delete_inactive_users($_POST["select_inactive"]);
                break;
            case "delete_user" :
                $admin->delete_user($_REQUEST["user"]["id"]);
                break;
            case "show_settings" :
                $admin->show_settings();
                break;
            case "save_settings" :
                $admin->save_settings($_POST["settings"]);
                $admin->show_settings();
                break;
            case "show_menuitems" :
                $admin->show_menuitems();
                break;
            case "save_menuitems" :
                $admin->save_menuitems($_POST["link"]);
                break;
            case "show_comments" :
                $admin->show_comments((array_key_exists("options", $_REQUEST)) ? $_REQUEST["options"] : "");
                break;
            case "delete_comment" :
                $admin->delete_comment($_REQUEST["id"]);
                break;
            case "recover_comment" :
                $admin->recover_comment($_REQUEST["id"]);
                break;
            case "show_plugins" :
                $admin->show_plugins();
                break;
            case "activate_plugin" :
                $admin->_activate_plugin($_REQUEST["plugin"]);
                $admin->show_plugins();
                break;
            case "deactivate_plugin" :
                $admin->_deactivate_plugin($_REQUEST["plugin"]);
                $admin->show_plugins();
                break;
            case "config_plugin" :
                $admin->configure_plugin($_REQUEST["plugin"]);
                break;
            case "edit_plugin_setting" :
                $admin->edit_plugin_setting($_REQUEST["plugin"], $_REQUEST);
                break;
            case "save_plugin_setting" :
                $admin->save_plugin_setting($_REQUEST["plugin"], $_REQUEST);
                break;
            case "show_import" :
                $admin->show_import();
                break;
            case "import" :
                $admin->import((array_key_exists("type", $_REQUEST)) ? $_REQUEST["type"] : "", $_REQUEST);
                break;
            case "show_about" :
                $admin->show_about();
                break;
            case "show_userlog" :
                $admin->show_userlog();
                break;
            case "show_adminlog" :
                $admin->show_adminlog();
                break;
            default : 
                $admin->show_index();
                break;
            }
$admin->html_footer();
?>
