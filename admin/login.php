<?php
/**
             * MvBlog -- An open source no-nosense blogtool
             *
             * Copyright (C) 2005-2008, Michiel van Baak
             * Michiel van Baak <mvanbaak@users.sourceforge.net>
             *
             * See http://dev.mvblog.org for more information on MvBlog.
             * That page also provides Bugtrackers, Filereleases etc.
             *
             * This program is free software, distributed under the terms of
             * the GNU General Public License Version 2. See the LICENSE file
             * at the top of the source tree.
             *
             * PHP version 5
             *
             * @category  PHP
             * @package   MvBlog
             * @author    Michiel van Baak <mvanbaak@users.sourceforge.net>
             * @copyright 2005-2008 Michiel van Baak
             * @license   GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
             * @version   SVN: $Revision: 809 $
             * @link      http://www.mvblog.org
             */

/**
 * Show login screen
 */
require_once "../common/functions_blog.php";

/**
 * dump the html for a loginscreen to the client
 *
 * @return void
 */
function show_login_screen() {
    ?>
	<form name="loginform" method="post" action="login.php">
	<input type="hidden" name="action" value="login" />
	<div id="if_container">
		<div id="if_title"></div>
		<div id="if_bar1"></div>
		<div id="if_page_header">
			<h1 class="page_title">login</h1>
		</div>
		<div id="if_page">
			<div class="log_post">
				<table border="0" cellspacing="3" cellpadding="0" align="center"><tr>
					<td align="right">username:</td><td><input type="text" id="loginname" name="login[name]" /></td>
				</tr><tr>
					<td align="right">password:</td><td><input type="password" name="login[password]" /></td>
				</tr><tr>
					<td colspan="2" align="center"><input type="submit" value="login" /></td>
				</tr></table>
			</div>
	</form>
	<script language="Javascript" type="text/javascript">
		document.loginform.loginname.focus();
	</script>
	<?php
}

/**
 * Check provided username and password against the authors database table
 *
 * @param array $login Array with name and password
 *
 * @return void
 */
function check_login($login) {
    global $db;
    $query = sprintf(
        "SELECT * FROM authors WHERE login = '%s' AND password = '%s' AND active = 1",
        preg_quote($login["name"], "'"),
        preg_quote($login["password"], "'")
    );
    $res = & $db->query($query);
    if (PEAR::isError($res)) {
        die($res->getUserInfo());
    }
    if ($res->numRows()) {
        $row = $res->fetchRow(MDB2_FETCHMODE_ASSOC);
        $_SESSION["author_id"]       = $row["id"];
        $_SESSION["author_name"]     = $row["login"];
        $_SESSION["author_fullname"] = $row["fullname"];
        $_SESSION["author_email"]    = $row["email"];
        $_SESSION["author_website"]  = $row["website"];
        $_SESSION["blog_user"]       = 1;
        header("Location: index.php");
    } else {
        show_login_screen();
    }
}

/**
 * Logout user
 *
 * @return void
 */
function logout() {
    session_destroy();
    show_login_screen();
}

/**
 * echo html start to client
 *
 * @param string $title The title to use
 *
 * @return void
 */
function html_header_admin($title = "Admin interface") {
    echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\"";
    echo "	\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n";
    echo "<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\" xml:lang=\"en\">\n";
    echo "<head>\n";
    echo "\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-15\" />\n";
    echo "\t<meta name=\"robots\" content=\"ALL\" />\n";
    echo "\t<meta name=\"generator\" content=\"MvBlog " . $GLOBALS["version"] . "\" />\n";
    echo "\t<meta name=\"resource-type\" content=\"document\" />\n";
    echo "\t<meta name=\"audience\" content=\"general\" />\n";
    echo "\t<meta name=\"web-rev\" content=\"4.0\" />\n";
    echo "\t<meta name=\"last-modified\" content=\"" . date("r") . "\" />\n";
    echo "\t<link rel=\"stylesheet\" href=\"style/index.css\" type=\"text/css\" />\n";
    echo "\t<link rel=\"alternate\" type=\"application/xml\" title=\"RSS\" href=\"index.php?action=rss\" />\n";
    echo "\t<link rel=\"icon\" href=\"favicon.ico\" type=\"image/x-icon\" />\n";
    echo "\t<link rel=\"shortcut icon\" href=\"favicon.ico\" type=\"image/x-icon\" />\n";
    echo "\t<title>" . $title . " - MvBlog powered</title>\n";
    echo "</head>\n";
    echo "<body>\n";
}


html_header_admin("Admin Interface", ".");
switch ($_REQUEST["action"]) {
case "login" :
    check_login($_POST["login"]);
    break;
case "logout" :
    logout();
    break;
default : show_login_screen();
    break;
}
html_footer();
?>
