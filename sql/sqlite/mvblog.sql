CREATE TABLE acronyms (
  id INTEGER PRIMARY KEY,
  acronym varchar(255) default NULL,
  description text
);
INSERT INTO acronyms VALUES (1,'mvblog','The engine this blog is running on. Coded by Michiel van Baak');
INSERT INTO acronyms VALUES (2,'AOL','America Online');
INSERT INTO acronyms VALUES (3,'API','Application Programming Interface');
INSERT INTO acronyms VALUES (4,'BTW','By The Way');
INSERT INTO acronyms VALUES (5,'BPL','Broadband over Power Lines');
INSERT INTO acronyms VALUES (6,'CD','Compact Disk');
INSERT INTO acronyms VALUES (7,'CGI','Common Gateway Interface');
INSERT INTO acronyms VALUES (8,'CLI','Common Language Interpreter');
INSERT INTO acronyms VALUES (9,'CMS','Content Management System');
INSERT INTO acronyms VALUES (10,'CSS','Cascading Style Sheets');
INSERT INTO acronyms VALUES (11,'CVS','Concurrent Versions System');
INSERT INTO acronyms VALUES (12,'DNS','Domain Name System');
INSERT INTO acronyms VALUES (13,'DOM','Document Object Model');
INSERT INTO acronyms VALUES (14,'DSL','Digital Subscriber Line (broadband over phone lines)');
INSERT INTO acronyms VALUES (15,'DTD','Document Type Definition');
INSERT INTO acronyms VALUES (16,'DVD','Digital Video Disc');
INSERT INTO acronyms VALUES (17,'EBI','Elim Bible Institute (Bible College in upstate NY)');
INSERT INTO acronyms VALUES (18,'EGC','Elim Gospel Church (my church)');
INSERT INTO acronyms VALUES (19,'EF','Elim Fellowship (an association of evangelical ministers)');
INSERT INTO acronyms VALUES (20,'FAQ','Frequently Asked Questions');
INSERT INTO acronyms VALUES (21,'FCC','Federal Communications Commission');
INSERT INTO acronyms VALUES (22,'FOAF','Friend Of A Friend is a RDF dialect for describing relationships');
INSERT INTO acronyms VALUES (23,'FSF','Free Software Foundation');
INSERT INTO acronyms VALUES (24,'FTP','File Transfer Protocol');
INSERT INTO acronyms VALUES (25,'FTTH','Fiber to the Home');
INSERT INTO acronyms VALUES (26,'GB','Gigabyte');
INSERT INTO acronyms VALUES (27,'GFDL','GNU Free Documentation License');
INSERT INTO acronyms VALUES (28,'GPG','Gnu PG (Open source public key encryption)');
INSERT INTO acronyms VALUES (29,'GPL','GNU General Public License');
INSERT INTO acronyms VALUES (30,'GTK','GUI ToolKit - The GIMP Tool Kit for dcreating user-interfaces');
INSERT INTO acronyms VALUES (31,'GUI','Graphical User Interface');
INSERT INTO acronyms VALUES (32,'HDTV','High Definition TeleVision');
INSERT INTO acronyms VALUES (33,'HTML','HyperText Markup Language');
INSERT INTO acronyms VALUES (34,'HTTP','HyperText Transfer Protocol');
INSERT INTO acronyms VALUES (35,'IE','Internet Explorer');
INSERT INTO acronyms VALUES (36,'ICANN','Internet Corporation for Assigned Names and Numbers');
INSERT INTO acronyms VALUES (37,'IHOP','International House of Pancakes');
INSERT INTO acronyms VALUES (38,'IIRC','if I remember correctly');
INSERT INTO acronyms VALUES (39,'IIS','Internet Infomation Server');
INSERT INTO acronyms VALUES (40,'IM','Instant Message');
INSERT INTO acronyms VALUES (41,'IMAP','Internet Message Access Protocol');
INSERT INTO acronyms VALUES (42,'IP','Internet Protocol');
INSERT INTO acronyms VALUES (43,'IRC','Internet Relay Chat - like Instant Messaging for groups');
INSERT INTO acronyms VALUES (44,'JSP','Java Server Pages');
INSERT INTO acronyms VALUES (45,'KB','Kilobyte');
INSERT INTO acronyms VALUES (46,'KDE','K Desktop Environment');
INSERT INTO acronyms VALUES (47,'KVM','Keyboard, Video, Mouse switch for controlling multiple computers');
INSERT INTO acronyms VALUES (48,'LDAP','Lightweight Directory Access Protocol');
INSERT INTO acronyms VALUES (49,'LGPL','GNU Lesser General Public License');
INSERT INTO acronyms VALUES (50,'MAPI','Mail Application Programming Interface');
INSERT INTO acronyms VALUES (51,'MB','Megabyte');
INSERT INTO acronyms VALUES (52,'MP3','MPEG Layer 3 - a common audio codec for music files');
INSERT INTO acronyms VALUES (53,'MS','Microsoft');
INSERT INTO acronyms VALUES (54,'MSDN','Microsoft Developer Network');
INSERT INTO acronyms VALUES (55,'MSIE','Microsoft Internet Explorer');
INSERT INTO acronyms VALUES (56,'MSN','Microsoft Network');
INSERT INTO acronyms VALUES (57,'NNTP','Network News Transfer Protocol - the protocol used for NewsGroups');
INSERT INTO acronyms VALUES (58,'NYC','New York City');
INSERT INTO acronyms VALUES (59,'OPML','Outline Processor Markup Language');
INSERT INTO acronyms VALUES (60,'P2P','Peer To Peer');
INSERT INTO acronyms VALUES (61,'PBS','Public Broadcasting System');
INSERT INTO acronyms VALUES (62,'PDF','Portable Document Format');
INSERT INTO acronyms VALUES (63,'PGP','Pretty Good Privacy (public key encryption)');
INSERT INTO acronyms VALUES (64,'PHP','Hypertext PreProcessing');
INSERT INTO acronyms VALUES (65,'PNG','Portable Network Graphics');
INSERT INTO acronyms VALUES (66,'POP','Short for POP3, the Post Office Protocol for email');
INSERT INTO acronyms VALUES (67,'POP3','Post Office Protocol 3 (for email)');
INSERT INTO acronyms VALUES (68,'RAID','Redundant Array of Independent Disks');
INSERT INTO acronyms VALUES (69,'RDF','Resource Description Framework');
INSERT INTO acronyms VALUES (70,'RPC','Remote Procedure Call');
INSERT INTO acronyms VALUES (71,'RSS','Really Simple Syndication');
INSERT INTO acronyms VALUES (72,'SIG','Special Interest Group');
INSERT INTO acronyms VALUES (73,'SOAP','Simple Object Access Protocol');
INSERT INTO acronyms VALUES (74,'SQL','Structured Query Language (a database standard)');
INSERT INTO acronyms VALUES (75,'SSH','Secure SHell (encrypted protocol replaces telnet and FTP)');
INSERT INTO acronyms VALUES (76,'SSN','Social Security Number');
INSERT INTO acronyms VALUES (77,'SSL','Secure Sockets Layer (a security protocol)');
INSERT INTO acronyms VALUES (78,'SVG','Scalable Vector Graphics');
INSERT INTO acronyms VALUES (79,'TCP','Transmission Control Protocol');
INSERT INTO acronyms VALUES (80,'UDP','User Datagram Protocol');
INSERT INTO acronyms VALUES (81,'URI','Uniform Resource Identifier');
INSERT INTO acronyms VALUES (82,'URL','Uniform Resource Locator');
INSERT INTO acronyms VALUES (83,'USB','Universal Serial Bus');
INSERT INTO acronyms VALUES (84,'VB','Visual Basic');
INSERT INTO acronyms VALUES (85,'VBS','Visual Basic Script');
INSERT INTO acronyms VALUES (86,'VM','Virtual Machine');
INSERT INTO acronyms VALUES (87,'VNC','Virtual Network Computing');
INSERT INTO acronyms VALUES (88,'W3C','World Wide Web Consortium');
INSERT INTO acronyms VALUES (89,'WCAG','Web Content Accessibility Guidelines');
INSERT INTO acronyms VALUES (90,'WYSIWYG','what you see is what you get');
INSERT INTO acronyms VALUES (91,'XHTML','eXtensible HyperText Markup Language - HTML reformulated as XML');
INSERT INTO acronyms VALUES (92,'XML','eXtensible Markup Language');
INSERT INTO acronyms VALUES (93,'XSL','eXtensible Stylesheet Language');
INSERT INTO acronyms VALUES (94,'XSLT','eXtensible Stylesheet Language Transformation');
CREATE TABLE articles (
  id INTEGER PRIMARY KEY,
  date int(11) NOT NULL,
  title varchar(255) NOT NULL,
  head text,
  body text,
  categories_ids varchar(255),
  authors_id int(11) NOT NULL,
  active smallint(2) default NULL,
  public smallint(2) default NULL,
  mail_comments smallint(2) default NULL,
  last_modified int(11) default NULL,
  modified_by int(11) default NULL,
  ping_sent smallint(2) default NULL,
  tb_uri varchar(255) default NULL,
  aside smallint(2) default '0',
  allowanoncomments smallint(2) default '0',
  dossier_id int(11),
  postformat varchar(10) DEFAULT 'HTML'
);
CREATE TABLE authors (
  id INTEGER PRIMARY KEY,
  login varchar(255) NOT NULL,
  password varchar(255) NOT NULL,
  email varchar(255) default NULL,
  fullname varchar(255) default NULL,
  active smallint(3) default '1',
  website varchar(255) default NULL
);
INSERT INTO authors VALUES (1,'mvblog','mvblog','mvblog@example.com','MvBlog default user',1,'www.mvblog.org');
CREATE TABLE blog_users (
  id INTEGER PRIMARY KEY,
  username varchar(255) default NULL,
  password varchar(255) default NULL,
  realname varchar(255) default NULL,
  email varchar(255) default NULL,
  website varchar(255) default NULL,
  regcode varchar(255) default NULL,
  active smallint(2) default '0',
  email_public smallint(3)
);
CREATE TABLE categories (
  id INTEGER PRIMARY KEY,
  name varchar(255) NOT NULL,
  desc text,
  public smallint(2) default NULL,
  active smallint(2) default NULL,
  icon varchar(255)
);
CREATE TABLE comments (
  id INTEGER PRIMARY KEY,
  name varchar(255) NOT NULL,
  email varchar(255) NOT NULL,
  website varchar(255) default NULL,
  comment text NOT NULL,
  date int(11) NOT NULL,
  articles_id int(11) NOT NULL,
  title varchar(255) default NULL,
  deleted int(11) default '0',
  ip varchar(255) default NULL
);
CREATE TABLE menulinks (
  id INTEGER PRIMARY KEY,
  url varchar(255) default NULL,
  linktitle text,
  image varchar(255) default NULL,
  sortorder int(11) default NULL
);
CREATE TABLE settings (
  id INTEGER PRIMARY KEY,
  settingname varchar(255) NOT NULL,
  settingvalue text
);
CREATE TABLE dossiers (
	id INTEGER PRIMARY KEY,
	name varchar(255) NOT NULL,
	desc varchar(255),
	public smallint(3) default '1',
	active smallint(3) default '1'
);
CREATE TABLE log (
	time INTEGER,
	user_id INTEGER,
	user_type smallint(3),
	msg text
);
INSERT INTO settings (settingname, settingvalue) VALUES ('dbversion', '2007123000');
