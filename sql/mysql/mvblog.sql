-- MySQL dump 10.10
--
-- Host: localhost    Database: mvblog
-- ------------------------------------------------------
-- Server version	5.0.22-Debian_3-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `acronyms`
--

DROP TABLE IF EXISTS `acronyms`;
CREATE TABLE `acronyms` (
  `id` int(11) NOT NULL auto_increment,
  `acronym` varchar(255) default NULL,
  `description` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `acronyms`
--


/*!40000 ALTER TABLE `acronyms` DISABLE KEYS */;
LOCK TABLES `acronyms` WRITE;
INSERT INTO `acronyms` VALUES (1,'mvblog','The engine this blog is running on. Coded by Michiel van Baak'),(2,'AOL','America Online'),(3,'API','Application Programming Interface'),(4,'BTW','By The Way'),(5,'BPL','Broadband over Power Lines'),(6,'CD','Compact Disk'),(7,'CGI','Common Gateway Interface'),(8,'CLI','Common Language Interpreter'),(9,'CMS','Content Management System'),(10,'CSS','Cascading Style Sheets'),(11,'CVS','Concurrent Versions System'),(12,'DNS','Domain Name System'),(13,'DOM','Document Object Model'),(14,'DSL','Digital Subscriber Line (broadband over phone lines)'),(15,'DTD','Document Type Definition'),(16,'DVD','Digital Video Disc'),(17,'EBI','Elim Bible Institute (Bible College in upstate NY)'),(18,'EGC','Elim Gospel Church (my church)'),(19,'EF','Elim Fellowship (an association of evangelical ministers)'),(20,'FAQ','Frequently Asked Questions'),(21,'FCC','Federal Communications Commission'),(22,'FOAF','Friend Of A Friend is a RDF dialect for describing relationships'),(23,'FSF','Free Software Foundation'),(24,'FTP','File Transfer Protocol'),(25,'FTTH','Fiber to the Home'),(26,'GB','Gigabyte'),(27,'GFDL','GNU Free Documentation License'),(28,'GPG','Gnu PG (Open source public key encryption)'),(29,'GPL','GNU General Public License'),(30,'GTK','GUI ToolKit - The GIMP Tool Kit for dcreating user-interfaces'),(31,'GUI','Graphical User Interface'),(32,'HDTV','High Definition TeleVision'),(33,'HTML','HyperText Markup Language'),(34,'HTTP','HyperText Transfer Protocol'),(35,'IE','Internet Explorer'),(36,'ICANN','Internet Corporation for Assigned Names and Numbers'),(37,'IHOP','International House of Pancakes'),(38,'IIRC','if I remember correctly'),(39,'IIS','Internet Infomation Server'),(40,'IM','Instant Message'),(41,'IMAP','Internet Message Access Protocol'),(42,'IP','Internet Protocol'),(43,'IRC','Internet Relay Chat - like Instant Messaging for groups'),(44,'JSP','Java Server Pages'),(45,'KB','Kilobyte'),(46,'KDE','K Desktop Environment'),(47,'KVM','Keyboard, Video, Mouse switch for controlling multiple computers'),(48,'LDAP','Lightweight Directory Access Protocol'),(49,'LGPL','GNU Lesser General Public License'),(50,'MAPI','Mail Application Programming Interface'),(51,'MB','Megabyte'),(52,'MP3','MPEG Layer 3 - a common audio codec for music files'),(53,'MS','Microsoft'),(54,'MSDN','Microsoft Developer Network'),(55,'MSIE','Microsoft Internet Explorer'),(56,'MSN','Microsoft Network'),(57,'NNTP','Network News Transfer Protocol - the protocol used for NewsGroups'),(58,'NYC','New York City'),(59,'OPML','Outline Processor Markup Language'),(60,'P2P','Peer To Peer'),(61,'PBS','Public Broadcasting System'),(62,'PDF','Portable Document Format'),(63,'PGP','Pretty Good Privacy (public key encryption)'),(64,'PHP','Hypertext PreProcessing'),(65,'PNG','Portable Network Graphics'),(66,'POP','Short for POP3, the Post Office Protocol for email'),(67,'POP3','Post Office Protocol 3 (for email)'),(68,'RAID','Redundant Array of Independent Disks'),(69,'RDF','Resource Description Framework'),(70,'RPC','Remote Procedure Call'),(71,'RSS','Really Simple Syndication'),(72,'SIG','Special Interest Group'),(73,'SOAP','Simple Object Access Protocol'),(74,'SQL','Structured Query Language (a database standard)'),(75,'SSH','Secure SHell (encrypted protocol replaces telnet and FTP)'),(76,'SSN','Social Security Number'),(77,'SSL','Secure Sockets Layer (a security protocol)'),(78,'SVG','Scalable Vector Graphics'),(79,'TCP','Transmission Control Protocol'),(80,'UDP','User Datagram Protocol'),(81,'URI','Uniform Resource Identifier'),(82,'URL','Uniform Resource Locator'),(83,'USB','Universal Serial Bus'),(84,'VB','Visual Basic'),(85,'VBS','Visual Basic Script'),(86,'VM','Virtual Machine'),(87,'VNC','Virtual Network Computing'),(88,'W3C','World Wide Web Consortium'),(89,'WCAG','Web Content Accessibility Guidelines'),(90,'WYSIWYG','what you see is what you get'),(91,'XHTML','eXtensible HyperText Markup Language - HTML reformulated as XML'),(92,'XML','eXtensible Markup Language'),(93,'XSL','eXtensible Stylesheet Language'),(94,'XSLT','eXtensible Stylesheet Language Transformation');
UNLOCK TABLES;
/*!40000 ALTER TABLE `acronyms` ENABLE KEYS */;

--
-- Table structure for table `articles`
--

DROP TABLE IF EXISTS `articles`;
CREATE TABLE `articles` (
  `id` int(11) NOT NULL auto_increment,
  `date` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `head` text,
  `body` text,
  `categories_id` int(11) NOT NULL,
  `authors_id` int(11) NOT NULL,
  `active` smallint(2) default NULL,
  `public` smallint(2) default NULL,
  `mail_comments` smallint(2) default NULL,
  `last_modified` int(11) default NULL,
  `modified_by` int(11) default NULL,
  `ping_sent` smallint(2) default NULL,
  `tb_uri` varchar(255) default NULL,
  `aside` smallint(2) default '0',
  `allowanoncomments` smallint(2) default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `articles`
--


/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
LOCK TABLES `articles` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;

--
-- Table structure for table `authors`
--

DROP TABLE IF EXISTS `authors`;
CREATE TABLE `authors` (
  `id` int(11) NOT NULL auto_increment,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) default NULL,
  `fullname` varchar(255) default NULL,
  `active` smallint(3) default '1',
  `website` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `authors`
--


/*!40000 ALTER TABLE `authors` DISABLE KEYS */;
LOCK TABLES `authors` WRITE;
INSERT INTO `authors` VALUES (1,'mvblog','mvblog','mvblog@example.com','MvBlog default user',1,'www.mvblog.org');
UNLOCK TABLES;
/*!40000 ALTER TABLE `authors` ENABLE KEYS */;

--
-- Table structure for table `blog_users`
--

DROP TABLE IF EXISTS `blog_users`;
CREATE TABLE `blog_users` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(255) default NULL,
  `password` varchar(255) default NULL,
  `realname` varchar(255) default NULL,
  `email` varchar(255) default NULL,
  `website` varchar(255) default NULL,
  `regcode` varchar(255) default NULL,
  `active` smallint(2) default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog_users`
--


/*!40000 ALTER TABLE `blog_users` DISABLE KEYS */;
LOCK TABLES `blog_users` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `blog_users` ENABLE KEYS */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `desc` text,
  `public` smallint(2) default NULL,
  `active` smallint(2) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--


/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
LOCK TABLES `categories` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `website` varchar(255) default NULL,
  `comment` text NOT NULL,
  `date` int(11) NOT NULL,
  `articles_id` int(11) NOT NULL,
  `title` varchar(255) default NULL,
  `deleted` int(11) default '0',
  `ip` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--


/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
LOCK TABLES `comments` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;

--
-- Table structure for table `menulinks`
--

DROP TABLE IF EXISTS `menulinks`;
CREATE TABLE `menulinks` (
  `id` int(11) NOT NULL auto_increment,
  `url` varchar(255) default NULL,
  `linktitle` text,
  `image` varchar(255) default NULL,
  `sortorder` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menulinks`
--


/*!40000 ALTER TABLE `menulinks` DISABLE KEYS */;
LOCK TABLES `menulinks` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `menulinks` ENABLE KEYS */;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(11) NOT NULL auto_increment,
  `settingname` varchar(255) NOT NULL,
  `settingvalue` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--


/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
LOCK TABLES `settings` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

