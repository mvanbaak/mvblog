--
-- PostgreSQL database dump
--

SET client_encoding = 'LATIN1';
SET check_function_bodies = false;

SET SESSION AUTHORIZATION 'postgres';

--
-- TOC entry 4 (OID 2200)
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO PUBLIC;


SET SESSION AUTHORIZATION 'mvblog';

SET search_path = public, pg_catalog;

--
-- TOC entry 5 (OID 626108)
-- Name: acronyms; Type: TABLE; Schema: public; Owner: mvblog
--

CREATE TABLE acronyms (
    id serial NOT NULL,
    acronym character varying,
    description text
);


--
-- TOC entry 6 (OID 626116)
-- Name: authors; Type: TABLE; Schema: public; Owner: mvblog
--

CREATE TABLE authors (
    id serial NOT NULL,
    login character varying(255) NOT NULL,
    "password" character varying(255) NOT NULL,
    email character varying(255),
    fullname character varying(255),
    active smallint DEFAULT 1,
    website character varying(255)
);

CREATE TABLE blog_users (
    id serial NOT NULL,
    username character varying(255),
    "password" character varying(255),
    realname character varying(255),
    email character varying(255),
    website character varying(255),
    regcode character varying(255),
    active smallint DEFAULT 0
);

--
-- TOC entry 7 (OID 626122)
-- Name: categories; Type: TABLE; Schema: public; Owner: mvblog
--

CREATE TABLE categories (
    id serial NOT NULL,
    name character varying(255) NOT NULL,
    "desc" text,
    public smallint,
    active smallint
);


--
-- TOC entry 8 (OID 626130)
-- Name: articles; Type: TABLE; Schema: public; Owner: mvblog
--

CREATE TABLE articles (
    id serial NOT NULL,
    date integer NOT NULL,
    title character varying NOT NULL,
    head text,
    body text,
    categories_id integer NOT NULL,
    authors_id integer NOT NULL,
    active smallint,
    public smallint,
    mail_comments smallint,
    last_modified integer,
    modified_by integer,
    ping_sent smallint,
    tb_uri character varying(255),
    aside smallint DEFAULT 0,
    allowanoncomments smallint DEFAULT 0
);


--
-- TOC entry 9 (OID 626138)
-- Name: settings; Type: TABLE; Schema: public; Owner: mvblog
--

CREATE TABLE settings (
    id serial NOT NULL,
    settingname character varying(255) NOT NULL,
    settingvalue text
);


--
-- TOC entry 10 (OID 626146)
-- Name: comments; Type: TABLE; Schema: public; Owner: mvblog
--

CREATE TABLE comments (
    id serial NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    website character varying(255),
    "comment" text NOT NULL,
    date integer NOT NULL,
    articles_id integer NOT NULL,
    title character varying(255),
    deleted integer DEFAULT 0,
    ip character varying(255)
);


--
-- TOC entry 11 (OID 626265)
-- Name: menulinks; Type: TABLE; Schema: public; Owner: mvblog
--

CREATE TABLE menulinks (
    id serial NOT NULL,
    url character varying,
    linktitle text,
    image character varying,
    sortorder integer
);


--
-- Data for TOC entry 26 (OID 626108)
-- Name: acronyms; Type: TABLE DATA; Schema: public; Owner: mvblog
--

COPY acronyms (id, acronym, description) FROM stdin;
1	mvblog	The engine this blog is running on. Coded by Michiel van Baak
2	AOL	America Online
3	API	Application Programming Interface
4	BTW	By The Way
5	BPL	Broadband over Power Lines
6	CD	Compact Disk
7	CGI	Common Gateway Interface
8	CLI	Common Language Interpreter
9	CMS	Content Management System
10	CSS	Cascading Style Sheets
11	CVS	Concurrent Versions System
12	DNS	Domain Name System
13	DOM	Document Object Model
14	DSL	Digital Subscriber Line (broadband over phone lines)
15	DTD	Document Type Definition
16	DVD	Digital Video Disc
17	EBI	Elim Bible Institute (Bible College in upstate NY)
18	EGC	Elim Gospel Church (my church)
19	EF	Elim Fellowship (an association of evangelical ministers)
20	FAQ	Frequently Asked Questions
21	FCC	Federal Communications Commission
22	FOAF	Friend Of A Friend is a RDF dialect for describing relationships
23	FSF	Free Software Foundation
24	FTP	File Transfer Protocol
25	FTTH	Fiber to the Home
26	GB	Gigabyte
27	GFDL	GNU Free Documentation License
28	GPG	Gnu PG (Open source public key encryption)
29	GPL	GNU General Public License
30	GTK	GUI ToolKit - The GIMP Tool Kit for dcreating user-interfaces
31	GUI	Graphical User Interface
32	HDTV	High Definition TeleVision
33	HTML	HyperText Markup Language
34	HTTP	HyperText Transfer Protocol
35	IE	Internet Explorer
36	ICANN	Internet Corporation for Assigned Names and Numbers
37	IHOP	International House of Pancakes
38	IIRC	if I remember correctly
39	IIS	Internet Infomation Server
40	IM	Instant Message
41	IMAP	Internet Message Access Protocol
42	IP	Internet Protocol
43	IRC	Internet Relay Chat - like Instant Messaging for groups
44	JSP	Java Server Pages
45	KB	Kilobyte
46	KDE	K Desktop Environment
47	KVM	Keyboard, Video, Mouse switch for controlling multiple computers
48	LDAP	Lightweight Directory Access Protocol
49	LGPL	GNU Lesser General Public License
50	MAPI	Mail Application Programming Interface
51	MB	Megabyte
52	MP3	MPEG Layer 3 - a common audio codec for music files
53	MS	Microsoft
54	MSDN	Microsoft Developer Network
55	MSIE	Microsoft Internet Explorer
56	MSN	Microsoft Network
57	NNTP	Network News Transfer Protocol - the protocol used for NewsGroups
58	NYC	New York City
59	OPML	Outline Processor Markup Language
60	P2P	Peer To Peer
61	PBS	Public Broadcasting System
62	PDF	Portable Document Format
63	PGP	Pretty Good Privacy (public key encryption)
64	PHP	Hypertext PreProcessing
65	PNG	Portable Network Graphics
66	POP	Short for POP3, the Post Office Protocol for email
67	POP3	Post Office Protocol 3 (for email)
68	RAID	Redundant Array of Independent Disks
69	RDF	Resource Description Framework
70	RPC	Remote Procedure Call
71	RSS	Really Simple Syndication
72	SIG	Special Interest Group
73	SOAP	Simple Object Access Protocol
74	SQL	Structured Query Language (a database standard)
75	SSH	Secure SHell (encrypted protocol replaces telnet and FTP)
76	SSN	Social Security Number
77	SSL	Secure Sockets Layer (a security protocol)
78	SVG	Scalable Vector Graphics
79	TCP	Transmission Control Protocol
80	UDP	User Datagram Protocol
81	URI	Uniform Resource Identifier
82	URL	Uniform Resource Locator
83	USB	Universal Serial Bus
84	VB	Visual Basic
85	VBS	Visual Basic Script
86	VM	Virtual Machine
87	VNC	Virtual Network Computing
88	W3C	World Wide Web Consortium
89	WCAG	Web Content Accessibility Guidelines
90	WYSIWYG	what you see is what you get
91	XHTML	eXtensible HyperText Markup Language - HTML reformulated as XML
92	XML	eXtensible Markup Language
93	XSL	eXtensible Stylesheet Language
94	XSLT	eXtensible Stylesheet Language Transformation
\.


--
-- Data for TOC entry 27 (OID 626116)
-- Name: authors; Type: TABLE DATA; Schema: public; Owner: mvblog
--

COPY authors (id, login, "password", email, fullname, active, website) FROM stdin;
1	mvblog	mvblog	mvblog@example.com	MvBlog default user	1	www.mvblog.org
\.


--
-- Data for TOC entry 28 (OID 626122)
-- Name: categories; Type: TABLE DATA; Schema: public; Owner: mvblog
--

COPY categories (id, name, "desc", public, active) FROM stdin;
\.


--
-- Data for TOC entry 29 (OID 626130)
-- Name: articles; Type: TABLE DATA; Schema: public; Owner: mvblog
--

COPY articles (id, date, title, head, body, categories_id, authors_id, active, public, mail_comments, last_modified, modified_by, ping_sent, tb_uri, aside) FROM stdin;
\.


--
-- Data for TOC entry 30 (OID 626138)
-- Name: settings; Type: TABLE DATA; Schema: public; Owner: mvblog
--

COPY settings (id, settingname, settingvalue) FROM stdin;
\.


--
-- Data for TOC entry 31 (OID 626146)
-- Name: comments; Type: TABLE DATA; Schema: public; Owner: mvblog
--

COPY comments (id, name, email, website, "comment", date, articles_id, title, deleted, ip) FROM stdin;
\.


--
-- Data for TOC entry 32 (OID 626265)
-- Name: menulinks; Type: TABLE DATA; Schema: public; Owner: mvblog
--

COPY menulinks (id, url, linktitle, image, sortorder) FROM stdin;
\.


--
-- TOC entry 19 (OID 626247)
-- Name: acronyms_pkey; Type: CONSTRAINT; Schema: public; Owner: mvblog
--

ALTER TABLE ONLY acronyms
    ADD CONSTRAINT acronyms_pkey PRIMARY KEY (id);


--
-- TOC entry 20 (OID 626249)
-- Name: authors_pkey; Type: CONSTRAINT; Schema: public; Owner: mvblog
--

ALTER TABLE ONLY authors
    ADD CONSTRAINT authors_pkey PRIMARY KEY (id);

ALTER TABLE ONLY blog_users
    ADD CONSTRAINT blog_users_id_pkey PRIMARY KEY (id);

--
-- TOC entry 21 (OID 626251)
-- Name: categories_pkey; Type: CONSTRAINT; Schema: public; Owner: mvblog
--

ALTER TABLE ONLY categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);


--
-- TOC entry 23 (OID 626253)
-- Name: settings_pkey; Type: CONSTRAINT; Schema: public; Owner: mvblog
--

ALTER TABLE ONLY settings
    ADD CONSTRAINT settings_pkey PRIMARY KEY (id);


--
-- TOC entry 22 (OID 626255)
-- Name: articles_pkey; Type: CONSTRAINT; Schema: public; Owner: mvblog
--

ALTER TABLE ONLY articles
    ADD CONSTRAINT articles_pkey PRIMARY KEY (id);


--
-- TOC entry 24 (OID 626257)
-- Name: comments_pkey; Type: CONSTRAINT; Schema: public; Owner: mvblog
--

ALTER TABLE ONLY comments
    ADD CONSTRAINT comments_pkey PRIMARY KEY (id);


--
-- TOC entry 25 (OID 626271)
-- Name: menulinks_pkey; Type: CONSTRAINT; Schema: public; Owner: mvblog
--

ALTER TABLE ONLY menulinks
    ADD CONSTRAINT menulinks_pkey PRIMARY KEY (id);


--
-- TOC entry 12 (OID 626106)
-- Name: acronyms_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mvblog
--

SELECT pg_catalog.setval('acronyms_id_seq', 94, true);


--
-- TOC entry 13 (OID 626114)
-- Name: authors_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mvblog
--

SELECT pg_catalog.setval('authors_id_seq', 1, true);


--
-- TOC entry 14 (OID 626120)
-- Name: categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mvblog
--

SELECT pg_catalog.setval('categories_id_seq', 2, true);


--
-- TOC entry 15 (OID 626128)
-- Name: articles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mvblog
--

SELECT pg_catalog.setval('articles_id_seq', 2, true);


--
-- TOC entry 16 (OID 626136)
-- Name: settings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mvblog
--

SELECT pg_catalog.setval('settings_id_seq', 1, true);


--
-- TOC entry 17 (OID 626144)
-- Name: comments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mvblog
--

SELECT pg_catalog.setval('comments_id_seq', 1, true);


--
-- TOC entry 18 (OID 626263)
-- Name: menulinks_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mvblog
--

SELECT pg_catalog.setval('menulinks_id_seq', 1, false);


SET SESSION AUTHORIZATION 'postgres';

--
-- TOC entry 3 (OID 2200)
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'Standard public schema';


