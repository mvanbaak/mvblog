ALTER TABLE comments ADD COLUMN deleted integer;
ALTER TABLE comments ALTER COLUMN deleted SET DEFAULT 0;
