alter table authors add active smallint;
alter table authors alter active set default 1;
update authors set active=1;
