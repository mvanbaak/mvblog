alter table articles add column aside smallint;
alter table articles alter column aside set default 0;
update articles set aside=0;
