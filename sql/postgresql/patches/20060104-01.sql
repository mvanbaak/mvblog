CREATE TABLE menulinks (
	id serial NOT NULL,
	url character varying,
	linktitle text,
	image character varying,
	sortorder integer
);

ALTER TABLE ONLY menulinks
	ADD CONSTRAINT menulinks_pkey PRIMARY KEY (id);
