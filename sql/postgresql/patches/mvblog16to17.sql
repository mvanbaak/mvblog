CREATE TABLE blog_users (
    id serial NOT NULL,
    username character varying(255),
    "password" character varying(255),
    realname character varying(255),
    email character varying(255),
    website character varying(255),
    regcode character varying(255),
    active smallint DEFAULT 0
);
 
ALTER TABLE ONLY blog_users
    ADD CONSTRAINT blog_users_id_pkey PRIMARY KEY (id);
 
ALTER TABLE articles ADD COLUMN allowanoncomments smallint;
