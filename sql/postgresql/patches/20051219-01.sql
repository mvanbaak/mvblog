CREATE TABLE acronyms (
    id serial NOT NULL,
    acronym character varying,
    description text
);

COPY acronyms (id, acronym, description) FROM stdin;
1	mvblog	The engine this blog is running on. Coded by Michiel van Baak
2	AOL	America Online
3	API	Application Programming Interface
4	BTW	By The Way
5	BPL	Broadband over Power Lines
6	CD	Compact Disk
7	CGI	Common Gateway Interface
8	CLI	Common Language Interpreter
9	CMS	Content Management System
10	CSS	Cascading Style Sheets
11	CVS	Concurrent Versions System
12	DNS	Domain Name System
13	DOM	Document Object Model
14	DSL	Digital Subscriber Line (broadband over phone lines)
15	DTD	Document Type Definition
16	DVD	Digital Video Disc
17	EBI	Elim Bible Institute (Bible College in upstate NY)
18	EGC	Elim Gospel Church (my church)
19	EF	Elim Fellowship (an association of evangelical ministers)
20	FAQ	Frequently Asked Questions
21	FCC	Federal Communications Commission
22	FOAF	Friend Of A Friend is a RDF dialect for describing relationships
23	FSF	Free Software Foundation
24	FTP	File Transfer Protocol
25	FTTH	Fiber to the Home
26	GB	Gigabyte
27	GFDL	GNU Free Documentation License
28	GPG	Gnu PG (Open source public key encryption)
29	GPL	GNU General Public License
30	GTK	GUI ToolKit - The GIMP Tool Kit for dcreating user-interfaces
31	GUI	Graphical User Interface
32	HDTV	High Definition TeleVision
33	HTML	HyperText Markup Language
34	HTTP	HyperText Transfer Protocol
35	IE	Internet Explorer
36	ICANN	Internet Corporation for Assigned Names and Numbers
37	IHOP	International House of Pancakes
38	IIRC	if I remember correctly
39	IIS	Internet Infomation Server
40	IM	Instant Message
41	IMAP	Internet Message Access Protocol
42	IP	Internet Protocol
43	IRC	Internet Relay Chat - like Instant Messaging for groups
44	JSP	Java Server Pages
45	KB	Kilobyte
46	KDE	K Desktop Environment
47	KVM	Keyboard, Video, Mouse switch for controlling multiple computers
48	LDAP	Lightweight Directory Access Protocol
49	LGPL	GNU Lesser General Public License
50	MAPI	Mail Application Programming Interface
51	MB	Megabyte
52	MP3	MPEG Layer 3 - a common audio codec for music files
53	MS	Microsoft
54	MSDN	Microsoft Developer Network
55	MSIE	Microsoft Internet Explorer
56	MSN	Microsoft Network
57	NNTP	Network News Transfer Protocol - the protocol used for NewsGroups
58	NYC	New York City
59	OPML	Outline Processor Markup Language
60	P2P	Peer To Peer
61	PBS	Public Broadcasting System
62	PDF	Portable Document Format
63	PGP	Pretty Good Privacy (public key encryption)
64	PHP	Hypertext PreProcessing
65	PNG	Portable Network Graphics
66	POP	Short for POP3, the Post Office Protocol for email
67	POP3	Post Office Protocol 3 (for email)
68	RAID	Redundant Array of Independent Disks
69	RDF	Resource Description Framework
70	RPC	Remote Procedure Call
71	RSS	Really Simple Syndication
72	SIG	Special Interest Group
73	SOAP	Simple Object Access Protocol
74	SQL	Structured Query Language (a database standard)
75	SSH	Secure SHell (encrypted protocol replaces telnet and FTP)
76	SSN	Social Security Number
77	SSL	Secure Sockets Layer (a security protocol)
78	SVG	Scalable Vector Graphics
79	TCP	Transmission Control Protocol
80	UDP	User Datagram Protocol
81	URI	Uniform Resource Identifier
82	URL	Uniform Resource Locator
83	USB	Universal Serial Bus
84	VB	Visual Basic
85	VBS	Visual Basic Script
86	VM	Virtual Machine
87	VNC	Virtual Network Computing
88	W3C	World Wide Web Consortium
89	WCAG	Web Content Accessibility Guidelines
90	WYSIWYG	what you see is what you get
91	XHTML	eXtensible HyperText Markup Language - HTML reformulated as XML
92	XML	eXtensible Markup Language
93	XSL	eXtensible Stylesheet Language
94	XSLT	eXtensible Stylesheet Language Transformation
\.

ALTER TABLE ONLY acronyms
    ADD CONSTRAINT acronyms_pkey PRIMARY KEY (id);

SELECT pg_catalog.setval('acronyms_id_seq', 94, true);
